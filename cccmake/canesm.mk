# makefile template for CanESM builds

# The root of the acrnopt package directory is at ~scrd102/package on XC40 or PPP
OPT_ROOT := $(shell perl -e '$$x=(getpwnam "scrd102")[7]."/package"; print $$x if -d $$x' 2>/dev/null)
ifndef OPT_ROOT
  $(warning OPT_ROOT is not defined.)
endif

# Define a unique string (append to certain file names below)
STAMP := $(shell date "+%Y%b%d_%H%M%S")

# Define a path search macro (used in conjunction with the call function below)
psearch = $(firstword $(wildcard $(addsuffix /$(1),$(subst :, ,$(PATH)))))

# Define a string to identify the kernel and hardware type
os := $(shell uname -s 2>/dev/null)
hw := $(shell uname -m 2>/dev/null)
ifeq ($(strip $(os)-$(hw)),Linux-x86_64)
  ARCH := linux64
else ifeq ($(strip $(os)),AIX)
  ARCH := aix64
else
  ARCH := $(strip $(os)-$(hw))
endif

# Determine the current domain name, if possible
ifneq "$(call psearch,dnsdomainname)" ""
  # dnsdomainname is on our path
  # Determine the current dns domain name using dnsdomainname
  DOMAIN := $(subst int.cmc.,cmc.,$(shell dnsdomainname))
else ifneq "$(strip $(wildcard /etc/resolv.conf))" ""
  # /etc/resolv.conf exists
  # Determine the current dns domain name from resolve.conf
  DOMAIN := $(subst int.cmc.,cmc.,$(word 2,$(shell grep -E 'search|domain' /etc/resolv.conf)))
endif

# Determine the name of the machine on which this is running
MACH_NAME := $(word 1,$(subst ., ,$(shell uname -n|awk -F'.' '{print $1}' -)))
$(info ARCH = $(ARCH) on $(MACH_NAME) in domain $(DOMAIN))

# project_root will be the root of the directory tree that was created during "setup"
ifndef project_root
  ifneq "$(strip $(wildcard project_root/.))" ""
    project_root := $(shell readlink project_root || readlink -f project_root)
    $(info Using default for project_root = $(project_root))
  endif
endif
ifeq "$(strip $(wildcard $(project_root)/.))" ""
  $(error project_root = $(project_root) is not a directory)
endif

# env_file is a shell script that will be sourced prior to running other commands
# It is intended to set environment variables that the build requires
ifndef env_file
  env_file := $(project_root)/generic/env_setup_file
  $(info Using default for     env_file = $(env_file))
endif
ifndef env_file
  $(error env_file is not defined)
else
  ifeq "$(strip $(wildcard $(env_file)))" ""
    $(error env_file = $(env_file) is missing)
  endif
endif

ifndef model_job
  model_job := ./model_job
  $(info Using default for    model_job = $(model_job))
endif
ifeq "$(strip $(wildcard $(model_job)))" ""
  $(error model_job = $(model_job) is missing)
endif

# canesm_repo must be a valid directory on disk that is visible from wherever make is invoked
# canesm_repo must also contain a certain tree structure as indicated below
ifndef canesm_repo
  canesm_repo := $(project_root)/canesm
  $(info Using default for  canesm_repo = $(canesm_repo))
endif
ifndef canesm_repo
  $(error canesm_repo is not defined)
endif
ifeq "$(strip $(wildcard $(canesm_repo)/.))" ""
  $(error canesm_repo = $(canesm_repo) is not a directory)
endif
ifndef canesm_ver
  shcmd := cd $(canesm_repo); git rev-parse HEAD
  shout := $(shell $(shcmd); echo $$?)
  exit_status := $(word $(words $(shout)), $(shout))
  canesm_HEAD := $(firstword $(shout))
  $(info canesm_HEAD  = $(canesm_HEAD))
  ifneq ($(strip $(exit_status)),0)
    $(error Problem executing --> $(shcmd) <--)
  endif
  canesm_ver ?= $(canesm_HEAD)
endif

# It is assumed that canesm_repo contains all relevant git repositories as sub directories
# note: cancpl_repo is only set here if it was not previously defined
cancpl_repo ?= $(canesm_repo)/CanCPL
ifeq "$(strip $(wildcard $(cancpl_repo)/.))" ""
  $(error cancpl_repo = $(cancpl_repo) is not a directory)
else
  # Determine the sha1 hash for the HEAD commit in cancpl_repo
  # As a side effect, this will also verify that cancpl_repo is a git repository
  shcmd := cd $(cancpl_repo); git rev-parse HEAD
  shout := $(strip $(shell $(shcmd); echo $$?))
  exit_status := $(word $(words $(shout)), $(shout))
  ifneq ($(strip $(exit_status)),0)
    $(info exit status = $(exit_status)  stdout = $(shout))
    $(error Problem executing --> $(shcmd) <--)
  endif
  cancpl_HEAD := $(firstword $(shout))
  $(info cancpl_HEAD  = $(cancpl_HEAD))
  # cancpl_ver is only set here if it was not previously defined
  cancpl_ver ?= $(cancpl_HEAD)
endif

# note: canam_repo is only set here if it was not previously defined
canam_repo ?= $(canesm_repo)/CanAM
ifeq "$(strip $(wildcard $(canam_repo)/.))" ""
  $(error canam_reop = $(canam_repo) is not a directory)
else
  ifndef canam_ver
    shcmd := cd $(canam_repo); git rev-parse HEAD
    shout := $(shell $(shcmd); echo $$?)
    exit_status := $(word $(words $(shout)), $(shout))
    canam_HEAD := $(firstword $(shout))
    $(info canam_HEAD   = $(canam_HEAD))
    ifneq ($(strip $(exit_status)),0)
      $(error Problem executing --> $(shcmd) <--)
    endif
    canam_ver ?= $(canam_HEAD)
  endif
endif

# note: nemo_repo is only set here if it was not previously defined
nemo_repo ?= $(canesm_repo)/CanNEMO
ifeq "$(strip $(wildcard $(nemo_repo)/.))" ""
  $(error nemo_repo = $(nemo_repo) is not a directory)
else
  ifndef nemo_ver
    shcmd := cd $(nemo_repo); git rev-parse HEAD
    shout := $(shell $(shcmd); echo $$?)
    exit_status := $(word $(words $(shout)), $(shout))
    nemo_HEAD := $(firstword $(shout))
    $(info nemo_HEAD    = $(nemo_HEAD))
    ifneq ($(strip $(exit_status)),0)
      $(error Problem executing --> $(shcmd) <--)
    endif
    # nemo_ver is only set if it was not previously defined
    nemo_ver ?= $(nemo_HEAD)
  endif
endif

# This help is the default target so that just typing "make" will not do anything destructive
.PHONY: help
help:
	@echo " "
	@echo "Usage: (quantities in square brackets are optional)"
	@echo "make [make_opts] cpl  [var=val ...]   ...build the coupler executable"
	@echo "make [make_opts] atm  [var=val ...]   ...build the AGCM executable"
	@echo "make [make_opts] nemo [var=val ...]   ...build the NEMO executable"
	@echo "make [make_opts] all  [var=val ...]   ...build the coupler, AGCM and NEMO executables"
	@echo "make_opts are any valid make option"
	@echo " "
	@echo "The following variables may be defined on the make command line"
	@echo '   project_root=PATH  a full pathname to the root of the project (e.g. $$RUNPATH_ROOT/users/$$USER/PRODUCTION/$$runid)'
	@echo "                      This directory is created during setup of the project"
	@echo "   canesm_repo=PATH   a full pathname to the root of the CanESM repo"
	@echo "                      This directory is created during setup of the project"
	@echo "   canesm_ver=STRING  the CanESM commit containing submodules for CanCPL, CanAM, CanNEMO"
	@echo "   cancpl_repo=PATH   a full pathname to the root of the CanCPL repo (optional)"
	@echo "                      This directory is created during setup of the project"
	@echo "   cancpl_ver=STRING  the CanCPL commit to compile (optional)"
	@echo "   canam_repo=PATH    a full pathname to the root of the CanAM repo (optional)"
	@echo "                      This directory is created during setup of the project"
	@echo "   canam_ver=STRING   the CanAM commit to compile (optional)"
	@echo "   nemo_repo=PATH     a full pathname to the root of the CanNEMO repo (optional)"
	@echo "                      This directory is created during setup of the project"
	@echo "   nemo_ver=STRING    the CanNEMO commit to compile (optional)"
	@echo "   model_job=STRING   the name of a file containing an appropriate model job string"

$(info project_root = $(project_root))
$(info env_file     = $(env_file))
$(info canesm_repo  = $(canesm_repo))
$(info canesm_ver   = $(canesm_ver))
$(info cancpl_repo  = $(cancpl_repo))
$(info cancpl_ver   = $(cancpl_ver))
$(info canam_repo   = $(canam_repo))
$(info canam_ver    = $(canam_ver))
$(info nemo_repo    = $(nemo_repo))
$(info nemo_ver     = $(nemo_ver))

.PHONY: cpl atm nemo

cpl:
	test -r $(env_file)
	. $(env_file) && build-exe -c $(model_job) canesm_root=$(canesm_repo)
atm:
	test -r $(env_file)
	. $(env_file) && build-exe -a $(model_job) canesm_root=$(canesm_repo)
nemo:
	test -r $(env_file)
	. $(env_file) && build-exe -n $(model_job) canesm_root=$(canesm_repo) \
	nemo_repo=$(nemo_repo)     nemo_ver=HEAD \
	cancpl_repo=$(cancpl_repo) cancpl_ver=HEAD
