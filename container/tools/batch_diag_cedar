#!/bin/bash
#SBATCH --time=1:00:0
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=12
#SBATCH --mem-per-cpu=4000
#SBATCH --job-name=diag_canesm

# NCS October 2020. This is a brute force implementation of some basic
# file rebuilding and diagnostics. It is meant only to produce a
# minimal level of information that will partly enable model validation.

set -x
echo "Working in $(pwd)"
config_file=$(pwd)/canesm.cfg

# GET CONFIGURATION VARIABLES
[ ! -f "$config_file" ] && ( echo "$config_file must exist!" ; exit 1 )

CWD=$PWD
. $config_file

# Check required variables are defined
[[ ! -f ${CONTAINER_IMAGE} ]]   && ( echo "CONTAINER_IMAGE must be defined and exist" ; exit 1 )
[[ ! -d ${CANESM_SRC_ROOT} ]]   && ( echo "Required code CANESM_SRC_ROOT/canesm does not exist" ; exit 1 )
[[ ! -d ${EXEC_STORAGE_DIR} ]]  && ( echo "EXEC_STORAGE_DIR must be defined and must exist" ; exit 1 )
[[ ! -d ${INPUT_DIR} ]]         && ( echo "INPUT_DIR must be defined and must exist" ; exit 1 )
[[ -z ${OUTPUT_DIR} ]]          && ( echo "OUTPUT_DIR must be defined" ; exit 1 )

# Move to tmpdir
cd $SLURM_TMPDIR

#Get mpich, unload mpi
set +x
module --force purge
module load StdEnv/2016.4 nixpkgs/16.09 gcc/7.3.0 mpich/3.2.1 netcdf-fortran/4.4.4
module unload intel openmpi
module load singularity
set -x

# GET required files and data
cp -p ${OUTPUT_DIR}/${DIAG_YEAR}/${RUNID}*.nc .
cp -p ${OUTPUT_DIR}/${DIAG_YEAR}/mesh_mask*.nc .
cp -p ${OUTPUT_DIR}/${DIAG_YEAR}/OUTGCM* .
cp -p ${OUTPUT_DIR}/${DIAG_YEAR}/time.step .
cp -p ${EXEC_STORAGE_DIR}/rebuild_nemo.exe .
cp -p ${EXEC_STORAGE_DIR}/bin_diag/candiag_mwe.exe .
cp -p ${EXEC_STORAGE_DIR}/ccc2nc_linux .
cp -p ${CANESM_SRC_ROOT}/CanDIAG/mwe/diagnostic_recipes/temp_recipe .
cp -p ${CANESM_SRC_ROOT}/CanDIAG/mwe/diagnostic_recipes/winds_recipe .
cp -p ${WRK_DIR}/config/* .

# Set local container image path for diagnostics
local_container_image=$(pwd)/$(basename ${CONTAINER_IMAGE})

. $CANESM_SRC_ROOT/CCCma_tools/container/tools/basic_diag
   
# Cycle job
cd $CWD
next_year=$(( $DIAG_YEAR + 1 ))

echo $next_year
echo $STOP_YEAR $CURRENT_YEAR

if [ "$next_year" -le "$STOP_YEAR" ] && [ "$next_year" -lt "$CURRENT_YEAR" ]; then
    sed -i "s/DIAG_YEAR=.*/DIAG_YEAR=${next_year}/" canesm.cfg
    sbatch --account=$CC_ACCOUNT $CANESM_SRC_ROOT/CCCma_tools/container/tools/batch_diag_cedar
fi



