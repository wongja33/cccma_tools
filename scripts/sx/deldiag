#! /bin/sh

#    Dec 03/08 - F.Majaess (Revised for sa/saiph, ib/dorval-ib, af/alef)
#    Apr 24/07 - F.Majaess (Added deletion of "_gp[lnpm,lnpt,map,pmsl,ps]")
#    Oct 16/06 - F.Majaess (Revised for ma/maia, ns/naos)
#    Mar 01/06 - F.Majaess (Revised for rg/rigel)
#    Feb 15/05 - F.Majaess (Revised for AIX/Linux setup in Victoria)
#    Jun 20/03 - F.Majaess (Revised for az/azur)
#    Oct 29/02 - F.Majaess (Added deletion of "flabel" associated 
#                           subdirectories containing splitted variables)
#    Feb 25/02 - F.Majaess (Revised for ya/yata)
#    Sep 14/00 - F.Majaess (Revised for tu/tsunami)
#    Jan 05/00 - F.Majaess (Added kz/kaze)
#    Apr 23/99 - F.Majaess (Added deletion of "_gbx","_gsx" & "_gxx")
#    Jan 21/99 - F.Majaess (Added yo/yonaka)
#    Nov 03/97 - F.Majaess (Added deletion of "_gpvr" & "_sp...")
#    Jul 14/97 - F.Majaess (Added sx/hiru)
#    Mar 14/96 - F.Majaess (Added sx4)
#    Jan 08/96 - MB,FM (Revise to use access/delete)
#    Jun 02/95 - F.Majaess (Added orion, gandalf & aragorn)
#    Nov 08/94 - F.Majaess (Modify for new destination identifiers)
#    Jul 18/94 - E. Chan (Add deletion of "_gpq" etc. set of files)
#    Nov 25/92 - E. Chan (Modify to delete files and their associated links
#                         on the official data directory)
#    Nov 13/92 - E. Chan   
 
#id  deldiag - removes a family of diagnostic files associated with a
#id            diagnostic run from any official CCRD/N file system and  
#id            their corresponding links on the official data directory
 
#    AUTHOR - E. Chan  
 
#hd  PURPOSE - "deldiag" submits a job to one of the local machine(s) at 
#hd            the local site to delete files related to a particular 
#hd            diagnostic run from any official CCRD/N file system. Their 
#hd            corresponding symbolic links on the official CCRD/N data 
#hd            directory are also removed.
#hd 
#hd            By default, the latest edition of each file is removed.
#hd            Although an optional parameter "ed" may be used to delete  
#hd            other editions, this usually is not very useful since the 
#hd            files in the same family normally do not have the same 
#hd            edition numbers.
#hd  
#hd            The affected files must start with "flabel" and end (if the
#hd            edition number is disregarded) with gb, gp, sp, or xp.
#hd            Other temporary files may also be removed using the "others"
#hd            option.
#hd  
#hd            No printout is returned unless the "outfil" switch is used.
 
#pr  PARAMETERS:
#pr  
#pr    POSITIONAL     
#pr  
#pr      flabel = family name of permanent files to be deleted
#pr  
#pr    PRIMARY
#pr
#pr      Note: One of 'ha/sp/po/ca/mz/hr/br/p1/p2'  in Dorval.
#pr            In Victoria, default destination: "$HOSTID"  for Linux.
#pr
#pr          hr = to target job to hare.
#pr
#pr          br = to target job to brooks.
#pr
#pr          p1 = to target job to ppp1.
#pr
#pr          p2 = to target job to ppp2.
#pr
#pr          ha = to target job to IBM (hadar).
#pr
#pr          sp = to target job to IBM (spica).
#pr
#pr          po = to target job to Linux (pollux).
#pr   
#pr          ca = to target job to Linux (castor).
#pr
#pr          mz = to target job to Linux (mez).
#pr
#pr
#pr        time = time limit for job in seconds (=200)
#pr  
#pr         mem = memory limit for job in megabytes (=25)
#pr  
#pr          ed = edition number of gb, gp, sp, and xp files (defaults 
#pr               to the latest edition)
#pr  
#pr        path = path of data directory (='$RUNPATH') 
#pr 
#pr    SECONDARY
#pr 
#pr      others = switch to delete other temporary diagnostic files and
#pr               subdirectories related to "flabel"
#pr  
#pr         adt = switch to produce an audit of back-end disk space usage
#pr  
#pr      outfil = switch to force return of printout

#ex  EXAMPLE: 
#ex  
#ex  deldiag dmm17 others po
#ex 
#ex  The above example deletes on "pollux", the latest editions of the gb, gp, 
#ex  sp, and xp files associated with the "flabel" dmm17. The corresponding 
#ex  symbolic links are removed from the official data directory. All 
#ex  editions of temporary files associated with "dmm17" are also removed.
#ex  
 
#  * Obtain any specified option. 

for arg in $@
do
  case $arg in
        -*) set $arg                            ;;
       *=*) eval $arg                           ;;
      ed=*) ed=`expr $arg : 'ed=\([0-9]*\)'`    ;;
    others) others=yes                          ;;
       adt) adt=yes                             ;;
        hr) mdest=${mdest:='hare'}              ;;
        br) mdest=${mdest:='brooks'}            ;;
        p1) mdest=${mdest:='ppp1'}              ;;
        p2) mdest=${mdest:='ppp2'}              ;;
        ha) mdest=${mdest:='hadar'}             ;;
        sp) mdest=${mdest:='spica'}             ;;
        po) mdest=${mdest:='pollux'}            ;;
        ca) mdest=${mdest:='castor'}            ;;
        mz) mdest=${mdest:='mez'}               ;;
    mamccc) mdest=${mdest:='mamccc'}            ;;
    odin) mdest=${mdest:='odin'}                ;;
    climate) mdest=${mdest:='climate'}          ;;
    outfil) outfil=yes                          ;;
         *) flabel=${flabel:=$arg}
  esac
done

#  * Set the defaults.

time=${time:=200}
mem=${mem:=25}
path=${path:='$RUNPATH'}
nonqs=${nonqs:=$NONQS}
nonqs=${nonqs:='no'}

#  * Prompt for a destination if none was specified.

while [ -z "$mdest" ]
do
 if [ "$SITE_ID" = 'Victoria' ] ; then
    mdest="$HOSTID"
 elif [ "$SITE_ID" = 'York' ] ; then
   mdest="$HOSTID"
 elif [ "$SITE_ID" = 'Utor1' -o "$SITE_ID" = 'UOFT' ] ; then
   mdest="$HOSTID"
 elif [ "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then
  echo "please enter a destination; ha/sp/po/ca/mz/hr/br/p1/p2: > \\c"
  read tmdest
  case $tmdest in
      hr) mdest='hare'                  ;;
      br) mdest='brooks'                ;;
      p1) mdest='ppp1'                  ;;
      p2) mdest='ppp2'                  ;;
      ha) mdest='hadar'                 ;;
      sp) mdest='spica'                 ;;
      po) mdest='pollux'                ;;
      ca) mdest='castor'                ;;
      mz) mdest='mez'                   ;;
       *) echo "illegal destination ! " ;;
  esac
 elif [ "$SITE_ID" = 'Downsview' ] ; then
    mdest="$HOSTID"
 else
  echo "please enter a destination: > \\c"
  read mdest
 fi
done


if [ "$mdest" = 'hare' ] ; then
  ext=.hr
elif [ "$mdest" = 'brooks' ] ; then
  ext=.br
elif [ "$mdest" = 'ppp1' ] ; then
  ext=.p1
elif [ "$mdest" = 'ppp2' ] ; then
  ext=.p2
elif [ "$mdest" = 'hadar' ] ; then
  ext=.ha
elif [ "$mdest" = 'spica' ] ; then
  ext=.sp
elif [ "$mdest" = 'pollux' ] ; then
  ext=.po
elif [ "$mdest" = 'castor' ] ; then
  ext=.ca
elif [ "$mdest" = 'mez' ] ; then
  ext=.mz
fi

#  * Prompt for a "flabel" if none was specified.
 
while [ -z "$flabel" ] 
do
  echo "please enter a value for flabel: > \\c"
  read flabel
done

#  * Translate "flabel" to lower case.

flabel=`echo $flabel | tr '[A-Z]' '[a-z]'`

#  * Remove leading zeros in the edition number and ensure that  
#  * three digits are used if it is specified.
 
if [ -n "$ed" ] ; then
  ed=`expr $ed + 0`
  if [ "$ed" -lt 100 ] ; then
    ed=0$ed
  fi
  if [ "$ed" -lt 10 ] ; then
    ed=0$ed
  fi
  Ted="ed=$ed"
fi

#   * Check parameter values.

chkrl=" deldiag flabel=$flabel ed=$ed  "
chkrl=" $chkrl  adt=$adt others=$others mdest=$mdest"

if [ "$SITE_ID" = 'York' ] ; then
 . checker | more 
elif [ "$SITE_ID" = 'Victoria' -o "$SITE_ID" = 'Downsview' ] ; then
 . checker 
elif [ \( "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' \) -a "$OS" = 'Linux' ] ; then
 . checker 
else
 . checker | more 
fi

#   * Setup for possible deletion of "flabel" associated 
#   * subdirectories containing splitted variables.

username=${username:=$USER}
if [ -z "$username" ] ; then
 username=`whoami`
fi

#  * Construct and submit the job.

if [ "$nonqs" = 'yes' ] ; then
  if [ -z "$outfil" -a -z "$adt" ] ; then
    outdest='1>>/dev/null 2>>/dev/null'
  elif [ -z "$adt" ] ; then
    outdest="1>>$OUTPUTQ/deldiag_${flabel}${ext}.$$ 2>>$OUTPUTQ/deldiag_${flabel}${ext}.$$"
  else
    outdest="1>>$OUTPUTQ/deldiag_${flabel}${ext}.$$ 2>>/dev/null"
  fi
  if [ "$OS" = 'Linux' ] ; then
   Batch_job="/tmp/tmp_deldiag_job$$"
   RM_Batch_job="rm -f $Batch_job"
   line="cat > $Batch_job ; chmod u+x $Batch_job ; nohup $Batch_job $outdest & " 
  else
   RM_Batch_job=':'
   line="nohup sh -s - $outdest & "
  fi
else
  if [ -z "$outfil" -a -z "$adt" ] ; then
    if [ "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then
      outdest='-eo -o $mdest:/dev/null'
    else
      outdest='-eo -o /dev/null'
    fi
  elif [ -z "$adt" ] ; then
    outdest="-eo -o $OUTPUTQ/deldiag_${flabel}${ext}.$$"
  else
    if [ "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then
      outdest="-o $OUTPUTQ/deldiag_${flabel}${ext}.$$ -e $mdest:/dev/null"
    else
      outdest="-o $OUTPUTQ/deldiag_${flabel}${ext}.$$ -e /dev/null"
    fi
  fi
  line="$CCCQSUB -q $mdest -lT $time -lM ${mem}mb -r deldiag $outdest "
fi

cd $HOME/tmp
# $CCCQSUB -q $mdest -lT $time -lM ${mem}mb -r deldiag.$flabel \
# $CCCQSUB -q $mdest -lT $time -lM ${mem}mb -r deldiag \
#              $outdest <<endjob
(cat << endjob) | eval "$line"
#!/bin/sh
#
if [ -n "$outfil" ] ; then
  set -x
fi
#
Stamp="\${HOSTID}_"\`date +%Y%j%H%M%S\`
mkdir -m 755 \$CCRNTMP/tmp_deldiag_\$\$_\$Stamp
cd \$CCRNTMP/tmp_deldiag_\$\$_\$Stamp
#
#  * Access and remove the desired files.
#  * The latest edition is used if the edition number is not specified.
#
for f in gb gp sp xp cp dd dp ds
  do
     access deldiag\$f ${flabel}\$f $Ted na nocp 2>/dev/null
     delete deldiag\$f na
     release deldiag\$f 
  done
#
#  * Access and remove all temporary files if so desired.
#
if [ -n "$others" ] ; then
#  list=\`ls ${flabel}[*_]*.*\`

# gp...

  for f in gpu gpv gpw gpx gpy gpt gpq gpz gpvr gpdiv gprh gplnpm gplnpt gpmap gpbeta gptbeta gppmsl gpps pmsl ps gslnsp 
    do
        access deldiag\$f ${flabel}_\$f $Ted na nocp 2>/dev/null
        delete deldiag\$f na
        release deldiag\$f
    done

# sp...

  for f in div q t vr z
    do
        access deldiagsp\$f ${flabel}_sp\$f $Ted na nocp 2>/dev/null
        delete deldiagsp\$f na
        release deldiagsp\$f
    done
fi

# gsq gsz gst gsu gsv gsw ... 

  for f in gsq gsz gst gsu gsv gsw gspres gsrho
    do
        access deldiag\$f ${flabel}_\$f $Ted na nocp 2>/dev/null
        delete deldiag\$f na
        release deldiag\$f
    done

# g?x..

  for f in gbx gsx gxx 
    do
        access deldiaggx\$f ${flabel}_\$f $Ted na nocp 2>/dev/null
        delete deldiaggx\$f na
        release deldiaggx\$f
    done

# npak?? "flabel" associated subdirectories...

  for suffx in gg sp td cm gz
    do
      if [ -d \$CCRNTMP/tmp_${flabel}_${username}_npak\${suffx}/. ] ; then
        chmod -R u+w \$CCRNTMP/tmp_${flabel}_${username}_npak\$suffx
        rm -rf \$CCRNTMP/tmp_${flabel}_${username}_npak\$suffx
      fi
      if [ \$CCRNTMP != \$RUNPATH ] ; then
       if [ -d \$RUNPATH/tmp_${flabel}_${username}_npak\${suffx}/. ] ; then
        chmod -R u+w \$RUNPATH/tmp_${flabel}_${username}_npak\$suffx
        rm -rf \$RUNPATH/tmp_${flabel}_${username}_npak\$suffx
       fi
      fi
    done
#
#  * Generate an audit of user's files in directory if desired.
#
cd \$CCRNTMP 
\rm -r -f \$CCRNTMP/tmp_deldiag_\$\$_\$Stamp
if [ -n "$adt" ] ; then
  cd $path
  auditor own full
fi 
#
( rmllfls || : )
$RM_Batch_job
exit
endjob

exit
