#! /bin/sh
 
#    JUN 17/08 - F.Majaess
 
#id  lnx2be - Transfer data from Linux front-end to one of the back-ends.
 
#    AUTHOR  - F.Majaess
 
#hd  PURPOSE - "lnx2be" script is used to transfer "target" from front-end
#hd            server "tarpath" directory to "despath" directory on the 
#hd            back-end.
#hd            NOTE : Edition number is added via "save" script if 
#hd                   "nooffsv=no" or if "target" is an official file.
#hd                   Removal of "target" is controlled by "rmv" parameter. 
 
#pr  PARAMETERS:
#pr
#pr    POSITIONAL     
#pr
#pr      target = name of target file/subdirectory to transfer. 
#pr               (same name is used on destination machine) 
#pr
#pr    PRIMARY
#pr
#pr  fil_grpname= User has the option of specifying one of
#pr               'ccrn_shr/ccrn_stf/ccrn_net/ccrn_rcm/ccrn_mam/ccrn_gst'
#pr               group ownership for officially saved transfered 
#pr               files. (DISABLED)
#pr               (=$DEFFGRP).
#pr
#pr    PRIMARY/SECONDARY
#pr
#pr      nooffsv= switch controlling saving on destination via "save"
#pr               script (=no/yes)
#pr      rmv    = switch controls deletion of "target" once the transfer
#pr               is complete (=no/yes). 
#pr      noprint= switch controls output generation; "yes" (or "on") no output
#pr               (=no/yes).
#pr       addhed= add highest edition found to "target" provided the latter
#pr               doesn't already has edition number suffix and "nooffsv=yes"
#pr               (=no/yes)
#pr
#pr    PRIMARY
#pr
#pr
#pr      Note: One of 'hr/br/dy/ba' is to be specified.
#pr
#pr    hare/hr  = to target job to hare.
#pr
#pr   brooks/br = to target job to brooks.
#pr
#pr   banting/ba = to target job to banting.
#pr
#pr   daley/dy = to target job to daley.
#pr
#pr      tarpath= subdirectory in which "target" data is residing 
#pr               on front-end machine (=$RUNPATH).
#pr      despath= subdirectory in which the transfered data get stored
#pr               on destination machine ($BERUNPATH).
#pr      time   = back-end CPU time in seconds (=600)
#pr      mfl    = back-end MFL                 (=250MB)
#pr      vmode  = "chmod" access code on "target" (=u+rw)
#pr      box    = box to dispose printout if desired (=$BOX).
#pr      un     = usernumber for printout (=$USER). 
 
#ex  EXAMPLE: 
#ex
#ex        lnx2be hr rmv=yes mydirectory 
#ex   
#ex    to transfer "mydirectory" subdirectory under what's
#ex    pointed to by RUNPATH on Hare back-end.
#ex
#ex        lnx2be hr rmv=yes \
#ex               despath=/space/hall1/work/eccc/crd/ccrn/users/abc123 \
#ex               myfilex.009 
#ex   
#ex    to transfer and save CCCma officially saved "myfilex" file to 
#ex    /space/hall1/work/eccc/crd/ccrn/users/abc123 subdirectory 
#ex    on HARE.
#ex
#ex        lnx2be despath=/space/hall2/work/eccc/crd/ccrn/users/abc123 \
#ex               myfiley addhed=yes br
#ex
#ex    The above will result in transferring "myfiley" with
#ex    the highest edition number found to BROOKS and perform
#ex    official "save" on it into 
#ex     /space/hall2/work/eccc/crd/ccrn/users/abc123 subdirectory.
#ex
#ex        lnx2be filexx.001 \
#ex               tarpath='/space/hall1/work/eccc/crd/ccrn/users/abc123' \
#ex               despath='/space/hall2/work/eccc/crd/ccrn/users/abc123' \
#ex                           br
#ex
#ex    The above command, executed on Hare back-end, requests transfer
#ex    of /space/hall1/work/eccc/crd/ccrn/users/abc123/filexx.001 file 
#ex    on Hare back-end into /space/hall2/work/eccc/crd/ccrn/users/abc123
#ex    subdirectory on Brooks back-end.
# set -x 
OS=${OS:-`uname -s`}
HOSTID=${HOSTID-`hostname | cut -d'.' -f1`}
HOSTID=`echo $HOSTID | sed -e 's/hpcr4-in/ppp4/' -e 's/hpcr3-in/ppp3/' `
HOSTIDf=`echo $HOSTID | sed -e 's/eccc.*-ppp/cs/g' -e 's/^ppp/cs/g' | cut -c 1-3`

unset mdest Tdefq
 
#   * code used to set switches and deal with parameter=value arguments 
#   * as well as setting parameters to their primary/secondary defaults.
#   * The list of other arguments (if any) is returned in "prmtrl" var...
 
#  *  check if switches/parameters specified on script call
 
if [ $# -gt 0 ] ; then
  
#  * put argument list in "prmtrl" ...
   
  prmtrl=$@
# prmtrl=`echo $prmtrl | tr '[A-Z]' '[a-z]'`
  
#  * for each argument specified do...
    
  for prmtr in $prmtrl
  do
    case $prmtr in
 
#   * check for standard shell switches and set them accordingly
 
     --|-|[-+][aefhkntuvx]) set `echo $prmtr ` ;;
 
#   * case where a parameter is to be assigned a value
 
    *=*   ) eval $prmtr  ;;
 
#   * destinations 
 
      hr) mdest=${mdest:='hare'}      ;;
      br) mdest=${mdest:='brooks'}      ;;
      dy) mdest=${mdest:='daley'}      ;;
      ba) mdest=${mdest:='banting'}      ;;
 
#   * anything else case skip...
 
    *  )  tprmtr="$tprmtr $prmtr"      ;;
 
    esac
  done
  prmtrl=$tprmtr
  tprmtr=
 
fi
prmtrl=${prmtrl=}

#   * Check/set parameters to the secondary defaults...
 
if [ -n "$prmtrl" ] ; then
  
  tprmtr=
 
#  * for each argument undelt with yet do...
  
  for prmtr in $prmtrl
  do
      
    case $prmtr in
 
#   * check for keyword(s) then 
#   *     if the corresponding parameter is already set then
#   *        store "keyword" in "tprmtr" so that it is treated 
#   *        later on as a value to a positional parameter
#   *     otherwise set parameter whose name is "keyword" to 
#   *     its secondary default.
 
    rmv ) if [ -n "$rmv" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            rmv='yes' 
          fi ;;
 
 nooffsv ) if [ -n "$nooffsv" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            nooffsv='yes' 
          fi ;;

 noqsub ) if [ -n "$noqsub" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            noqsub='yes' 
          fi ;;
 
  noprint) if [ -n "$noprint" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            noprint='yes'
          fi ;;

  addhed ) if [ -n "$addhed" ] ; then
            tprmtr="$tprmtr $prmtr"
          else
            addhed='yes' 
          fi ;;

#   * anything else case accumulate in "tprmtr" temporary var...
 
    *  )  tprmtr="$tprmtr $prmtr";;
 
    esac
  done
  prmtrl=$tprmtr
  tprmtr=''
fi

 
#   * make sure arguments list variable "prmtrl" is initialized...
 
prmtrl=${prmtrl=}
  
#   * The following code deals with checking/setting positional 
#   * parameters ...
#   * Note: script will abort if too many arguments are specified.
 
if [ -n "$prmtrl" ] ; then
  i=1
  for prmtr in $prmtrl
  do
    case $i in
      1) target=$prmtr;;
      *) eval "echo '$0 : too many parameters !' "; exit;;
    esac
    i=`expr $i + 1`
  done
fi
 
#   * Prompt for a positional parameter value(s) if none was/were
#   * specified...
 
while [ -z "$target" ] ; do
  echo "please enter file\/subdirectory name ? > \\c"
  read target 
done
 
#  * Prompt for a destination if none was specified.

while [ -z "$mdest" ]
do
  echo "please enter a destination; hr/br/dy/ba: > \\c"
  read tmdest
  case $tmdest in
##     sx) mdest='sx'                   ;;
# sx|hiru) Tdefq=${DEFBEQ:='hiru'}
#          mdest='hiru'                 ;;
      hr) mdest='hare'                 ;;
      br) mdest='brooks'                 ;;
      dy) mdest='daley'                 ;;
      ba) mdest='banting'                 ;;
       *) echo "illegal destination ! " ;;
  esac
done
Tdefq=${Tdefq:=$mdest}
if [ "$mdest" = 'hare' ] ; then
  mext='hr'
elif [ "$mdest" = 'brooks' ] ; then
  mext='br'
elif [ "$mdest" = 'daley' ] ; then
  mext='dy'
elif [ "$mdest" = 'banting' ] ; then
  mext='ba'
else
  mext='sss'
fi

#   * code used to set hard coded primary defaults...
 
eval "time=${time='600'}" 
eval "mfl=${mfl='250MB'}"
# eval "vmode=${vmode='640'}"
eval "vmode=${vmode='u+rw'}"
eval "dvmode=${dvmode='755'}"
eval "box=${box=$BOX}"
eval "nooffsv=${nooffsv='no'}"
eval "noqsub=${noqsub='no'}"
noprint=${noprint='no'}
if [ "$noprint" = 'on' ] ; then
 noprint='yes'
elif [ "$noprint" = 'off' ] ; then
 noprint='no'
fi
if [ -z "$despath" -a "$mdest" = 'brooks' ] ; then
 BERUNPATH=`echo $RUNPATH | sed -e 's/\/hall1\//\/hall2\//' | sed -e 's/\/sitestore\//\/work\//'`
elif [ -z "$despath" -a "$mdest" = 'hadar' ] ; then
 BERUNPATH=`echo $RUNPATH | sed -e 's/\/hall2\//\/hall1\//' | sed -e 's/\/sitestore\//\/work\//'`
elif [ -z "$despath" -a "$mdest" = 'daley' ] ; then
 BERUNPATH=`echo $RUNPATH | sed -e 's/\/hall.\//\/hall4\//' | sed -e 's/\/sitestore\//\/work\//'`
elif [ -z "$despath" -a "$mdest" = 'banting' ] ; then
 BERUNPATH=`echo $RUNPATH | sed -e 's/\/hall.\//\/hall3\//' | sed -e 's/\/sitestore\//\/work\//'`
fi
eval "rmv=${rmv='no'}"
eval "addhed=${addhed='no'}"
eval "tarpath=${tarpath=$RUNPATH}"
eval "despath=${despath=$BERUNPATH}"
 
nooffsv=`echo $nooffsv | tr '[A-Z]' '[a-z]'`
rmv=`echo $rmv | tr '[A-Z]' '[a-z]'`
addhed=`echo $addhed | tr '[A-Z]' '[a-z]'`
mfl=`echo $mfl | tr '[a-z]' '[A-Z]'`

if [ "$addhed" = 'yes' ] ; then
 ed=`echo $target | sed -e 's/.*[.]\([0-9][0-9][0-9]\)/\1/'`
 if [ "$nooffsv" = 'yes' ] ; then
  echo "lnx2be: Invalid use of nooffsv=$nooffsv with addhed=$addhed !"
  exit 1
 elif [ -n "$ed" -a "$ed" != "$target" ] ; then
  echo "lnx2be: target=$target already has ed=$ed suffix!"
  exit 2
 else
  ttarget=`ls -1 $tarpath/${target}.[0-9][0-9][0-9] | sort | tail -1`
  if [ -n "$ttarget" ] ; then
   ed=`basename $ttarget | sed -e 's/.*[.]\([0-9][0-9][0-9]\)/\1/'`
   echo "lnx2be: Adjusting $target to ${target}.${ed}"
   target="${target}.${ed}"
  else
   echo "lnx2be: Sorry no matching ${target} filenmame with edition number suffix in $tarpath !"
   exit 3
  fi
 fi
fi

#   * Check if the file to be transfered, exist, readable and non-empty
#   * otherwise, issue an abort message and exit.
 
 
if [ ! -f "$tarpath/$target" -a ! -d  "$tarpath/$target/." ] ; then
 eval "echo $tarpath/$target is not valid !"
 exit 4
else
  :
fi
 
cd $OUTPUTQ
Pid=$$
Lhost=${HOSTID:=`hostname`}
SStrgt=`echo $tarpath | sed -n -e 's/^\/space\/.*\/sitestore\/.*$/yes/p'`
SStrgt=${SStrgt:-'no'}
 
Mainfrm=${HOSTID:=`hostname`}
HOSTIDf=`echo $Mainfrm | sed -e 's/eccc.*-ppp/cs/g' -e 's/^ppp/cs/g' | cut -c 1-3`
if [ "$HOSTIDf" = 'cs1' ] ; then
 Mainfrm='eccc-ppp1'
elif [ "$HOSTIDf" = 'cs2' ] ; then
 Mainfrm='eccc-ppp2'
elif [ "$HOSTIDf" = 'cs3' ] ; then
 Mainfrm='eccc-ppp3'
elif [ "$HOSTIDf" = 'cs4' ] ; then
 Mainfrm='eccc-ppp4'
elif [ "$HOSTIDf" = 'xc1' ] ; then
 Mainfrm='hare'
elif [ "$HOSTIDf" = 'xc2' ] ; then
 Mainfrm='brooks'
elif [ "$HOSTIDf" = 'xc3' ] ; then
 Mainfrm='banting'
elif [ "$HOSTIDf" = 'xc4' ] ; then
 Mainfrm='daley'
fi
Outq=$OUTPUTQ

Tmpfl="/tmp/tmp.lnx2be.ftptmp.$Pid"
if [ "$noprint" = 'yes' ] ; then
 Outqfl="/dev/null"
else
 Outqfl="$Outq/lnx2be.${Mainfrm}.$target.outerr.$Pid"
fi
if [ "$noqsub" = 'yes' ] ; then
 sbmtcmd=' /bin/sh - '
else
 sbmtcmd=" $CCCQSUB -q $Mainfrm -r lnx2be -lT $time -lM $mfl -eo -o $Outqfl "
fi
Ttarget=$target
if [ -d $tarpath/$target ] ; then
  targetdir='yes'
  cd $tarpath/$target 
  Tnlst=`echo *` 
  cd $OUTPUTQ
  descmd1="cd $tarpath/$target "'&&'
  descmd2="mkdir -m 755 $target"
  descmd3="cd $target"
  descmd4="mput $Tnlst"
  descmd12="ssh $mdest ' mkdir -m 755 ""$despath/$target""'"' &&'
  descmd14="$despath/$target"
else
  targetdir='no'
  ed=`expr $target : '.*\.\([0-9][0-9][0-9]\)$'`
  if [ -z "$ed" ] ; then
    Tnlst=$target
    descmd4="mput $Tnlst"
     #descmd14="scp -p -o NoneSwitch=yes $Tnlst $mdest:$despath/$Tnlst "
      descmd14="scp -p $Tnlst $mdest:$despath/$Tnlst "
  else
    Tflnm=`echo $target | sed 's/....$//' | tr '[a-z]' '[A-Z]' `
    Tnlst=$Tflnm
    descmd4="put $target $Tflnm"
    Ttarget=$Tflnm
    nooffsv='no'
    if [ "$tarpath/$target" != "$DATAPATH/$target" -a -h "$DATAPATH/$target" ] ; then
     OFFLink="$DATAPATH/$target"
    fi
     #descmd14="scp -p -o NoneSwitch=yes $target $mdest:$despath/$Tflnm"
      descmd14="scp -p $target $mdest:$despath/$Tflnm"
  fi
  descmd1="cd $tarpath "'&&'
  descmd2=' '
  descmd3=' '
  descmd12=' '
fi
if [ "$rmv" = "yes" ] ; then
  descmd5="\rm -r -f $tarpath/$target $OFFLink "'&&'
else
  descmd5="  "
fi
if [ "$nooffsv" != "yes" ] ; then
  if [ "$noqsub" = 'yes' ] ; then
   besbmtcmd=" ssh $Tdefq ' cat | /bin/sh - '"
  else
   if [ "$noprint" = 'yes' ] ; then
    beOutqfl="/dev/null"
   else
    beOutqfl="$Outq/lnx2be.$mext.$target.outerr.$Pid"
   fi
   besbmtcmd=" $CCCQSUB -q $Tdefq -r lnx2be -lT $time -lM $mfl -eo -o $beOutqfl"
  fi 
fi
 
#   ****   Task of the script...   ****
 
#   * Assemble and submit the job ...
 
# set -x
# set -v
if [ "$HDNODE" = 'hare' -o "$HDNODE" = 'brooks' -o "$HDNODE" = 'daley' -o "$HDNODE" = 'banting' ] ; then
 fexfer='' ; export fexfer
 bexber='on' ; export bexber 
else
 fexfer=on ; export fexfer
 bexber='' ; export bexber 
fi
# cat << woofsub 
# cat << woofsub | $sbmtcmd 
cat << woofsub | eval "$sbmtcmd" &&
#! /bin/sh
# set -x
if [ -n "$CUSTOM_ENV_FILE" -a -s "$CUSTOM_ENV_FILE" ] ; then
 . $CUSTOM_ENV_FILE || :
fi
CUSTOM_ENV_FILE="$CUSTOM_ENV_FILE" ; 
SStrgt="$SStrgt" ;
echo "" && echo "  lnx2be: - \`hostname\` - transferring $tarpath/$target : " &&
cd $tarpath  &&
echo "" &&
ls -al $tarpath/$target &&
Tnlst="$Tnlst" &&
echo "" &&
date | sed -n -e 's/^/0 lnx2be : Before starting the transfer at /p'  &&
ssh $mdest "cd $despath " &&
$descmd1
if [ "$descmd12" != ' ' ] ; then
$descmd12 
 for prmtr in \$Tnlst
  do
    if [ "$SStrgt" = 'yes' ] ; then
     ssh $mdest "cp -p $tarpath/$target/\$prmtr $descmd14/\$prmtr"
    else
     # scp -p -o NoneSwitch=yes \$prmtr $mdest:$descmd14/\$prmtr
     scp -p \$prmtr $mdest:$descmd14/\$prmtr
    fi
  done 
else
 $descmd14 
fi &&
echo "" &&
date | sed -n -e 's/^/0 lnx2be : After the transfer at /p'  &&
ssh $mdest "ls $despath/$Ttarget" > $Tmpfl &&
if [ -f "$Tmpfl" -a -s "$Tmpfl" ] ; then 
  (\rm $Tmpfl || : ) &&
  set -x &&
cd $tarpath &&
$descmd5
  set +x &&
  :
else
  chmod 775 $Tmpfl  &&
  exit 1
fi &&

if [ "$nooffsv" != "yes" ] ; then
# set -x &&
fexfer='' ; export fexfer &&
bexfer=on ; export bexfer &&
cd &&
# cat << woofbesub | $besbmtcmd 
cat << woofbesub | $besbmtcmd &&
#! /bin/sh
# set -x
if [ -n "$CUSTOM_ENV_FILE" -a -s "$CUSTOM_ENV_FILE" ] ; then
 . $CUSTOM_ENV_FILE || :
fi
#
echo "" &&
date | sed -n -e 's/^/0 lnx2be : Back-end job starting at /p'  &&
if [ "$noqsub" != 'yes' ] ; then 
 echo "" && echo "  lnx2be: - \\\`hostname\\\` - transferring $tarpath/$target to $mdest " 
fi &&
cd $despath &&
RUNPATH=$despath &&
export RUNPATH &&
if [ "$targetdir" = "yes" ] ; then
# date | sed -n -e 's/^/0 lnx2be : /p'  &&
  chmod $dvmode $despath/$target &&
  chmod o+rx $despath/$target &&
  cd $despath/$target
fi &&
Tnlst="$Tnlst" &&
chmod $vmode $Tnlst &&
echo "" &&
echo " $despath/$Ttarget : " &&
echo "" &&
ls -al $despath/$Ttarget &&
echo "" &&
if [ "$nooffsv" != "yes" ] ; then
for prmtr in \\\$Tnlst
  do
   lprmtr=\\\`echo \\\$prmtr | tr '[A-Z]' '[a-z]' \\\` &&
   set -x &&
   save \\\$prmtr \\\$lprmtr &&
   release \\\$prmtr &&
   set +x &&
   :
  done &&
  cd $despath &&
  set -x &&
  \\rm -r -f $despath/$Ttarget &&
  set +x  &&
  :
fi &&
if [ "$noqsub" != 'yes' ] ; then
  date | sed -n -e 's/^/0 lnx2be : Back-end job done at /p'  
fi &&
( rmllfls || : ) &&
exit ||
( rmllfls || : ) &&
exit 1 
woofbesub
  set +x &&
 if [ "$noqsub" != 'yes' ] ; then
  echo "" && echo "  lnx2be : $mdest output will be send to $beOutqfl "
 fi
fi &&
( rmllfls || : ) &&
exit ||
# echo " abnormal termination" && exit 2 || exit 3
( rmllfls || : ) &&
exit 4
woofsub
if [ "$noqsub" != 'yes' ] ; then
 date | sed -n -e 's/^/0 lnx2be : Backend job submitted at /p'  &&
 echo "" && echo "  lnx2be: An nqs job is submitted, output will have $Pid suffix" 
fi || exit 5
