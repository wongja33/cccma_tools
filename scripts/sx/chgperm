#! /bin/sh

#    Dec 03/08 - F.Majaess (Revised for sa/saiph, ib/dorval-ib, af/alef)
#    Oct 16/06 - F.Majaess (Revised for ma/maia, ns/naos)
#    Mar 01/06 - F.Majaess (Revised for rg/rigel)
#    Jun 20/03 - F.Majaess (Revised for az/azur)
#    Feb 25/02 - F.Majaess (Revised for ya/yata)
#    Sep 14/00 - F.Majaess (Revised for tu/tsunami)
#    Jan 05/00 - F.Majaess (Added kz/kaze)
#    Jan 21/99 - F.Majaess (Added yo/yonaka)
#    Jul 14/97 - F.Majaess (Added sx/hiru)
#    Mar 18/97 - F.Majaess (Revise for group restructuring)
#    Mar 18/96 - F.Majaess (revise for SX-4)
#    Jun 21/95 - F.Majaess (Added orion, gandalf & aragorn)
#    Nov 08/94 - F.Majaess (Modify for new destination identifiers)
#    Dec 14/92 - E. Chan   
 
#id  chgperm - submits a job to change the permissions of a file 
#id            in the user's run directory   
 
#    AUTHOR - E. Chan  
 
#hd  PURPOSE - "chgperm" submits a job to the destination machine and 
#hd            changes the permissions of the specified file in the 
#hd            user's run directory ('$RUNPATH').
#hd 
#hd            The permission of the specified file is changed simply by using
#hd            the "chmod" command on the targetted machine. This is
#hd            accomplished by passing the "perm" parameter string to the 
#hd            "chmod" command line. Consequently, what is valid for "chmod"
#hd            is also valid for "chgperm".
#hd 
#hd            By default, the file with the highest edition number will be
#hd            affected. The "ed" parameter may be used to specify a 
#hd            particular edition number.
 
#pr  PARAMETERS:
#pr  
#pr    POSITIONAL
#pr 
#pr        perm = parameter string acceptable by chmod command for changing
#pr               file permissions
#pr  
#pr         pfn = name of the file
#pr 
#pr    PRIMARY
#pr
#pr      Note: One of 'ha/sp/po/ca/mz'  in Dorval.
#pr
#pr          ha = to target job to IBM (hadar).
#pr
#pr          sp = to target job to IBM (spica).
#pr
#pr          po = to target job to Linux (pollux).
#pr
#pr          ca = to target job to Linux (castor).
#pr
#pr          mz = to target job to Linux (mez).
#pr
#pr
#pr  fil_grpname= User has the option of specifying one of
#pr               'ccrn_shr/ccrn_stf/ccrn_net/ccrn_rcm/ccrn_mam/ccrn_gst'
#pr               group ownership for targeted file.
#pr               (=$DEFFGRP).
#pr
#pr          ed = edition number of saved file (default: the latest edition)
#pr 
#pr        time = time limit for job in seconds (=200)
#pr  
#pr         mem = memory limit for job in megabytes (=25)
#pr  
 
#ex  EXAMPLE: 
#ex  
#ex  chgperm g+r fil_grpname='ccrn_shr' dmm17sp ha
#ex 
#ex  The above example gives read access for the group to the latest edition 
#ex  of "dmm17sp" in the user's own run directory on Hadar and have the
#ex  target file reside under "ccrn_shr" group ownership.

#  * Obtain any specified option. 

for arg in $@
do
  case $arg in
        -x) set $arg                                       ;;
       *=*) eval $arg                                      ;;
      ed=*) ed=`expr $arg : 'ed=\([0-9]*\)'`               ;;
        ha) mdest=${mdest:='hadar'}                        ;;
        sp) mdest=${mdest:='spica'}                        ;;
        po) mdest=${mdest:='pollux'}                       ;;
        ca) mdest=${mdest:='castor'}                       ;;
        mz) mdest=${mdest:='mez'}                          ;;
         *) tmp1=${perm:+$arg} ; pfn=${pfn:=$tmp1}         ; 
            perm=${perm:=$arg}
  esac
done

#  * Ensure that the filename and the permission string is specified.

if [ -z "$perm" -o -z "$pfn" ] ; then
  echo "Error in chgperm: must specify both filename and permission string"
  exit 1
fi

#  * Set the defaults.

time=${time:=200}
mem=${mem:=25}
nonqs=${nonqs:=$NONQS}
nonqs=${nonqs:='no'}

#  * Prompt for a destination if none was specified.

while [ -z "$mdest" ]
do
 if [ "$SITE_ID" = 'Victoria' ] ; then
   mdest='vicsmall'
 elif [ "$SITE_ID" = 'Dorval' ] ; then
  echo "please enter a destination; ha/sp/po/ca/mz: > \\c"
  read tmdest
  case $tmdest in
      ha) mdest='hadar'                 ;;
      sp) mdest='spica'                 ;;
      po) mdest='pollux'                ;;
      ca) mdest='castor'                ;;
      mz) mdest='mez'                   ;;
       *) echo "illegal destination ! " ;;
  esac
 elif [ "$SITE_ID" = 'Downsview' ] ; then
  mdest="$HOSTID"
 else
  echo "please enter a destination: > \\c"
  read mdest
 fi
done

#  * If edition number is specified remove leading zeros in the edition 
#  * number and ensure that three digits are used. 

if [ -n "$ed" ] ; then
  ed=`expr $ed + 0`
  if [ "$ed" -lt 100 ] ; then
    ed=0$ed
  fi
  if [ "$ed" -lt 10 ] ; then
    ed=0$ed
  fi
fi

#  * Construct and submit the job.

if [ "$mdest" = 'hadar' ] ; then
  outdest=$OUTPUTQ/chgperm_${pfn}.ha.$$
elif [ "$mdest" = 'spica' ] ; then
  outdest=$OUTPUTQ/chgperm_${pfn}.sp.$$
elif [ "$mdest" = 'pollux' ] ; then
  outdest=$OUTPUTQ/chgperm_${pfn}.po.$$
elif [ "$mdest" = 'castor' ] ; then
  outdest=$OUTPUTQ/chgperm_${pfn}.ca.$$
elif [ "$mdest" = 'mez' ] ; then
  outdest=$OUTPUTQ/chgperm_${pfn}.mz.$$
else
  outdest=$OUTPUTQ/chgperm_${pfn}.$$
fi
# fil_grpname=${fil_grpname:='ccrn_shr'}
if [ "$SITE_ID" = 'Dorval' ] ; then
 DEFFGRP=${DEFFGRP:='ccrn_shr'}
 fil_grpname=${fil_grpname:=$DEFFGRP}
fi
if [ -n "$fil_grpname" ] ; then
 Tfil_grpname="fil_grpname=$fil_grpname && export fil_grpname "
else
 Tfil_grpname=" : "
fi


# qsub -q $mdest -lT $time -lM ${mem}mb -r chgperm.$oldpfn \
# qsub -q $mdest -lT $time -lM ${mem}mb -r chgperm \
#                -eo -o $outdest <<endjob
if [ "$nonqs" = 'yes' ] ; then
 if [ "$mdest" = "$HOSTID" ] ; then
  line="nohup sh -s - 1>>$outdest 2>>$outdest & "
 else
  echo "CHGPERM: Sorry, nohup is limited to the same platform"
  echo "         mdest=$mdest; HOSTID=$HOSTID !"
  exit 1
 fi
else
 line="qsub -q $mdest -lT $time -lM ${mem}mb -r chgperm -eo -o $outdest"
fi
#
cd $HOME/tmp
(cat <<endjob) | eval "$line"
#!/bin/sh
#
$Tfil_grpname
cd \$DATAPATH
#
#  * Obtain the absolute path to the desired file.
#
if [ -n "$ed" ] ; then
  line=\`ls -l $pfn.$ed 2>/dev/null\`
else
  line=\`ls -l $pfn.[0-9][0-9][0-9] 2>/dev/null | tail -1\` 
fi
if [ -z "\$line" ] ; then
  echo "Error in chgperm: file $pfn does not exist" 
  exit 1 
fi
#
abspath=\`expr "\$line" : '.* \{1,\}\([^ ]*\)$'\`
#
#  * Change the permission.
#
chmod $perm \$abspath  
if [ -n "\$fil_grpname" ] ; then
 chgrp \$fil_grpname \$abspath
fi
echo "Permissions for \$abspath has been changed"
ls -lg \$abspath
#
( rmllfls || : )
exit
endjob

exit
