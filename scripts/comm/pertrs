#!/bin/sh
set -e

USAGE='
Perturb winds (VORT and DIV) slightly in AGCM restart files.

USAGE

  pertrs rs_input.NNN [ rs_output[.MMM] ]

where

rs_input.NNN is AGCM rs restart file with edition number NNN.

If output restart file is not specified, the new restart file name will be
rs_input.NNP where NNP=NNN+1.

Use "pertrs" for packed ("rs") and unpacked ("rs_nmf") 64-bit restart files.
Use "pertrs_float1" for unpacked ("rs_nmf") 32-bit restart files.
'

if [ $# -eq 0 -o $# -gt 2 ] ; then
  echo "$USAGE"
  exit
fi

script=$0
inp=$1

float1=`basename $script | cut -c7-`
dir=`dirname $inp`
base=`basename $inp`
pfx=`echo $base | sed -e 's/\(.*\)\.\([0-9][0-9][0-9]\)/\1/'`
ed=`echo $base | sed -e 's/\(.*\)\.\([0-9][0-9][0-9]\)/\2/'`

echo inp=$inp
echo dir=$dir base=$base pfx=$pfx ed=$ed

# perturb file
echo "C*PERTURB          1    1.E-15    1" | ccc perturb_winds${float1} $inp $pfx

# save
save $pfx $pfx

# clean up
release $pfx .ccc_cards
