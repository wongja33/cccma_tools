#!/bin/sh

#   Mar 06/12 - F. Majaess (Added support for "USRXBIN")
#   Dec 07/04 - F. Majaess (Abort if "oldiag"/"TSTRDIR" is not accessible)
#   Jan 17/97 - F. Majaess (Enable "oldiag")
#   Dec 16/94 - E. Chan

#id  getpar  - Lists the parmsub and condef parameters of a standard diagnostics
#id            deck.

#hd  PURPOSE - "getpar" extracts a listing of parmsub and condef parameters
#hd            from any standard deck. By default, the deck is assumed to
#hd            reside in the official diagnostics deck directory. 
#hd
#hd            The script assumes that all shell variables used in if/fi 
#hd            conditional blocks are condef parameters and the rest are 
#hd            parmsub parameters. 
#hd 
#hd            *** NOTE: Shell variables which are neither but are used ***
#hd            *** internally within the script cannot be distinguished ***
#hd            *** as such and may be listed as a parmsub or condef     ***
#hd            *** parameter depending on its usage in the script.      ***
#hd            *** Internal shell variables appear in only a few decks, ***
#hd            *** however (mostly in GCM related and a few low level   ***
#hd            *** comdecks), so the problem is not extensive.          *** 
#hd
#hd            "getpar" may also be used on submission jobs. However, if
#hd            shell variables are used outside the deck definition section,
#hd            then they may be misinterpreted as a parmsub or condef 
#hd            parameter. 

#pr  PARAMETERS:
#pr 
#pr    POSITIONAL
#pr 
#pr       deck = name of deck - absolute path is optional (path defaults to 
#pr              official diagnostics deck directory)
#pr 
#pr    SECONDARY
#pr        
#pr        own = switch to process user's own deck (path defaults to
#pr              directory from which the script is executed) 
#pr
#pr      oldiag=Diagnostic library name to use under $CCRNSRC/source$OSbin.
#pr             (=$DEFDIAG).

#ex  EXAMPLES:
#ex 
#ex   1) getpar del oldiag=diagxx
#ex 
#ex      The above example gets the parmsub and condef parameters for the
#ex      deck del.dk using $CCRNSRC/source$OSbin/diagx as a default diagnostic
#ex      library.
#ex
#ex   2) getpar mydeck own
#ex 
#ex      The above example gets the parmsub and condef parameters for the 
#ex      deck mydeck.dk (or mydeck.cdk) in the directory where getpar is
#ex      run.
#ex
#ex   2) getpar $HOME/work/mydeck own
#ex 
#ex      Same as the previous example except that the deck is retrieved
#ex      from the $HOME/work directory.

#  * Obtain the file names and any specified option.

for arg in $@
do
  case $arg in
       -*) set $arg                 ;;
      *=*) eval $arg                ;;
      own) own=on                   ;;
        *) deck=${deck:-$arg}       ;
  esac
done 

#  * Set the path and the name of the deck. The default path is set
#  * to the official deck directory. If the "own" option is used,
#  * then the path defaults to the directory from which the script is
#  * executed. Otherwise, the full path name to the deck must be supplied.

if [ "$own" = on ] ; then
  path=`expr $deck : '\(.*\)/'`
  deck=`expr //$deck : '.*/\(.*\)'`
  path=${path:-`pwd`}
else
  DEFDIAG=${DEFDIAG:=newdiag2}
  oldiag=${oldiag:=$DEFDIAG}
  diaglib=${oldiag:=$DEFDIAG}
  if [ -d "$CCRNSRC/source${OSbin}/$diaglib/." ] ; then
   path=$CCRNSRC/source${OSbin}/$diaglib
  else
   echo "\ngetpar: Unable to continue due to $CCRNSRC/source${OSbin}/$diaglib subdirectory is not accessible!\n"
   exit 1
  fi
fi
if [ "$USRTSTR" = 'true' -a -n "$TSTRDIR" -a ! -d "$TSTRDIR/." ] ; then
 echo "\ngetpar: Unable to continue due to $TSTRDIR subdirectory is not accessible!\n"
 exit 2
fi
. usrxbin_split
if [ -n "$USRXBIN1" -a ! -d "$USRXBIN1/." ] ; then
 echo "\ngetpar: Unable to continue due to $USRXBIN1 subdirectory is not accessible!\n"
 exit 2
fi
if [ -n "$USRXBIN2" -a ! -d "$USRXBIN2/." ] ; then
 echo "\ngetpar: Unable to continue due to $USRXBIN2 subdirectory is not accessible!\n"
 exit 2
fi

#  * Check to see if the deck exists.

if [ -f $path/$deck ] ; then
  file=$deck
elif [ -f $path/$deck.*dk ] ; then
  file="${deck}.*dk"
elif [ "$USRTSTR" = 'true' -a -n "$TSTRDIR" -a -d "$TSTRDIR/." -a -s $TSTRDIR/$deck ] ; then
  file=$deck
  path=$TSTRDIR
elif [ "$USRTSTR" = 'true' -a -n "$TSTRDIR" -a -d "$TSTRDIR/." -a -s $TSTRDIR/$deck.*dk ] ; then
  file="${deck}.*dk"
  path=$TSTRDIR
elif [ -n "$USRXBIN1" -a -d "$USRXBIN1/." -a -s $USRXBIN1/$deck ] ; then
  file=$deck
  path=$USRXBIN1
elif [ -n "$USRXBIN1" -a -d "$USRXBIN1/." -a -s $USRXBIN1/$deck.*dk ] ; then
  file="${deck}.*dk"
  path=$USRXBIN1
elif [ -n "$USRXBIN2" -a -d "$USRXBIN2/." -a -s $USRXBIN2/$deck ] ; then
  file=$deck
  path=$USRXBIN2
elif [ -n "$USRXBIN2" -a -d "$USRXBIN2/." -a -s $USRXBIN2/$deck.*dk ] ; then
  file="${deck}.*dk"
  path=$USRXBIN2
else
  echo "Error: deck $path/$deck does not exist"
  exit 1
fi

#  * Move into a temporary directory to execute the script.

Stamp="${HOSTID}_"`date +%Y%j%H%M%S`
mkdir $HOME/tmp/tmp_getpar_$$_$Stamp
cd $HOME/tmp/tmp_getpar_$$_$Stamp

#  * Extract out all parmsub parameters into the temporary file 
#  * 'parmsub' and all condef parameters into the temporary file 
#  * 'condef'. Condef parameters are distinguished from parmsub
#  * parmameters by their appearance in if-fi blocks. Note, however,
#  * that there is no way to distinguish internal shell variables 
#  * from parmsub or condef parameters. 

sed -n '

: label
/ *if *\[/!s/\(.*\)\$[{]*\([A-Za-z0-9_]\{1,\}\).*/\2%%PARMSUB%%\
\1/
/ *if *\[/s/\(.*\)\$[{]*\([A-Za-z0-9_]\{1,\}\).*/\2%%CONDEF%%\
\1/
t label
s/\(.*\)%%PARMSUB%%.*$/\1%%PARMSUB%%/
s/\(.*\)%%CONDEF%%.*$/\1%%CONDEF%%/
s/%%PARMSUB%%//gw parmsub
s/%%CONDEF%%//gw condef

' $path/$file

#s/\\\$//

#  * Print out sorted parameter lists.

echo 
echo _______________________________ $deck _______________________________
echo
echo "Sorted list of parmsub parameters"
echo
cat parmsub | sort -u
echo
echo "Sorted list of condef parameters"
echo
cat condef | sort -u
echo
echo _______________________________ $deck _______________________________
echo 
echo Warning: Internal shell variables which are neither parmsub nor 
echo          condef parameters may be mis-interpreted as one or the
echo          other. Use of internal shell variables, however, is 
echo          is restricted to the gcm decks and a few low level 
echo          common decks. 
echo

cd ..
rm -r tmp_getpar_$$_$Stamp

exit
