#! /bin/sh

#    May 31/2004 - F.Majaess

#id arc_fmt - Creates CCCma "arc_list" format from "arcfile".

#   AUTHOR  - F.Majaess

#hd PURPOSE - "arc_fmt" code is used to create a file to update
#hd           "arc_dir" from "cmcarc" listing output of "arcfile".
#hd            Note: The script is not intended to be called directly by
#hd                  the user, instead it's to be called by higher level
#hd                  scripts.

#pr PARAMETERS:
#pr
#pr   POSITIONAL
#pr
#pr     arcfile   = "cmcarc" target filename.
#pr                 (must be specified in lower case and terminates 
#pr                  with "_arc" suffix; may contain path information).

#ex EXAMPLE:
#ex  
#ex     arc_fmt mc_abc_001_arc

IFS=' '
export IFS
# set -x

#  * Set variable 'AWK' to the version of "awk" to be used.

AWK=${AWK:='awk'}

#  * Obtain the list of file(s) to be archived.

arg_list=$@
for arg in $arg_list
do
  case $arg in
       -*) set $arg                    ;;
      *=*) eval "$arg"                 ;;
        *) arcfile=${arcfile:-$arg}
  esac
done

Arc_extnsn=`basename $arcfile | sed -n -e 's/^.*_arc$/Yes/p'`
barcfile=`basename $arcfile | tr '[A-Z]' '[a-z]'`
if [ -n "$arcfile" -a -s "$arcfile" -a "$Arc_extnsn" = 'Yes' -a `basename $arcfile` = "$barcfile" ] ; then
 # set -x
 
 # Obtain "cmcarc" version number, date and time ...
 # cmcarc_ver=`cmcarc -h | sed -n -e '/^cmcarc version /p' | $AWK '{print $3}'`
 cmcarc_ver=`cmcarc.64 -h | sed -n -e '/^cmcarc version /p' | $AWK '{print $3}'`
 # ndatetime=`date -u '+%Y%j%H%M%S'`
 Date=${Date:-`date -u '+%Y%j'`}
 Time=${Time:-`date -u '+%H%M%S'`}

 # Generate the formatted listing...

 # cmcarc -f $arcfile -t -v | sed -n -e '3,$p' | $AWK -v linenum=0 -v Date="$Date" -v Time="$Time" -v arcfile="$barcfile" -v cmcarc_ver="$cmcarc_ver" '{ linenum = ++linenum ; printf "%-40s%1s%8d%6.6d%1s%-40s%1s%4.3d%1s%4s%1s%12d\n", $9," ",Date,Time," ",arcfile," ",linenum," ",cmcarc_ver," ",$4 ; }'
 # cmcarc -f $arcfile -t -v | sed -n -e '3,$p' | $AWK -v linenum=0 -v Date="$Date" -v Time="$Time" -v arcfile="$barcfile" -v cmcarc_ver="$cmcarc_ver" '{ linenum = ++linenum ; printf "%-40s%1s%8d%6.6d%1s%-40s%1s%4.3d%1s%4s%1s%12s\n", $9," ",Date,Time," ",arcfile," ",linenum," ",cmcarc_ver," ",$4 ; }'
 cmcarc.64 -f $arcfile -t -v | sed -n -e '3,$p' | $AWK -v linenum=0 -v Date="$Date" -v Time="$Time" -v arcfile="$barcfile" -v cmcarc_ver="$cmcarc_ver" '{ linenum = ++linenum ; printf "%-40s%1s%8d%6.6d%1s%-40s%1s%4.3d%1s%4s%1s%12s\n", $9," ",Date,Time," ",arcfile," ",linenum," ",cmcarc_ver," ",$4 ; }'

fi
