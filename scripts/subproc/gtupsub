#!/bin/sh

#    Jun 16/03 - F.Majaess (Adjust for "$HOME/bin${OSbin}" changes)
#    Jun 11/99 - F.Majaess (Added "sublib").
#    Nov 06/96 - F.Majaess (Ensure only the equivalent of one 
#                           subroutine copy is used in case of 
#                           the existence of multiple copies for 
#                           a subroutine).
#    Sep 11/96 - F.Majaess (Adjust path massaging for Victoria)
#    May 05/94 - E. Chan   (Support both upper/lower case directives)
#    Jul 29/93 - E. Chan   (Add lnum option)
#    May 26/93 - E. Chan   (Modify to use new in-house version of update)
#    Sep 21/92 - E. Chan   (Add option to pass parameters to Bourne shell 
#                           'set' command)
#    Jan 12/92 - M. Lazare.

#id  gtupsub - job to produce set of in-line code from a subroutine update file. 

#    AUTHOR  - M. Lazare

#hd  PURPOSE - "gtupsub produces a set of in-line code suitable for compiling 
#hd            from a file containing subroutine update directives for one or
#hd            more routines. This is necessary owing to the presence of many
#hd            files containing these sections in various COS jobs, keeping
#hd            in mind that there is no longer an update file of subroutines 
#hd            in the HNSX/MIPS Unix world.
#hd 

#pr  PARAMETERS:
#pr 
#pr    POSITIONAL
#pr 
#pr       infil = name of file containing subroutine updates
#pr      outfil = name of resulting file having in-lined code, suitable
#pr               for compiling
#pr
#pr      sublib = (optional); subdirectory residing ( not necessarily immediately)
#pr               under $CCRNSRC/source$OSbin/$lib to start searching for the 
#pr               needed subroutine source code from instead of the default 
#pr               $CCRNSRC/source$OSbin/$lib.
#pr               (i.e. if sublib=gcm6u; target search subdirectory
#pr                     will be $CCRNSRC/source$OSbin/lssub/.../gcm6u )
#pr 
#pr    PRIMARY
#pr 
#pr        lib  = subdirectory in $CCRNSRC/source$OSbin to start search from.
#pr               (=lssub)
#pr 
#pr    SECONDARY
#pr  
#pr        lnum = add update identification line numbers to the in-lined code
#pr

#ex  EXAMPLE:
#ex  
#ex   1) gtupsub upin upout
#ex  
#ex      The above example uses the update directives for subroutines in
#ex      the file "upin" to produce a file of compilable updated source
#ex      code in the file "upout".

#  * Reset field separators (otherwise those defined in the parent process will
#  * be used and these may cause problems if they include special characters 
#  * used in this script).

IFS=' '
export IFS

#  * Obtain the file names.

arg_list=$@
for arg in $arg_list
do
  case $arg in
	   -*) set $arg                                       ;;
         lnum) lnum=list                                      ;;
        lib=*) eval $arg                                      ;;
       *) tmp1=${infil:+$arg} ; tmp2=${outfil:+$arg}         ;
          sublib=${sublib:=$tmp2} ; outfil=${outfil:=$tmp1}  ;
          infil=${infil:=$arg}
  esac
done

#  * Exit if no value set for infil.

if [ -z "$infil" ] ; then
  echo "No value specified for infil"
  exit 1
fi

#  * Exit if no value set for outfil.

if [ -z "$outfil" ] ; then
  echo "No value specified for outfil"
  exit 2
fi

lib=${lib:='lssub'}

#  * Make and move into a temporary working directory.

cwd=`pwd`
if [ "$SITE_ID" = 'Victoria' ] ; then
 cwd=`echo $cwd | sed "
  s/^\/HOME\/$USR\/$USR/\/home\/$USR/
  s/^\/tmp_mnt\/home.*\/$USR/\/home\/$USR/
  s/^\/tmp_mnt\/home\/$USER/\/home/
  s/^\/nfs\/cs..//
  s/^\/nfs\/sp..s//
  /^\/tmp_mnt\/net\/cs..-fddinet/{
   s/\/$USR\/$USR/\/$USR/g
   s/\/$USR\/$USR/\/$USR/g
   s/^\/tmp_mnt\/net\/cs..-fddinet//
                                 }
  /^\/tmp_mnt\/net\/sp..s/{
   s/\/$USR\/$USR/\/$USR/g
   s/\/$USR\/$USR/\/$USR/g
   s/^\/tmp_mnt\/net\/sp..s//
                                 }
  s/^\/tmp_mnt\/net\/ss..\/home\/gjb/\/home\/rgb\/sunhome/
  s/^\/net\/ss..\/home\/gjb/\/home\/rgb\/sunhome/" `
fi
mkdir -m 755 tmp_gtupsub$$
cd tmp_gtupsub$$

#  * Scan update directives in the input file and generate a list
#  * of the subroutines required.

file_list=`sed -n '

s/^[*%][^ ]*[Dd][Ee][Cc][Kk]//
s/^[*%][^ ]\{1,\}[FfLlDd]//
s/%c /%C /
s/%d /%D /
s/%I /%I /
s/^[*%]\///
/^[*%]C /s/,/\
/g
s/^[*%][^ ]* \{1,\}\([^ .]*\).*/\1/p

' ../$infil | sort -u | tr [A-Z] [a-z] | tr '\\012' '\\040'`

#  * Get a copy of each required subroutine.  Each routine is converted 
#  * into an update deck.

mkdir -m 755 ulib
cd ulib
ulib=`pwd`
if [ "$SITE_ID" = 'Victoria' ] ; then
 ulib=`echo $ulib | sed "
  s/^\/HOME\/$USR\/$USR/\/home\/$USR/
  s/^\/tmp_mnt\/home.*\/$USR/\/home\/$USR/
  s/^\/tmp_mnt\/home\/$USER/\/home/
  s/^\/nfs\/cs..//
  s/^\/nfs\/sp..s//
  /^\/tmp_mnt\/net\/cs..-fddinet/{
   s/\/$USR\/$USR/\/$USR/g
   s/\/$USR\/$USR/\/$USR/g
   s/^\/tmp_mnt\/net\/cs..-fddinet//
                                 }
  /^\/tmp_mnt\/net\/sp..s/{
   s/\/$USR\/$USR/\/$USR/g
   s/\/$USR\/$USR/\/$USR/g
   s/^\/tmp_mnt\/net\/sp..s//
                                 }
  s/^\/tmp_mnt\/net\/ss..\/home\/gjb/\/home\/rgb\/sunhome/" `
fi

#  * Set target search directory.

if [ -n "$sublib" ] ; then
  libpath=`find -L $CCRNSRC/source${OSbin}/${lib}/. -name $sublib -print -depth 2>/dev/null `
  if [ -z "$libpath" -o \( -n "$libpath" -a ! -d "$libpath" \) ] ; then
    echo "Gtupsub: unable to find --> $sublib <-- subdirectory under $CCRNSRC/source${OSbin}/$lib !"
    exit 1
  fi
else
  libpath="$CCRNSRC/source${OSbin}/$lib"
fi

for file in $file_list
do

  deck=`echo $file 2>/dev/null  | tr [a-z] [A-Z]`
  find -L ${libpath}/. -name $file.f -exec cp {} $file.dk 2>/dev/null \;
  if [ -s $file.dk ] ; then
   chmod u+w $file.dk
  else
   find -L $CCRNSRC/source${OSbin}/lssub/comm/. -name $file.f -exec cp {} $file.dk \;
   if [ -s $file.dk ] ; then
    chmod u+w $file.dk
   fi
  fi
# if [ "$USRTSTR" = 'true' -a -n "$TSTRDIR" -a -d "$TSTRDIR" ] ; then
#  find -L ${TSTRDIR}/. -name $file.f -exec cp {} $file.dk \;
#  if [ -s $file.dk ] ; then
#   chmod u+w $file.dk
#  fi
# fi
# OSbin=${OSbin-`ostarget bin`}
# find -L $HOME/bin${OSbin}/. -name $file.f -exec cp {} $file.dk \;
  if [ -s $file.dk ] ; then
   chmod u+w $file.dk
   eval "( \ed -s $file.dk << ..ended
1i
%DECK $deck
.
w
q
..ended
         ) "
  fi

done

#  * Finally apply the updates to the subroutines and put into the
#  * output file.

cd ..
update $lnum usrlib $cwd/$infil $ulib outfil

#  * Clean up files and exit working directory.

mv outfil ../$outfil
cd ..
# rm -r tmp_gtupsub$$

exit
