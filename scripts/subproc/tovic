#! /bin/sh
 
#    Oct 27/04 - F.Majaess (Merge Victoria changes into this version)
#    Jun 09/98 - F.Majaess (Revised to call "gnhdcpy" for hardcopy)
#    May 26/94 - F.Majaess (Added "print" option)
#    Oct 12/93 - F.Majaess (Adjust for new "metacod" path)
#    Sep 07/93 - F.Majaess (enforce routing via "ftp1" batch line)
#    May 20/93 - F.Majaess (Based on "tosun" script)
 
#id  tovic   - Transfer a file from current machine to Victoria filesystem.
 
#    AUTHOR  - F.Majaess
 
#hd  PURPOSE - "tovic" script is used to transfer "ifile" from current machine
#hd            /data/.../metacod subdirectory to "$pldir" if specified,
#hd            otherwise to /plots/"SUBD"/"ofile" on 
#hd            Victoria filesystem. 
#hd            The data transfer type can be in character or binary depending
#hd            on "nobin" parameter value. Also, the decision to delete 
#hd            "ifile" is based on "del" parameter value.
 
#pr  PARAMETERS:
#pr
#pr    POSITIONAL     
#pr
#pr      pldir  = Optional final destination "plots" subdirectory full path.
#pr      ifile  = name of the file to be transfered. 
#pr      SUBD   = user's subdirectory in /plots on CCRN server where 
#pr               the transfered file is to be saved.
#pr      ofile  = name under which the transfered file is to be saved.
#pr      subpath= path to the subdirectory containing "ifile".
#pr               (=/data/ccrn/gen/metacod)
#pr
#pr    PRIMARY/SECONDARY
#pr
#pr      del    = switch controlling deletion of input file (=no/yes)
#pr      nobin  = switch controlling type of transfer (=no/yes)
#pr               (default is binary).
#pr      print  = switch controls producing hardcopy (=no/yes)
 
#ex  EXAMPLE: 
#ex
#ex tovic ifile=metafile ofile=myplots SUBD=UXXX 

#  diagnostic codes...

#  echo $@  
#  set -e
#  set -xv
 
#  *  check if switches/parameters specified on script call
 
if [ $# -gt 0 ] ; then
  
#  * put argument list in "prmtrl" ...
   
  prmtrl=$@
  
#  * for each argument specified do...
    
  for prmtr in $prmtrl
  do
    case $prmtr in
 
#   * check for standard shell switches and set them accordingly
 
     --|-|[-+][aefhkntuvx]) set `echo $prmtr ` ;;
 
#   * case where print is specified.
 
    print ) print='yes';;

#   * case where a parameter is to be assigned a value
 
    *=*   ) eval $prmtr ;;
 
#   * anything else case skip...
 
    *  )  ;;
 
    esac
  done
 
fi

# ensure all used parameters without a value are set to the appropriate
# default value...
 
nobin=${nobin='no'}
del=${del='no'}
print=${print='no'}
ifile=${ifile='xxxx'}
ofile=${ofile='yyyy'}
SUBD=${SUBD='uall'}
subpath=${subpath='/data/ccrn/gen/metacod'}

# translate upper-case characters to lower-case...
 
SUBD=`echo $SUBD | tr '[A-Z]' '[a-z]'`

# Temporary fix to allow sending "y01" plots 
# to "/plots/u106" in Victoria.

if [ "$SITE_ID" = 'Dorval' -a "$SUBD" = 'uy01' ] ; then
 SUBD='u106'
fi

if [ -z "$pldir" ] ; then
 pldir="/plots/$SUBD"
fi
 
 cd $subpath 
 chmod g+rw $ifile
if [ "$SITE_ID" = 'Victoria' ] ; then
 set -x
 if [ "$subpath/$ifile" != "$pldir/$ofile" ] ; then
  \mv $ifile $pldir/$ofile
 fi
 chmod g+rw $pldir/$ofile
 if [ -f "$ifile.AAASOVL" ] ; then 
# \mv $ifile.AAASOVL $pldir/$ofile.AAASOVL
# chmod g+rw $pldir/$ofile.AAASOVL
  \rm -f $pldir/$ofile.AAASOVL
 fi
 if [ "$print" = 'yes' ] ; then
#  rsh ftp1.cccma.bc.ec.gc.ca "cd $pldir ; psframe -g $ofile  ; \rm $pldir/$ofile "
#  rsh sp10.cccma.bc.ec.gc.ca "/home/acrnsrc/scripts/comm/gnhdcpy metafil=$ofile subd=$SUBD rmv=yes"
#  rsh ftp1.cccma.bc.ec.gc.ca "/home/acrnsrc/scripts/comm/gnhdcpy metafil=$ofile subd=$SUBD rmv=yes"
#  rsh ftp1.cccma.bc.ec.gc.ca "~acrnsrc/scripts/comm/gnhdcpy metafil=$ofile subd=$SUBD rmv=yes"
#  rsh ftp1.cccma.ec.gc.ca "~acrnsrc/scripts/comm/gnhdcpy metafil=$ofile subd=$SUBD rmv=yes"
   ssh ftp1.cccma.ec.gc.ca "~acrnsrc/scripts/comm/gnhdcpy metafil=$ofile subd=$SUBD rmv=yes"
 fi && 
 exit || exit 1
fi


#   ****   Task of the script...   ****
#   * setup temporary variables holding directives based on "nobin" 
#   * and "del" values specified...
 
if [ $nobin = 'yes' ] ; then 
  tmode='ascii' ;
else
  tmode='binary' ;
# if [ -z "$PATH" ] ; then
#   PATH=.:/bin:/usr/bin:/usr/bsd:/usr/sbin:/usr/local/bin:/bin:/usr ;
#   export PATH
# fi
fi
if [ -f "$ifile.AAASOVL" ] ; then 
#  ftpcmd="put $ifile.AAASOVL $pldir/$ofile.AAASOVL"
   ftpcmd='ascii'
else
   ftpcmd='ascii'
fi
 
 
#   * Assemble and transfer the file to CCRN file server...
 
Zzzdmy="Zzdmyz.tovic.$ifile.$Tpid"
Zzzdmyx="Zzdmyzx.tovic.$ifile.$Tpid"
(\rm -f $Zzzdmyx || : )
# /usr/ucb/ftp -v $VICSV01 << woof
# /usr/ucb/ftp -v cs01.cccma.bc.ec.gc.ca << woof
# /usr/ucb/ftp -v ftp1.cccma.bc.ec.gc.ca << woof
# ftp -v ftp1.cccma.bc.ec.gc.ca << woof
# ftp -i -v sp10.cccma.bc.ec.gc.ca << woof
# ftp -i -v ftp1.cccma.bc.ec.gc.ca << woof
if [ "$SITE_ID" = 'Dorval' -a "$OS" = 'Linux' ] ; then
 HOSTID=${HOSTID:=`hostname| cut -d'.' -f1 `}
 HOSTIDf=`echo $HOSTID | cut -c 1-3`
 if [ "$HOSTIDf" = 'ib3' ] ; then
  ssh pollux "cd $subpath; scp -p -o NoneSwitch=yes $ifile ftp1.cccma.ec.gc.ca:$pldir/$ofile" &&
  ssh pollux "ssh ftp1.cccma.ec.gc.ca \ls -lds $pldir/$ofile" > $subpath/$Zzzdmyx
 elif [ "$HOSTIDf" = 'ib4' ] ; then
  ssh castor "cd $subpath; scp -p -o NoneSwitch=yes $ifile ftp1.cccma.ec.gc.ca:$pldir/$ofile" &&
  ssh castor "ssh ftp1.cccma.ec.gc.ca \ls -lds $pldir/$ofile" > $subpath/$Zzzdmyx
 elif [ "$HOSTIDf" = 'ib8' ] ; then
  ssh mez "cd $subpath; scp -p -o NoneSwitch=yes $ifile ftp1.cccma.ec.gc.ca:$pldir/$ofile" &&
  ssh mez "ssh ftp1.cccma.ec.gc.ca \ls -lds $pldir/$ofile" > $subpath/$Zzzdmyx
 elif [ "$HOSTIDf" = 'jou' ] ; then
  cd $subpath; scp -p -o NoneSwitch=yes $ifile ftp1.cccma.ec.gc.ca:$pldir/$ofile &&
  ssh ftp1.cccma.ec.gc.ca "\ls -lds $pldir/$ofile" > $subpath/$Zzzdmyx
 else
  ssh dorval-ib "cd $subpath; scp -p -o NoneSwitch=yes $ifile ftp1.cccma.ec.gc.ca:$pldir/$ofile" &&
  ssh dorval-ib "ssh ftp1.cccma.ec.gc.ca \ls -lds $pldir/$ofile" > $subpath/$Zzzdmyx
 fi
else
## ftp -i -v ftp1.cccma.ec.gc.ca << woof
## binary
## del $pldir/$ofile 
## put $ifile $pldir/$ofile
## ascii
## $ftpcmd
## ls $pldir/$ofile $Zzzdmy
## quit
## woof
 scp -p -o NoneSwitch=yes $ifile ftp1.cccma.ec.gc.ca:$pldir/$ofile &&
 ssh ftp1.cccma.ec.gc.ca "\ls -lds $pldir/$ofile" > $subpath/$Zzzdmyx
fi || \rm -f $Zzzdmyx
if [ -f "$Zzzdmyx" ] ; then
 chmod ug+rw $Zzzdmyx
 Fsz=`cat $Zzzdmyx | tail -1 | $AWK '{ print $1 ; }'` 
 if [ -n "$Fsz" ] ; then
  if [ "$Fsz" -gt 0 ] ; then
   Ok2del='yes'
   :
  else
   \rm -f $Zzzdmyx
  fi
 fi
fi
if [ -f "$Zzzdmyx" ] ; then 
 if [ -s "$Zzzdmyx" ] ; then 
   if [ $del = 'yes' -a "$Ok2del" = 'yes' ] ; then 
    \rm -f $ifile
    if [ -f "$ifile.AAASOVL" ] ; then 
      \rm -f $ifile.AAASOVL
    fi
   fi
   [ "$Ok2del" = 'yes' ] && \rm -f $Zzzdmy || :
   [ "$Ok2del" = 'yes' ] && \rm -f $Zzzdmyx || :
   if [ "$print" = 'yes' ] ; then
    if [ "$SITE_ID" = 'Dorval' -a "$OS" = 'Linux' ] ; then
     HOSTID=${HOSTID:=`hostname| cut -d'.' -f1 `} &&
     HOSTIDf=`echo $HOSTID | cut -c 1-3` &&
     Cmd='~acrnsrc/scripts/comm/gnhdcpy'" metafil=$ofile subd=$SUBD rmv=yes" 
     if [ "$HOSTIDf" = 'ib3' ] ; then
      eval ssh pollux 'ssh ftp1.cccma.ec.gc.ca "$Cmd"'
     elif [ "$HOSTIDf" = 'ib4' ] ; then
      eval ssh castor 'ssh ftp1.cccma.ec.gc.ca "$Cmd"'
     elif [ "$HOSTIDf" = 'ib8' ] ; then
      eval ssh mez 'ssh ftp1.cccma.ec.gc.ca "$Cmd"'
     elif [ "$HOSTIDf" = 'jou' ] ; then
     #eval ssh ftp1.cccma.ec.gc.ca "$Cmd"
      ssh ftp1.cccma.ec.gc.ca "~acrnsrc/scripts/comm/gnhdcpy metafil=$ofile subd=$SUBD rmv=yes"
     else
      eval ssh dorval-ib 'ssh ftp1.cccma.ec.gc.ca "$Cmd"'
     fi
    else
#    rsh ftp1.cccma.bc.ec.gc.ca "cd $pldir ; psframe -g $ofile  ; \rm $pldir/$ofile "
#    rsh sp10.cccma.bc.ec.gc.ca "/home/acrnsrc/scripts/comm/gnhdcpy metafil=$ofile subd=$SUBD rmv=yes"
#    rsh ftp1.cccma.bc.ec.gc.ca "/home/acrnsrc/scripts/comm/gnhdcpy metafil=$ofile subd=$SUBD rmv=yes"
#    rsh ftp1.cccma.bc.ec.gc.ca "~acrnsrc/scripts/comm/gnhdcpy metafil=$ofile subd=$SUBD rmv=yes"
#    rsh ftp1.cccma.ec.gc.ca "~acrnsrc/scripts/comm/gnhdcpy metafil=$ofile subd=$SUBD rmv=yes"
     ssh ftp1.cccma.ec.gc.ca "~acrnsrc/scripts/comm/gnhdcpy metafil=$ofile subd=$SUBD rmv=yes"
    fi
   fi && 
   exit || exit 1
 else
  #echo "$@ USER=$USER" >> $Zzzdmy
   echo "tovic $@ USER=$USER" >> $Zzzdmy
  #chown acrnsrc $Zzzdmy
   chmod g+r $Zzzdmy
   exit 1
 fi
fi
exit 2
# open 131.235.100.51
