      module cdftype
c***********************************************************************
c Netcdf related type definitions
c
c $Id$
c***********************************************************************

        !--- netcdf attribute type
        type cdf_att_t
          character(512)            :: name
          integer                   :: vid=-1
          integer                   :: xtype=0
          integer                   :: length=0
          character(4096)           :: char_vals=" "
          integer(1), pointer       :: byte_vals(:)
          integer(2), pointer       :: short_vals(:)
          integer(4), pointer       :: int_vals(:)
          real(4), pointer           :: float_vals(:)
          real(8), pointer           :: double_vals(:)
        end type cdf_att_t

        !--- netcdf variable type
        type cdf_var_t
          character(512)            :: name
          integer                   :: vid=-1
          integer                   :: xtype=0
          logical                   :: iscoord=.false.
          integer, pointer          :: dimids(:)
          integer                   :: ndims=0
          integer, pointer          :: shape(:)
          integer                   :: natts=0
          type(cdf_att_t), pointer  :: attribs(:)
          !--- For coordinate variables, one of these arrays will be assigned
          !--- otherwise no space will be allocated
          !--- note that there is no char type available
          integer(1), pointer       :: byte_vals(:)
          integer(2), pointer       :: short_vals(:)
          integer(4), pointer       :: int_vals(:)
          real(4), pointer          :: float_vals(:)
          real(8), pointer          :: double_vals(:)
        end type cdf_var_t

      CONTAINS

      SUBROUTINE init_cdf_att_t(att)
        type(cdf_att_t) :: att
        att % name      = ' '
        att % vid       = -1
        att % xtype     = 0
        att % length    = 0
        att % char_vals = " "
        nullify(att % byte_vals)
        nullify(att % short_vals)
        nullify(att % int_vals)
        nullify(att % float_vals)
        nullify(att % double_vals)
      END SUBROUTINE init_cdf_att_t

      SUBROUTINE init_cdf_var_t(var)
        type(cdf_var_t) :: var
        var % name    = ' '
        var % vid     = -1
        var % xtype   = 0
        var % iscoord = .false.
        var % ndims   = 0
        var % natts   = 0
        nullify(var % dimids)
        nullify(var % shape)
        nullify(var % attribs)
        nullify(var % byte_vals)
        nullify(var % short_vals)
        nullify(var % int_vals)
        nullify(var % float_vals)
        nullify(var % double_vals)
      END SUBROUTINE init_cdf_var_t

      SUBROUTINE copy_cdf_att_t(att_in, att_out)
        type(cdf_att_t) :: att_in, att_out
        att_out % name      = att_in % name
        att_out % vid       = att_in % vid
        att_out % xtype     = att_in % xtype
        att_out % length    = att_in % length
        att_out % char_vals = att_in % char_vals
        if (associated( att_in%byte_vals )) then
          att_out%byte_vals => att_in%byte_vals
        endif
        if (associated( att_in%short_vals )) then
          att_out%short_vals => att_in%short_vals
        endif
        if (associated( att_in%int_vals )) then
          att_out%int_vals => att_in%int_vals
        endif
        if (associated( att_in%float_vals )) then
          att_out%float_vals => att_in%float_vals
        endif
        if (associated( att_in%double_vals )) then
          att_out%double_vals => att_in%double_vals
        endif
      END SUBROUTINE copy_cdf_att_t

      SUBROUTINE copy_cdf_var_t(var_in, var_out)
        type(cdf_var_t) :: var_in, var_out
        var_out % name = var_in % name
        var_out % vid= var_in % vid
        var_out % xtype=  var_in % xtype
        var_out % iscoord=  var_in % iscoord
        if (associated( var_in%dimids )) then
          var_out%dimids => var_in%dimids
        endif
        var_out % ndims=  var_in % ndims
        nullify(var_out % shape)
        var_out % natts=  var_in % natts
        if (associated( var_in%attribs )) then
          var_out%attribs => var_in%attribs
        endif
        if (associated( var_in%byte_vals )) then
          var_out%byte_vals => var_in%byte_vals
        endif
        if (associated( var_in%short_vals )) then
          var_out%short_vals => var_in%short_vals
        endif
        if (associated( var_in%int_vals )) then
          var_out%int_vals => var_in%int_vals
        endif
        if (associated( var_in%float_vals )) then
          var_out%float_vals => var_in%float_vals
        endif
        if (associated( var_in%double_vals )) then
          var_out%double_vals => var_in%double_vals
        endif
      END SUBROUTINE copy_cdf_var_t

      SUBROUTINE print_cdf_att_t(attrib)
        type(cdf_att_t) :: attrib
        character(32) :: indent=" "
        integer :: nsp=4
        write(6,*)indent(1:nsp),
     &    '=============== ATTRIBUTE =================='
        write(6,*)indent(1:nsp),'   name = ',trim(attrib%name)
        write(6,*)indent(1:nsp),'    vid = ',attrib%vid
        write(6,*)indent(1:nsp),'  xtype = ',attrib%xtype
        write(6,*)indent(1:nsp),' length = ',attrib%length
        if (len_trim(attrib%char_vals).gt.0) then
          write(6,*)indent(1:nsp),'TEXT :: ',trim(attrib%char_vals)
        endif
        if (associated(attrib%byte_vals)) then
          write(6,*)indent(1:nsp),'BYTE :: '
          write(6,'(20i4)')attrib%byte_vals
        endif
        if (associated(attrib%short_vals)) then
          write(6,*)indent(1:nsp),'SHORT :: '
          write(6,'(10i8)')attrib%short_vals
        endif
        if (associated(attrib%int_vals)) then
          write(6,*)indent(1:nsp),'INT :: '
          write(6,'(8i10)')attrib%int_vals
        endif
        if (associated(attrib%float_vals)) then
          write(6,*)indent(1:nsp),'FLOAT :: '
          write(6,'(1p6e12.3)')attrib%float_vals
        endif
        if (associated(attrib%double_vals)) then
          write(6,*)indent(1:nsp),'DOUBLE :: '
          write(6,'(1p6d12.3)')attrib%double_vals
        endif
        write(6,*)indent(1:nsp),
     &    '============================================'
      END SUBROUTINE print_cdf_att_t

      SUBROUTINE print_cdf_var_t(var)
        type(cdf_var_t) :: var
        integer :: n
        write(6,*)'=============== VARIABLE =================='
        write(6,*)'   name = ',trim(var%name)
        write(6,*)'    vid = ',var%vid
        write(6,*)'  xtype = ',var%xtype
        write(6,*)'iscoord = ',var%iscoord
        write(6,*)'  ndims = ',var%ndims
        if (associated(var%dimids)) then
          write(6,*)' dimids = ',var%dimids
        endif
        if (associated(var%shape)) then
          write(6,*)'  shape = ',var%shape
        endif
        write(6,*)'  natts = ',var%natts
        do n=1,var%natts
          call print_cdf_att_t(var%attribs(n))
        enddo

        if (associated(var%byte_vals)) then
          write(6,'(20i4)')var%byte_vals
        endif
        if (associated(var%short_vals)) then
          write(6,'(10i8)')var%short_vals
        endif
        if (associated(var%int_vals)) then
          write(6,'(8i10)')var%int_vals
        endif
        if (associated(var%float_vals)) then
          write(6,'(1p6e12.3)')var%float_vals
        endif
        if (associated(var%double_vals)) then
          write(6,'(1p6d12.3)')var%double_vals
        endif
        write(6,*)'============================================'
      END SUBROUTINE print_cdf_var_t

      end module cdftype
