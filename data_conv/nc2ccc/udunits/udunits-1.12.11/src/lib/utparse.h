/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_UT_UTPARSE_TAB_H_INCLUDED
# define YY_UT_UTPARSE_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int utdebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    INT = 258,
    ERR = 259,
    SHIFT = 260,
    SPACE = 261,
    MULTIPLY = 262,
    DIVIDE = 263,
    EXPONENT = 264,
    REAL = 265,
    NAME = 266,
    DATE = 267,
    TIME = 268,
    ZONE = 269
  };
#endif
/* Tokens.  */
#define INT 258
#define ERR 259
#define SHIFT 260
#define SPACE 261
#define MULTIPLY 262
#define DIVIDE 263
#define EXPONENT 264
#define REAL 265
#define NAME 266
#define DATE 267
#define TIME 268
#define ZONE 269

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 26 "utparse.y" /* yacc.c:1909  */

    double	rval;			/* floating-point numerical value */
    long	ival;			/* integer numerical value */
    char	name[UT_NAMELEN];	/* name of a quantity */
    utUnit	unit;			/* "unit" structure */

#line 89 "utparse.tab.h" /* yacc.c:1909  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE utlval;

int utparse (void);

#endif /* !YY_UT_UTPARSE_TAB_H_INCLUDED  */
