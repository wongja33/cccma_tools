/* A Bison parser, made by GNU Bison 2.0.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     INT = 258,
     ERR = 259,
     SHIFT = 260,
     SPACE = 261,
     MULTIPLY = 262,
     DIVIDE = 263,
     EXPONENT = 264,
     REAL = 265,
     NAME = 266,
     DATE = 267,
     TIME = 268,
     ZONE = 269
   };
#endif
#define INT 258
#define ERR 259
#define SHIFT 260
#define SPACE 261
#define MULTIPLY 262
#define DIVIDE 263
#define EXPONENT 264
#define REAL 265
#define NAME 266
#define DATE 267
#define TIME 268
#define ZONE 269




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 26 "utparse.y"
typedef union YYSTYPE {
    double	rval;			/* floating-point numerical value */
    long	ival;			/* integer numerical value */
    char	name[UT_NAMELEN];	/* name of a quantity */
    utUnit	unit;			/* "unit" structure */
} YYSTYPE;
/* Line 1274 of yacc.c.  */
#line 72 "utparse.tab.h"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE utlval;



