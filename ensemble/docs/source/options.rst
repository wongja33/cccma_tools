.. _options:

Configuration Settings
----------------------

The various options are set through a YAML configuration file (eg. ``config.yaml``)
passed to ``setup-ensemble``. Below are some of the more common options that can be set.

**Ensemble Setup**

#. :ref:`Setting up an ensemble from a table<from_table>`
#. :ref:`Sharing code between ensemble members<share_code>`
#. :ref:`Sharing executables between ensemble members<share_exec>`
#. :ref:`Job Numbering<first_job_num>`
#. :ref:`Setting up an ensemble from a forked repo of CanESM<setting_up_from_a_fork>`

**Model Settings**

#. :ref:`Start and stop times<run_dates>`
#. :ref:`Restart files and restart dates<restart_files>`
#. :ref:`Additional restart options<restart_options>`
#. :ref:`Random number seed<rdm_num_pert>`
#. :ref:`Settings in canesm.cfg<canesm_options>`
#. :ref:`Settings in the basefile<basefile_options>`
#. :ref:`Namelist Settings<namelist_mods>`
#. :ref:`Passing flags to setup-canesm<setup_flags>`
#. :ref:`Setting compiler (cpp) definitions<cpp_defs>`
#. :ref:`Using variable assignment<var_assign>`
#. :ref:`Using namelists from restarts<restart_namelists>`

.. _ensemble_settings:

Ensemble Setup
^^^^^^^^^^^^^^

.. _from_table:

Setting Up an Ensemble from a Table
***********************************
If a large numbe of ensembles is required it can be convenient to generate
them from a table. This can be done by setting up a ``config_table.txt`` such
as :download:`run_table_abc.txt <../../canesm/setup_examples/example_table.txt>`

.. literalinclude:: ../../canesm/setup_examples/example_table.txt

and setting the following option in configuration file (eg. ``config.yaml``)

.. code-block:: yaml

   config_table: run_table_abc.txt

The ``ensemble_size``, ``runid``, ``restart_dates``, ``restart_files`` and ``pp_rdm_num_pert``
options are then set from the table, and don't need to be included in ``config.yaml``.
All table columns are optional, so you can include only the options you need changed for each run.

.. note::

   If an option is included in both the main configuration ``yaml`` or ``json`` file *and*
   the ``config_table`` file, **the value in the table file will take precedence**.

To set options in ``canesm_cfg`` or ``basefile`` prefix the variable name in
table column by the file variable. For example if ``runmode`` is changed for
each run the column name would be ``canesm_cfg:runmode``. Additionally, if you'd
like to make namelist modifications, add a column with a name like
``namelist_mods:namelist_file:variable_name`` - you can see an examples modifying
``canesm.cfg``/namelist files :ref:`here<canesm_cfg_table>`.

The location of ``config_table.txt`` is first assumed to be a path relative to the directory where the program is run.
If it is not found here, then it is assumed to be a path relative to ``config.yaml``.

.. _share_code:

Sharing Code Between Jobs
*************************

As of version :ref:`0.4<changelog>` the default configuration is that ``canesm-ensemble``
will share code between ensemble members. The ``CanESM5`` repo will be cloned for the first
ensemble member, and each successive run will simply link to this code.
If independent code is desired for each run then this option can be turned off through

.. code-block:: yaml

   share_member_code: False

Note that the code is still compiled for each member unless ``share_executables`` is also set.

.. note:: This code uses changes to the ``adv-setup`` script in the ``CCCma_tools`` repository,
   so your run will need to contain these updates if you want to share code between members. If
   you are running older versions you can cherry pick the relevant commits using

   .. code-block:: bash

      git cherry-pick -x 8e6fcec8..9d0c58e3

.. _share_exec:

Sharing Executables Between Jobs
********************************

To share executables between ensemble members use the option.

.. code-block:: yaml

   share_member_executables: True

Note that this will not take affect if ``share_member_code`` has been set to ``False``
(``share_member_code`` is ``True`` by default).


.. _first_job_num:

Setting the First Job Number
****************************
By default the script will create a folder structure of

::

   run_directory
     |-- runid-000
     |-- runid-001
     |-- ...
     |-- runid-XXX


However, the default start value of ``000`` can be changed from zero by setting the option

.. code-block:: yaml

   first_job_number: 5

This can be useful when adding additional members to an ensemble. The runid can also be adjusted
if you are running up against the 12 character runid limit.

.. code-block:: yaml

   runid: test-run
   job_delimiter: ""
   job_str_zeropad: 0
   first_job_number: 5

This will create runs with ids ``test-run5``, ``test-run6``, etc.

.. _setting_up_from_a_fork:

Setting Up an Ensemble from a Forked Repository of CanESM
*********************************************************
By default, the ensemble tool will set up runs from the main repo of CanESM, i.e. 
``git@gitlab.science.gc.ca:CanESM/CanESM5.git``. However, it may be useful for a user
to setup/launch the ensemble from their development fork. Thus the functionality for this has been added
to the ensemble tool, much like the ``repo`` flag for ``setup-canesm``. Specifically, all that a user needs to do is add

.. code-block:: yaml

    repo: git@git.fake.url:namespace/RepoName

to their ``yaml`` file, where ``git@git.fake.url:namespace/RepoName`` is replaced by the url address to 
the desired fork (i.e. ``git@gitlab.science.gc.ca:user123/CanESM5.git``, if ``user123`` created a fork
of ``CanESM5.git`` on ``gitlab.science.gc.ca``.)

.. _model_settings:

Model Settings
^^^^^^^^^^^^^^

.. _run_dates:

Run Dates
*********

The length of the simulation is controlled by the ``start_time``
and ``stop_time`` attributes. If all ensemble members will use the
same run times then they can be set as

.. code-block:: yaml

   start_time: 2000
   stop_time: 2010

If different times are desired then they can be set as a list the same length as ``ensemble_size``.

.. code-block:: yaml

   start_time: [2000, 2001, 2002]
   stop_time: [2005, 2006, 2007]

Start and stop times can also be added to a :ref:`config table<from_table>`

::

    runid       restart_files   restart_dates   start_date      stop_date
    run-001     mc_abc          1999_m12        2000            2002
    run-002     mc_abc          2000_m12        2001            2003

If monthly resoltuion is wanted then dates can be set as ``YYYY_mMM``.

.. _restart_files:

Restart Files and Dates
***********************

The ``restart_files`` option handles what files are used for starting the simulation
(both in amip and esm modes). If specified as a string, the same restart file is used
for all members. If specified as a list it should be the same length as the ensemble size,
with each member using a different restart file,

.. code-block:: yaml

   ensemble_size: 3
   restart_files: [vsa_v4_01, vsa_v4_02, vsa_v4_03]


If the restart files are stored on tape, then the ``tapeload`` option should be set to ``True``.
If not set the files are assumed to be on disc and ``save_restart`` is ran instead.

.. code-block:: yaml

   tapeload: True

Similar to restart files, if a number is provided then all members will use the same
restart date. If a list is provided it should be same length as ``ensemble_size``.
Dates either as integers or strings with the format :code:`YYYY_mMM` are accepted. If
an integer is provided ``YYYY_m12`` is assumed.

Restarts can also be initialized at fixed time intervals using

.. code-block:: yaml

   ensemble_size: 5
   restart_files: vsa_v4_01
   restart_dates: 2000  # first sample
   restart_every_x_years: 10  # sample every 10 years starting in 2000

This will use restart files from 2000_m12, 2010_m12, 2020_m12, etc.

.. _restart_options:

Restart Options
***************

Other restart options can be set using the key ``restart_options`` in ``config.yaml``

.. code-block:: yaml

   restart_options:
     archive_class: crd_short_term

.. _rdm_num_pert:

Random Number Perturbations
***************************

The random number ``pp_rdm_num_pert`` can be specified as a fixed number,
a list of values, or the string ``from job number``. If set to the string
then each member will use the value

.. code-block:: python

   pp_rdm_num_pert = 10 * job_number

where ``job_number`` goes from ``first_job_number`` to ``ensemble_size`` + ``first_job_number``.
Note that setting ``pp_rdm_num_pert`` will **always** cause it to be set for the
run. No matter the value of other settings.

.. _setup_flags:

Setup Flags
***********

Additional flags can be passed to the ``canesm-setup`` script using the ``setup_flags`` option. For example,
if it desired to specify the runs as cmip experiments and set the priority as topdog you can use the line

.. code-block:: yaml

   setup_flags: cmip=1 --topdog

.. _canesm_options:

Options in canesm.cfg
*********************

Options in the ``canesm.cfg`` file are accessed through a ``canesm_cfg`` dictionary. A common one to change
is ``runmode``, but any parameter with the format :code:`option=value` in the ``canesm.cfg`` file can be updated.

.. code-block:: yaml

   canesm_cfg:
     runmode: AMIP-nudged

Like other options, a list can be provided if the options need to be changed on a per-run basis. The list
should be the same length as the ensemble size.

.. code-block:: yaml

   ensemble_size: 2
   canesm_cfg:
     runmode: [AMIP, AMIP-nudged]

.. _basefile_options:

Options in the basefile
************************

Options in the ``basefile`` are accessed in a very similar way to ``canesm.cfg`` options.

.. code-block:: yaml

   basefile:
     production: 0

.. _namelist_mods:

Namelist Modications
********************

The ensemble tool supports general modifications to any namelist that is extracted
for the desired ``runmode``/``config``, and is controlled through the nested dicitonary,
``namelist_mods``. Underneath this dictionary, the ensemble tool 
**expects keys matching the filename for the namelists to be modified** (case sensitive). 
For example, if a user wanted to set the solar constant used in the AGCM *across the entire
ensemble* (which gets defined in the ``PHYS_PARM`` file), this could be
achieved through the following settings:

.. code-block:: yaml

    namelist_mods:
        PHYS_PARM:                      # this key MUST match the actual filename (case sensitive)
            pp_solar_const : 1360.747

If instead the user wanted to define a different value for each member of the ensemble, 
it could be done like:

.. code-block:: yaml

    ensemble_size: 3

    ...

    namelist_mods:
        PHYS_PARM:
            pp_solar_const: [1360.747, 1362.747, 1364.747]

Likewise, if a user wants to make this modification *and* also include modifications for other
namelists, it can by adding additional dictionaries under ``namelist_mods``

.. code-block:: yaml

    ensemble_size: 3

    ...

    namelist_mods:
        PHYS_PARM:
            pp_solar_const: [1360.747, 1362.747, 1364.747]  # will set three different values across the ensemble
        INLINE_DIAG_NL:
            Llidar_sim: .false.  # will be set consistently across the ensemble


**Modifying AGCM tracer properites**

Recent work to the ``CanAM`` has made it much easier modify the tracer
attributes within the AGCM, through the use of the ``&tracer_config`` namelist
in ``modl.dat``. Most notable of the configurable variables is
``tracers_inp_list``, which can be a list of lists that are used to modify
things like a tracer's ``XREF`` or ``MW`` - for specifics on this, developers
are directed to the 
`trinfo <https://gitlab.com/cccma/canam/-/blob/30eed07cd24e20147b0c97f0e00206ad61aba857/spectral_canesm/lsmod/trinfo.F90>`_ 
``module``. 

Modifying this namelist variable *is supported by the ensmeble tool*, but care
must be taken to make sure the changes are made correctly. Specifically, users
need to surround the each list of lists in single quotations (``'``), with
double quotations ``"`` used to group each sublist. For example, say a user
wants to turn off the advection of tracer ``TRAC1``, and ``TRAC2`` for one of
two members, and leave it on for the other, it would need to be specified as:

.. code-block:: yaml

    namelist_mods:
        modl.dat:
            tracers_inp_list : [ '"TRAC1,ADV=0","TRAC2,ADV=0"', '"TRAC1,ADV=1","TRAC2,ADV=1"' ] 

Likewise, if the user simply wants to turn off the advection for ``TRAC1`` and
``TRAC2`` across the ensemble, it can be done via:

.. code-block:: yaml

    namelist_mods:
        modl.dat:
            tracers_inp_list : '"TRAC1,ADV=0","TRAC2,ADV=0"'
    
.. _cpp_defs:

CPP Definitions
***************

cpp definitions can be accessed through the nested dictionary ``cpp_defs`` with the structure

.. code-block:: yaml

   cpp_defs:
     filename:
       option: True/False

For example, if we wanted to turn on ``radforce`` in the default diagnostic, and 4x times CO\ :sub:`2`
forcing (``co2_x4``) and turn off explosive volcanoes (``xplvol``), the configuration would be

.. code-block:: yaml

   cpp_defs:
     cppdefs_diag_default:
       radforce: True
     cppdefs_32bit_amip:
       co2_x4: True
       xplvol: False

Where ``cppdefs_32bit_amip`` or ``cppdefs_diag_default`` are the names of the cppdef
files that will be updated. ``True`` will turn the option on (:code:`#define`) and ``False``
will turn the option off (:code:`#undef`) in the cpp_def file.

The name of the cppdef files that are being used can be found in the ``canesm.cfg`` file under
the options ``cppdefs_diag`` and ``cppdefs_file`` in the ``runmode`` that you are using.


.. _var_assign:

Variable Assignment
*******************

Sometimes many variables in a run are initialized with the same value. For example, if a snippet
of our ``PHYS_PARM`` file looked like this:

.. code-block::

   ghg_co2_scale=$ghg_scale_param
   ghg_ch4_scale=$ghg_scale_param
   ghg_n2o_scale=$ghg_scale_param
   ...

While possible, it would be tedious to assign each value in the ``config.yaml`` file. To help
with this variables of the form ``$varname`` can be replaced using the syntax:

.. code-block:: yaml

   filename:
     $varname: value

Where ``filename`` is ``canesm_cfg``, ``PHYS_PARM``, etc. So, for the snippet above, if we wanted
change all of the values, then in our yaml file we could include:

.. code-block:: yaml

   phys_parm:
     $ghg_scale_param: 2.0

This will generate a ``phys_parm`` file with the values

.. code-block::

   ghg_co2_scale=2.0
   ghg_ch4_scale=2.0
   ghg_n2o_scale=2.0
   ...

Similar to :ref:`setting up a table <canesm_cfg_table>`, we can assign
these values on a per run basis using the table format of:

.. code-block::

   runid        phys_parm:$ghg_scale_param
   run_abc-000  1.0
   run_abc-001  1.1
   ...

.. _restart_namelists:

Using Namelists from Restarts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The restart files dictate the initial conditions of the model and are produced
from a "parent run". Mostly, these are physical paramters like temperature,
winds, etc., but they also include the namelist files that were used to run the
simulation that produced the restart. 

By *default*, the namelists from the restarts will be ignored and instead the
infrastructure extracts versions from the source code and configures them according
to the desired ``runmode``/``config``, however there are instances when users
would prefer to use the namelists from the parent run.  As such, this
functionality has been built into the ensemble tool, specifically via the
nested dictionary ``namelists_to_pull_from_restarts``.

For example, if a user wishes to use the ``PHYS_PARM`` and ``modl.dat`` namelists
from the restart, this can be achieved via:

.. code-block:: yaml

    namelists_to_pull_from_restarts:
        agcm: [ 'PHYS_PARM', 'modl.dat' ]

Additionally, if you would like to pull namelists from restarts for the *other* components
(i.e. ``coupler``, ``ocean``), additional dictionary entries can be added:

.. code-block:: yaml

    namelists_to_pull_from_restarts:
        agcm: [ 'PHYS_PARM', 'modl.dat' ]
        coupler: [ 'nl_coupler_par' ]

It should also be noted that when this option is used, users are still free to change
parameters in the extracted files via ``namelist_mods``, which is described :ref:`above <namelist_mods>`
