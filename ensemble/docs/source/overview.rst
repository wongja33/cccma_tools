.. _overview:

Overview
--------

The main idea behind ``canesm-ensemble`` is to separate ensemble configuration from the actual setup.
This allows for configuration without the need to write setup scripts or worry about the implementation details.


Setup of each ensemble revolves around a single configuration file (and an optional table for more detailed setup).
This holds the necessary information such as ensemble size, what version of the code to use,
what machine to run on, etc.
The work is then done using a few command line interfaces that read this configuration file to get the necessary
information.


The Configuration File
**********************

The configuration file is a short setup written in `YAML <https://yaml.org/spec/1.2/spec.html>`_,
or, "yet another markup language".
A nice guide to YAML is available `here <https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html>`_,
but the most relevant bit for this project is that YAML defines options/value pairs using the format:

.. code-block::

   option: value

Values don't generally need to be wrapped in parentheses, but can be if you want to guarantee the
value is interpreted as a string. Values can be scalars (eg. ``1``) or lists (eg. ``[1, 2, 3]``).
Options can also be organized into collections using indentation:

.. code-block::

   collection:
     option1: value
     option2: value

The configuration file should be treated something like a run id.
Try to make the name descriptive and more importantly unique.
Configuration files are not meant to be re-used, instead make a new one for each ensemble,
as the original file can be used to add new members, extend the simulation, and monitor the
status of the ensemble.
It is also good to keep around (and under version control when possible) for reproducibility purposes
(to be safe a copy of the file is also automatically stored on the machine where the ensemble is ran).


What Needs to be Set?
*********************

There are quite a few things that :ref:`can be set<options>`, but a few options are required in every configuration file:

  - :attr:`runid<.CanESMensemble.runid>`
  - :attr:`version<.CanESMensemble.ver>`
  - :attr:`config<.CanESMensemble.config>`
  - :attr:`machine<.CanESMensemble.machine>`
  - :attr:`user<.CanESMensemble.user>`
  - :attr:`run_directory<.CanESMensemble.run_directory>`
  - :attr:`start_time<.CanESMensemble.start_time>`
  - :attr:`stop_time<.CanESMensemble.stop_time>`
  - :attr:`restart_dates<.CanESMensemble.restart_dates>`
  - :attr:`restart_files<.CanESMensemble.restart_files>`

Lists and Scalars Values
************************

By default, if an option is a scalar value, then it is applied to every ensemble member.
If it is a list, there should be one value for each member.

Most options can be set as either a list or scalar, but there a few exceptions.
Currently, the options that *cannot* be set on a per-member basis are

 - remote connection settings: ``machine``, ``user``
 - source code settings: ``repo``, ``ver``, ``config``
 - code linking settings: ``share_member_code``, ``share_executables``
 - job string settings: ``first_job_number``, ``job_str_zeropad``, ``job_delimiter``
 - settings that are used when users want to use restart namelists - specifically, the component specific lists that can be given to the optional dictionary ``namelists_to_pull_from_restarts``. More information on this is provided :ref:`here<restart_namelists>` for more details.

If you have a large ensemble, or want to change several parameters for each run creating a table
can be a more convenient option, as is also supported, as described :ref:`here<from_table>`.


How Does it All Work?
*********************

The ``config.yaml`` file holds all the required information, so an ensemble can always be re-created
later. The :ref:`command line interface tools<cli>` then call the python code that is used to do the
actual setup and interaction. Lastly, the current state of the ensemble is stored in a
`sqlite <https://www.sqlite.org/index.html>`_ database so that ``canesm-ensemble`` knows what members
have been setup, where they are stored, and whether they have been submitted.

Why canesm-ensemble?
********************

Writing a bash script to loop over ``setup-canesm`` is often a perfectly viable option for setting
up an ensemble.
It works well for one-off runs and there's plenty of examples floating around to build off of.
However, as the number of ensembles grows, there a few downsides to this approach:

  - A lot of copy and pasted code that does mostly the same thing. This has a couple of knock-on effects:

    - Difficulty in finding the script that has the feature that you need.
    - Propogating bug fixes becomes very difficult.
  - Options and settings can be buried in ``awk`` or ``sed`` commands making it hard to see
    exactly what is being changed at a glance.
  - While one-off setup can be straight-forward, monitoring and further interaction with an ensemble
    can be more difficult.

``canesm-ensemble`` aims to address these limitations with the following:

  - A single repository that encompasses *most* use cases to avoid code duplication.
  - Separate configuration and setup to make settings clearer and easier to change.
  - Additional scripts to help with :ref:`job submission<submit_the_jobs>`,
    :ref:`monitoring<monitor_ensemble>` and :ref:`extension<extend_ensemble>`.
