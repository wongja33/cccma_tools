.. _canesm:

API Reference
=============

The code has essentially three layers. The lowest layer is
:py:class:`.CanESMsetup` which handles the setup
of a single model run. Above this is :py:class:`.CanESMensemble`
which takes cares of organizing the ensemble members. Lastly, is the user-facing
script :py:func:`.scripts.setup_ensemble` that provides a
command line interface to the code and sends the configuration file values to
:py:class:`.CanESMensemble`.


CanESMensemble
--------------

.. autoclass:: canesm.canesm_ensemble.CanESMensemble

CanESMsetup
-----------

.. autoclass:: canesm.canesm_setup.CanESMsetup

