import canesm
import pytest
from canesm.scripts.cli_functions import setup_ensemble
from canesm.scripts.cli_functions import submit_ensemble
from canesm.scripts.cli_functions import monitor_ensemble
from click.testing import CliRunner


@pytest.skip('version option is no longer supported')
class TestInstall:

    def test_setup_ensemble(self):

        runner = CliRunner()
        result = runner.invoke(setup_ensemble, ['--version'])
        assert result.stdout.strip() == canesm.__version__

    def test_submit_ensemble(self):

        runner = CliRunner()
        result = runner.invoke(submit_ensemble, ['--version'])
        assert result.stdout.strip() == canesm.__version__

    def test_monitor_ensemble(self):

        runner = CliRunner()
        result = runner.invoke(monitor_ensemble, ['--version'])
        assert result.stdout.strip() == canesm.__version__
