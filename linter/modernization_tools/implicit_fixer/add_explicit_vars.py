#!/usr/bin/python3
import re
import sys

def main():
    filename = sys.argv[1]
    lines = []
    correctedLines = []
    with open(filename, 'r') as f:
        lines = f.readlines()
    
    explicitVars = []
    with open('vars.txt', 'r') as f:
        explicitVars = [x for x in f.readlines() if len(x) > 0]

    continuation = False
    for line in lines:
        if re.match(r'^\s*implicit\s+[^nN]', line, re.IGNORECASE):
            correctedLines.append('  implicit none\n')
            correctedLines.extend(explicitVars)
            if re.match(r'^[^!&\n]*&', line, re.IGNORECASE):
                continuation = True
            continue
        if continuation:
            if re.match(r'^[^!&\n]*&', line, re.IGNORECASE) == None:
                continuation = False
            continue
        correctedLines.append(line)

    with open(filename, 'w') as f:
        for line in correctedLines:
            f.write(line)


if __name__ == '__main__':
    main()