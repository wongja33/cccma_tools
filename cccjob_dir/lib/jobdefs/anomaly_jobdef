#!/bin/sh
#=======================================================================
# Standard anomaly deck                                  --- anomaly ---
# $Id: anomaly_jobdef 639 2011-02-02 18:29:32Z acrnrls $
#=======================================================================
#     keyword :: anomaly
# description :: diagnostic anomaly deck
#******************************

 set -a
 . betapath2

#  * ........................... Parmsub Parameters ............................

  # These variables are set when the job string is created

  previous_year=NotSet
  previous_month=NotSet

  current_year=NotSet
  current_month=NotSet

  next_year=NotSet
  next_month=NotSet

  run_start_year=NotSet
  run_start_month=NotSet
  run_stop_year=NotSet
  run_stop_month=NotSet

  jobname=anomaly;
  flabel="ANOM";
  runid="job000"; uxxx='uxxx'; pool_uxxx=$uxxx; anomaly_uxxx=$pool_uxxx;
  crawork="${runid}_job"; username="acrnxxx"; user="XXX";
  RUNID=`echo "${runid}"|tr '[a-z]' '[A-Z]'`;
  nqsprfx="${runid}_"; nqsext='';

  anomaly_prefix="${anomaly_uxxx}_${runid}";         # memory99=1
  anomaly_prefix_=${anomaly_prefix}_                 # memory99=1

  anomaly_days=NotSet   # memory99=1
  days="DJF"            # memory99=1
  if [ x"$anomaly_days" != "xNotSet" ]; then
    eval days\=$anomaly_days   # memory99=1
  fi
  lc_days=`echo $days|tr '[A-Z]' '[a-z]'`
  case $lc_days in
    jan) lc_days=m01 ;;
    feb) lc_days=m02 ;;
    mar) lc_days=m03 ;;
    apr) lc_days=m04 ;;
    may) lc_days=m05 ;;
    jun) lc_days=m06 ;;
    jul) lc_days=m07 ;;
    aug) lc_days=m08 ;;
    sep) lc_days=m09 ;;
    oct) lc_days=m10 ;;
    nov) lc_days=m11 ;;
    dec) lc_days=m12 ;;
  esac

  year=yyy       # memory99=1

  nextjob=on
  noprint=on

  # ---Start_submit_ignore_code----

  stamp=`date "+%j%H%M%S"$$`

  # Use -e option if recognized by echo
  if [ "X`echo -e`" = "X-e" ]; then
    echo_e() { echo ${1+"$@"}; }
  else
    echo_e() { echo -e ${1+"$@"}; }
  fi

  # bail is a simple error exit routine
  # Note: we write the error directly to a file in ~/.queue so that this
  #       info is not lost if/when stdout is not returned
  error_out="$HOME/.queue/error_anomaly_${runid}_$stamp"
  [ ! -z "$error_out" ] && rm -f $error_out
  bail(){
    echo_e `date`" --- anomaly: $*"
    echo_e `date`" --- anomaly: $*" >>$error_out
    exit 1
  }

  # This invocation of make_file_name_list will process the *_year and *_months
  # variables defined above and output a file containing definitions for
  # start_year, start_mon, stop_year, stop_mon
  tmp_file_list="anomaly_date_list_${runid}_${stamp}"
  make_file_name_list --dates_only $tmp_file_list >>$error_out 2>&1 ||\
    bail "Problem in make_file_name_list"
  rm -f $error_out

  # Verify that the output list is not empty
  [ ! -s "$tmp_file_list" ] && bail "Unable to create file list"

  # A file list was created ...source it
  # This will define start_year, start_mon, stop_year, stop_mon
  : ; . $tmp_file_list
  rm -f $tmp_file_list

  # Define start and stop dates
  anomaly_start_year=$start_year
  anomaly_start_mon=$start_mon
  anomaly_stop_year=$stop_year
  anomaly_stop_mon=$stop_mon

  # ym_range is used in the definition of input (model1, model2) and
  # output (plpfn) file names as well as in the title of most plots.

  # define a range string in the form YYYYmMM_YYYYmMM to be used in file names
  ym_range="${anomaly_start_year}m${anomaly_start_mon}"
  ym_range="${ym_range}_${anomaly_stop_year}m${anomaly_stop_mon}"

  # In the special case that start and stop dates are identical use a YYYY format
  if [ $anomaly_start_year -eq $anomaly_stop_year ]; then
    if [ $anomaly_start_mon -eq $anomaly_stop_mon ]; then
      ym_range=$anomaly_start_year
    fi
  fi

  # files to be compared are named ${model1}gp and ${model2}gp
  # obsdat and resol will be used to define the default model1 value
  obsdat=on
  resol="128_64";
  if [ "$obsdat" = on ]; then
    # model1 will contain obs data
    model1=pd_era40_${resol}_198101_200012_${lc_days}_
  else
    # model1 will contain model data
    model1_prefix=NotSet
    model1_prefix_=${model1_prefix}_
    model1=${model1_prefix_}${ym_range}_${lc_days}_
  fi
  model2=${anomaly_prefix_}${ym_range}_${lc_days}_

  # runid1 and runid2 will be used in some plot titles
  r1ism1=0
  r2ism2=0
  runid1=`echo $model1|awk -F_ '{print $2}' -|tr '[a-z]' '[A-Z]'`
  [ -z "$runid1" ] && runid1=$model1 && r1ism1=1
  runid2=`echo $model2|awk -F_ '{print $2}' -|tr '[a-z]' '[A-Z]'`
  [ -z "$runid2" ] && runid2=$model2 && r2ism2=1

  # define the default run value in terms of runid1 and runid2
  if [ $r1ism1 -eq 0 -a $r2ism2 -eq 0 ]; then
    run="${runid2}-${runid1}"   # memory99=1
  else
    run=`echo "${model2}"|tr '[a-z]' '[A-Z]'`  # memory99=1
    run="${run}-OBS"                           # memory99=1
  fi

  iday="  335"; plunit=VIC;  plpfn="${run}_${lc_days}_${ym_range}";
  pldir=''
  obsfile=obs_sfc_${resol}; obsday=$days
  lc_obsday=`echo $obsday|tr '[A-Z]' '[a-z]'`
  case $lc_obsday in
    jan) lc_obsday=m01 ;;
    feb) lc_obsday=m02 ;;
    mar) lc_obsday=m03 ;;
    apr) lc_obsday=m04 ;;
    may) lc_obsday=m05 ;;
    jun) lc_obsday=m06 ;;
    jul) lc_obsday=m07 ;;
    aug) lc_obsday=m08 ;;
    sep) lc_obsday=m09 ;;
    oct) lc_obsday=m10 ;;
    nov) lc_obsday=m11 ;;
    dec) lc_obsday=m12 ;;
  esac

  obsceres=pd_ceres_ebaf_toa_200003_200510_grid_${resol}_${lc_obsday};
  obsceres_msk=pd_ceres_ebaf_toa_200003_200510_grid_tmask_${resol}_${lc_obsday};
  obsuwisc=pd_uwisc_1988_2007_lwp_${resol}_${lc_obsday};
  obsuwisc_msk=pd_uwisc_1988_2007_lwp_tmask_${resol}_${lc_obsday};
  ceres_cld=pd_ceres_2d_srbavg2_month_2000_2003_data_${resol}_${lc_obsday};
  obspcp_xa=pd_xie_arkin_1979_1998_pcpn_${resol}_${lc_obsday};
  obspcp_gpcp=pd_gpcp_month_1979_2005_pcpn_${resol}_${lc_obsday};

 # obsssmi=pd_ssmi_1987_2007_${resol}_${lc_obsday};
 # obsssmi_msk=pd_ssmi_1987_2007_tmask_${resol}_${lc_obsday};

  t1="        1"; t2="999999999"; t3="   1"; s3="   1"; stime="200";
  lxp="    0"; kax="    1"; kin="  1 1"; d="D"; 
  map="   00"; b="-1+";
  plv="   16"; 
  lopgm=lopgm;  oldiag=diag4;
  memory1="100mb"; memory2="100mb";

  case $resol in
     96_48)
       lrt="   47"; lmt="   47"; typ="    2"
       lon="   96"; lat="   48"; npg="    2"
       ncx="    2"; ncy="    2"
       delt="   1200.0";
       ;;
    128_64)
       lrt="   63"; lmt="   63"; typ="    2"
       lon="  128"; lat="   64"; npg="    2"
       ncx="    2"; ncy="    2"
       delt="    900.0"
       ;;
    192_96)
       lrt="   95"; lmt="   95"; typ="    2"
       lon="  192"; lat="   96"; npg="    2"
       ncx="    2"; ncy="    2"
       delt="    600.0"
       ;;
          *) echo "Unknown resolution $resol"
             exit 1 ;;
  esac

  # ptitle and ptitle2 are common prefixes for certain plot titles
  ym_range=`echo $ym_range|sed 's/_/-/g'`
  ptitle="$days $ym_range  $run "
  ptitle2="$days $ym_range "

#  * ............................ Condef Parameters ............................

  nocldo=on
  colourplots=on
  gpcp=on
  noprint=on
  nextjob=on
  stat2nd=off

  # ---Stop_submit_ignore_code----

#  * ............................. Deck Definition .............................

  . anomaly13.dk

#end_of_job
