#!/bin/sh
#=======================================================================
# Create A/O GCM model diagnostic files                   --- stdiag ---
# $Id: stdiag_jobdef 657 2012-02-22 23:48:49Z acrnrls $
#=======================================================================
#     keyword :: stdiag
# description :: standard diagnostics

  set -a
  . betapath2

#  * ........................... Parmsub Parameters ............................

  dtime=1800; gptime=3600; stime=3600;
  memory1="1500mb"; memory2="1500mb"; memory3="1500mb";

  # Define agcm version
  modver=${modver:=gcm15h}
  model_version=$modver

  oldiag="diag4"
  lopgm=lopgm

  username="acrnxxx"; user="XXX";
  runid=aaa; crawork=${runid}_stdiag;
  nqsprfx="${runid}_"; nqsext="";

  # These parameters are used to define model1 and flabel
  # The trailing comment, "# memory99=1", is required
  # to work around a bug in the submission scripts
  diag_uxxx=dc; stdiag_flabel_uxxx=$diag_uxxx           # memory99=1
  model_uxxx=mc; stdiag_model_uxxx=$model_uxxx          # memory99=1
  stdiag_flabel_prefix=${stdiag_flabel_uxxx}_${runid}   # memory99=1
  stdiag_model_prefix=${stdiag_model_uxxx}_${runid}     # memory99=1
  year=yyy; mon=mm;                                     # memory99=1

  # model1 determines names of the input model files
  # (e.g. ${model1}gs, ${model1}ss ...)
  model1="${stdiag_model_prefix}_${year}_m${mon}_";

  # flabel determines names of the output diagnostic files
  # (e.g. ${flabel}gp, ${flabel}xp, ...)
  flabel="${stdiag_flabel_prefix}_${year}_m${mon}_";

  # It is sometimes desirable to reset RUNPATH and/or CCRNTMP
  # These definitions are included to allow this via cccjob command line args
  diag_RUNPATH=''
  RUNPATH=${diag_RUNPATH:=$RUNPATH}
  diag_CCRNTMP=''
  CCRNTMP=${diag_CCRNTMP:=$CCRNTMP}

  # Alternate path to a directory where .queue/.crawork will be found
  JHOME=''

  if [ -n "$JHOME" -a x"$JHOME" != x"$HOME" ]; then
    # Allow optional reset of DATAPATH/RUNPATH
    JHOME_DATA=''
    DATAPATH=${JHOME_DATA:=$DATAPATH}
    RUNPATH=${JHOME_DATA:=$RUNPATH}
    # Allow optional reset of CCRNTMP
    JHOME_RUN=''
    CCRNTMP=${JHOME_RUN:=$CCRNTMP}
  fi

  t1="        1"; t2="999999999"; t3="   1"; s3="   1";
  r1="        1"; r2="999999999"; r3="   1";
  g1="        1"; g2="999999999"; g3="   1"
  a1="        1"; a2="999999999"; a3="   1";
  lay="    2"; coord=" ET15"; plid="      50.0"; topsig="-1.00";
  kax="    1"; kin="    1"; lxp="    0"; map="    0";   b="  +"; d="B";
  ncx="    2"; ncy="    2"; typ="    2"; npg="    2"; lml="  995";

  # Define pressure levels for output fields
  case $model_version in
    gcm16|gcm17) # This is equivalent to model version 4.1 | 4.2
      plv="   37";
      p01="-0100"; p02="-0200"; p03="-0300"; p04="-0500"; p05="-0700";
      p06="   10"; p07="   20"; p08="   30"; p09="   50"; p10="   70";
      p11="  100"; p12="  125"; p13="  150"; p14="  175"; p15="  200";
      p16="  225"; p17="  250"; p18="  300"; p19="  350"; p20="  400";
      p21="  450"; p22="  500"; p23="  550"; p24="  600"; p25="  650";
      p26="  700"; p27="  750"; p28="  775"; p29="  800"; p30="  825";
      p31="  850"; p32="  875"; p33="  900"; p34="  925"; p35="  950";
      p36="  975"; p37=" 1000"; p38="     "; p39="     "; p40="     ";
      p41="     "; p42="     "; p43="     "; p44="     "; p45="     ";
      p46="     "; p47="     "; p48="     "; p49="     "; p50="     ";
      ;;

    gcm15h) # This is the set of pressure levels used for AR5 data
      plv="   22";
      p01="-0100"; p02="-0200"; p03="-0300"; p04="-0500"; p05="-0700";
      p06="   10"; p07="   20"; p08="   30"; p09="   50"; p10="   70";
      p11="  100"; p12="  150"; p13="  200"; p14="  250"; p15="  300";
      p16="  400"; p17="  500"; p18="  600"; p19="  700"; p20="  850";
      p21="  925"; p22=" 1000"; p23="     "; p24="     "; p25="     ";
      p26="     "; p27="     "; p28="     "; p29="     "; p30="     ";
      p31="     "; p32="     "; p33="     "; p34="     "; p35="     ";
      p36="     "; p37="     "; p38="     "; p39="     "; p40="     ";
      p41="     "; p42="     "; p43="     "; p44="     "; p45="     ";
      p46="     "; p47="     "; p48="     "; p49="     "; p50="     ";
      ;;

    gcm15[bcdefg]) # This was the standard set of pressure levels prior to AR5
      plv="   17";
      p01="   10"; p02="   20"; p03="   30"; p04="   50"; p05="   70";
      p06="  100"; p07="  150"; p08="  200"; p09="  250"; p10="  300";
      p11="  400"; p12="  500"; p13="  600"; p14="  700"; p15="  850";
      p16="  925"; p17=" 1000"; p18="     "; p19="     "; p20="     ";
      p21="     "; p22="     "; p23="     "; p24="     "; p25="     ";
      p26="     "; p27="     "; p28="     "; p29="     "; p30="     ";
      p31="     "; p32="     "; p33="     "; p34="     "; p35="     ";
      p36="     "; p37="     "; p38="     "; p39="     "; p40="     ";
      p41="     "; p42="     "; p43="     "; p44="     "; p45="     ";
      p46="     "; p47="     "; p48="     "; p49="     "; p50="     ";
      ;;

    gcm13[bcde])
      plv="   17";
      p01="   10"; p02="   20"; p03="   30"; p04="   50"; p05="   70";
      p06="  100"; p07="  150"; p08="  200"; p09="  250"; p10="  300";
      p11="  400"; p12="  500"; p13="  600"; p14="  700"; p15="  850";
      p16="  925"; p17=" 1000"; p18="     "; p19="     "; p20="     ";
      p21="     "; p22="     "; p23="     "; p24="     "; p25="     ";
      p26="     "; p27="     "; p28="     "; p29="     "; p30="     ";
      p31="     "; p32="     "; p33="     "; p34="     "; p35="     ";
      p36="     "; p37="     "; p38="     "; p39="     "; p40="     ";
      p41="     "; p42="     "; p43="     "; p44="     "; p45="     ";
      p46="     "; p47="     "; p48="     "; p49="     "; p50="     ";
      ;;

    *) echo "stdiag: Invalid model version --> $model_version <--"
       exit 1 ;;
  esac

  # Define these so that they may be changed by the user, if desired,
  # via cccjob command line args
  p51="     "; p52="     "; p53="     "; p54="     "; p55="     ";
  p56="     "; p57="     "; p58="     "; p59="     "; p60="     ";
  p61="     "; p62="     "; p63="     "; p64="     "; p65="     ";
  p66="     "; p67="     "; p68="     "; p69="     "; p70="     ";
  p71="     "; p72="     "; p73="     "; p74="     "; p75="     ";
  p76="     "; p77="     "; p78="     "; p79="     "; p80="     ";
  p81="     "; p82="     "; p83="     "; p84="     "; p85="     ";
  p86="     "; p87="     "; p88="     "; p89="     "; p90="     ";
  p91="     "; p92="     "; p93="     "; p94="     "; p95="     ";
  p96="     "; p97="     "; p98="     "; p99="     "; p100="     ";

  # ---Start_submit_ignore_code----

  stamp=`date "+%j%H%M%S"$$`

  # Use -e option if recognized by echo
  if [ "X`echo -e`" = "X-e" ]; then
    echo_e() { echo ${1+"$@"}; }
  else
    echo_e() { echo -e ${1+"$@"}; }
  fi

  # bail is a simple error exit routine
  # Note: we write the error directly to a file in ~/.queue so that this
  #       info is not lost if/when stdout is not returned
  this_host=`hostname|cut -d'.' -f1`
  error_out="${JHOME:-$HOME}/.queue/error_stdiag_${runid}_${this_host}_$stamp"
  [ ! -z "$error_out" ] && rm -f $error_out
  bail(){
    echo_e `date`" $this_host --- stdiag: $*"
    echo_e `date`" $this_host --- stdiag: $*" >>$error_out
    exit 1
  }

  # Add a dir to the current path
  add2path(){
    if echo $PATH|/bin/grep -Evq "(^|:)$1($|:)" ; then
       if [ "$2" = "atend" ] ; then
          PATH=$PATH:$1
       else
          PATH=$1:$PATH
       fi
    fi
  }

  # Determine the name of a dir containing non-standard scripts and executables
  # that may be required below. Programs in this dir will superceed similar
  # programs found elswhere on the users path.
  path_to_add=/users/tor/acrn/cbn/bin_linux_ar5
  case $this_host in
    # This is joule, use 64 bit binaries
    joule*) path_to_add='/users/tor/acrn/cbn/bin_linux64_ar5' ;;
    # This is pollux, use 64 bit binaries
    ib3-*) path_to_add='/users/tor/acrn/cbn/bin_linux64_ar5' ;;
    c[0-9]*) path_to_add='/users/tor/acrn/cbn/bin_aix64_ar5' ;;
  esac

  dsd3D=off
  PhysA=on
  PhysO=on
  CarbA=on
  CarbO=on
  CarbL=on

  # Extract the PARC record from the gs file and source it to define
  # critical parameter values in the current environment
  CWD=`pwd`
  tmpd=$CCRNTMP/tmp_stdiag_$stamp
  mkdir $tmpd || bail "Cannot create temporary dir $tmpd"
  cd $tmpd
    echo_e "Temporary dir $tmpd" >>$error_out 2>&1
    access  gs_in ${model1}gs    >>$error_out 2>&1
    separmc gs_in X PARM PARC    >>$error_out 2>&1
    release gs_in                >>$error_out 2>&1
    [ ! -s PARC ] && bail "Unable to extract PARC from ${model1}gs"
    binach PARC PARC.txt         >>$error_out 2>&1
    rm -f X PARM PARC
    sed 1d PARC.txt > PARC
    cat PARC                     >>$error_out 2>&1
    : ; . PARC
  cd $CWD
  rm -fr $tmpd
  rm -f $error_out

  if [ -n "$lmt" ]; then
    # Set resolution dependent diagnostic parameters
    # based on the value of lmt from the PARC record
    if [ $lmt -eq 31 ]; then
      # 64x32 (T31) resolution
      lrt="   31"; lon="   64"; lat="   32"
    elif [ $lmt -eq 47 ]; then
      # 96x48 (T47) resolution
      lrt="   47"; lon="   96"; lat="   48"
    elif [ $lmt -eq 63 ]; then
      # 128x64 (T63) resolution
      lrt="   63"; lon="  128"; lat="   64"
    elif [ $lmt -eq 95 ]; then
      # 192x96 (T95) resolution
      lrt="   95"; lon="  192"; lat="   96"
    elif [ $lmt -eq 127 ]; then
      # 256x128 (T127) resolution
      lrt="  127"; lon="  256"; lat="  128"
    else
      bail "Invalid lmt=$lmt found in PARC record"
    fi
  elif [ -n "$lrt" ]; then
    # Set resolution dependent diagnostic parameters
    # based on the value of lrt from the PARC record
    if [ $lrt -eq 31 ]; then
      # 64x32 (T31) resolution
      lmt="   31"; lon="   64"; lat="   32"
    elif [ $lrt -eq 47 ]; then
      # 96x48 (T47) resolution
      lmt="   47"; lon="   96"; lat="   48"
    elif [ $lrt -eq 63 ]; then
      # 128x64 (T63) resolution
      lmt="   63"; lon="  128"; lat="   64"
    elif [ $lrt -eq 95 ]; then
      # 192x96 (T95) resolution
      lmt="   95"; lon="  192"; lat="   96"
    elif [ $lrt -eq 127 ]; then
      # 256x128 (T127) resolution
      lmt="  127"; lon="  256"; lat="  128"
    else
      bail "Invalid lrt=$lrt found in PARC record"
    fi
  else
    # Set a default set of values when lmt is missing from the PARC record
    # WARNING: This may or may not be the desired resolution
    lmt="   63"; lrt="   63"; lon="  128"; lat="   64"
  fi

  # Add a dir to PATH so that non standard decks used herein are found
  # Note: append to PATH so that this dir is the last place to look
  [ -n "$path_to_add" ] && add2path $path_to_add atend

  # ---Stop_submit_ignore_code----

  # daily_cmip5_tiers is a space separated list of integers indicating which
  # daily data types are to be processed by daily_cmip5.dk
  #  1 - (sub)daily 2D variables (for all runs/periods) --- suffix "_dd"
  #  2 - (sub)daily 3D variables on pressure levels (for some runs/periods) --- suffix "_dp"
  daily_cmip5_tiers='1 2'

  # gssave=on means (sub)daily 3D variables on model levels are selected in
  # daily_cmip5.dk, in addition to whatever is specified by daily_cmip5_tiers
  # These files are given the suffix "_ds"
  gssave=off

  # year_offset is required by the daily_cmip5 deck
  year_offset=0

#  * ............................ Condef Parameters ............................

  join=1
  auto=on
  nextjob=on
  noprint=on
  tnoprint=on

  datatype=specsig
  gcmtsav=on
  gcm2plus=on
  wxstats=on

  xtracld=off
  obsdat=off
  xtradif=on

  carbon=on
  ctem=on
  slab=off

  tmpsave=on

  splitfiles=on       # switch on splitting
  stat2nd=off         # no 2nd order statistics

#  * ............................. Deck Definition .............................

  # --------------------- Compute beta/del and process DATA DESCRIPTION records.
  . delmodinfo.dk

  # --------------------- Interpolate model data on pressure levels + PMSL.
  . gpintstat2.dk

  # --------------------- Model physics statistics (from gs files).
  . gsstats2.dk

  # --------------------- Tracer statistics.
  . xstats5.dk

  # --------------------- "xtrachem" diagnostics.
  . xtrachem.dk

  # --------------------- "xtraconv" diagnostics.
  . xtraconv.dk

  # --------------------------------- extra diagnostics.
  # . xtradust_bulk.dk
  # . aodpth_volc1.dk
  # . aerorf.dk

  # --------------------------------- COSP
  # . cosp_sim.dk

  # --------------------------------- Cloud microphysical budgets
  # . cldmic.dk

  # --------------------------------- CFMIP monthly radiative forcing
  # . cfmip_monthly_rad_force.dk

  # --------------------------------- aerosol radiative properties
  # . aodpth_volc.dk

  # --------------------------------- daily data for CMIP5 (dp and ds files)
  # . daily_cmip5.dk

  # --------------------------------- CFMIP extra daily fields
  # . daily_cmip5_cfmip_v2_dsd3D.dk

  # --------------------------------- COSP daily diagnostics.
  # . daily_cmip5_cosp_v2.dk

  # --------------------- Creation of cp file (from cm files)
  . cmstats2.dk

  # --------------------- cleanup
  . cleanall.dk

#end_of_job
