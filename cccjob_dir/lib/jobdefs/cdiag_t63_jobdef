#!/bin/sh
#=======================================================================
# Create coupled model diagnostic files (gp,xp,cp)     --- cdiag_t63 ---
# $Id: cdiag_t63_jobdef 657 2012-02-22 23:48:49Z acrnrls $
#=======================================================================
#     keyword :: cdiag_t63
# description :: coupled model diagnostics
#        hide :: yes

#******************************
#********* WARNING ************
#******************************
#
# auto, nonqs, nextjob, flabel, RCMRUN, runid, run, nqsext, crawork,
# mdest, [sgd]time, nnodex, nprocx, initmem, sv, vic, memory[0-9],
# queue, jobname, gcmjcl
#
# These variables are special in that they are extracted from the
# submission script prior to execution and used to parse file names,
# among other things. Therefore any of these variables whose definition
# depends on another variable that is not in this list will be improperly
# defined in this preprocessing step. This could lead to file names being
# incorrectly specified. The variables are extracted by copying the entire
# line on which they are defined from the submission script. Therefore
# variables that are used to define any of these variables must be defined
# on the same line as that variable and prior to it or on a previous line
# that contains any of these variables.
#
#******************************

set -a
. betapath2

#  * ........................... Parmsub Parameters ............................

 username="acrnxxx"; user="XXX";
 runid=aaa; days="MAM"
 crawork=${runid}_cdiag_t63; uxxx=uxxx; cdiag_t63_flabel_uxxx=$uxxx; cdiag_t63_model_uxxx=$uxxx
 jobname=cdiag_t63; cdiag_t63_flabel_prefix=${cdiag_t63_flabel_uxxx}_${runid}

 nqsprfx="x${runid}_"; nqsext='';
 RUNID=`echo "${runid}"|tr '[a-z]' '[A-Z]'`;

 run=COUPLED;

 # offset model year to yield
 # year in real time... may be negative.
 year_offset="0";

 # atmos_file and year_offset are only required for ts_save.dk
 atmos_file="sc_${runid}_${year}_m${mon}";

 # sst_file and sss_file are used by cp_save.dk
 sst_file="sc_${runid}_sst";
 sss_file="sc_${runid}_sss";

 #starting year of rtdiag.
 year_rdiag_start="007";

 # name of run time diagnostic file
 rtdfile="sc_${runid}_${year_rdiag_start}_yyy_rtd";

 nextjob=on
 noprint=on

 year="001"; mon="06"; monn="jan"; flabel="${cdiag_t63_flabel_prefix}_${year}_m${mon}_";
 memory99=1; cdiag_t63_model_prefix=${cdiag_t63_model_uxxx}_${runid}
 model1="${cdiag_t63_model_prefix}_${year}_m${mon}_";
 days="JUN"; obsday="JUN"; plpfn="${runid}_${year}_m${mon}_";

 resol="128_64";
 obsfile="obs_sfc_${resol}";
 obsfile2="pd_nmc_${resol}_1979_1988_m${mon}_";
#
  mask=land_mask_${resol};
#

 t1="        1"; t2="999999999"; t3="   1"; s3="   1";
 r1="        1"; r2="999999999"; r3="   1";
 g1="        1"; g2="999999999"; g3="   1";
 a1="        1"; a2="999999999"; a3="   1";
 lml="  995";  d="B";
 lay="    2";   coord=" ET15"; topsig="-1.00"; moist=" QHYB"; plid="      50.0";
 sref="   7.15E-3"; spow="        1.";
 m01="  012"; itrvar="QHYB";
 xref_LWC="        0."; xpow_LWC="        1.";
 xref_IWC="        0."; xpow_IWC="        1.";
 xref_BCO="        0."; xpow_BCO="        1.";
 xref_BCY="        0."; xpow_BCY="        1.";
 xref_OCO="        0."; xpow_OCO="        1.";
 xref_OCY="        0."; xpow_OCY="        1.";
 xref_SSA="   1.05E-8"; xpow_SSA="        1.";
 xref_SSC="   7.59E-8"; xpow_SSC="        1.";
 xref_DUA="   1.54E-7"; xpow_DUA="        1.";
 xref_DUC="   2.19E-7"; xpow_DUC="        1.";
 xref_DMS="  2.48E-10"; xpow_DMS="        1.";
 xref_SO2="   1.88E-9"; xpow_SO2="        1.";
 xref_SO4="   1.48E-9"; xpow_SO4="        1.";
 xref_HPO="        0."; xpow_HPO="        1.";
 xref_H2O="        0."; xpow_H2O="        1.";
 plv="   17"; p01="   10"; p02="   20"; p03="   30"; p04="   50"; p05="   70";
            p06="  100"; p07="  150"; p08="  200"; p09="  250"; p10="  300";
            p11="  400"; p12="  500"; p13="  600"; p14="  700"; p15="  850";
            p16="  925"; p17=" 1000"; p18="     "; p19="     "; p20="     ";
            p21="     "; p22="     "; p23="     "; p24="     "; p25="     ";
            p26="     "; p27="     "; p28="     "; p29="     "; p30="     ";
            p31="     "; p32="     "; p33="     "; p34="     "; p35="     ";
            p36="     "; p37="     "; p38="     "; p39="     "; p40="     ";
            p41="     "; p42="     "; p43="     "; p44="     "; p45="     ";
            p46="     "; p47="     "; p48="     "; p49="     "; p50="     ";
 kax="    1"; kin="    1"; lxp="    0"; map="    0"; b="  +";

 dtime="100"; gptime="600"; stime="600";

 plunit=VIC;
 lopgm="lopgm_o4xa";
 this_host=`hostname|cut -d'.' -f1`
 case $this_host in
   # This is pollux, use 64 bit binaries
   ib3-*) lopgm=lopgm_o4xal ;;
 esac

 oldiag="diag4";

 CCRNTMP=$CCRNTMP

 memory1="200mb"; memory2="200mb"; memory3="200mb";
 lrt="   63"; lmt="   63"; typ="    2";
 lon="  128"; lat="   64"; npg="    2";
 ncx="    2"; ncy="    2";
 delt="    900.0";

#  * ............................ Condef Parameters ............................

join=1
auto=on
nextjob=on
noprint=on
tnoprint=on
datatype=specsig
gcmtsav=on
gcm2plus=on
wxstats=on
xtracld=off
obsdat=off
xtradif=on
colourplots=off
lmstat=on

carbon=off
ctem=off
slab=off

tmpsave=on

splitfiles=on       # switch on the splitting mode.

#  * ............................. Deck Definition .............................

# --------------------------------- Run-time diagnostics.
. rtdiag35.dk
# --------------------------------- Compute beta/del and process MODEL INFO records.
. del.dk
. modinfo.dk
# --------------------------------- Interpolate model data on pressure levels.
. gpint.dk
# --------------------------------- Basic model statistics.
. mslpr_eta.dk
. gpstats.dk
# --------------------------------- Spectral statistics (optional, for reference runs only)
#. spvdqtz.dk
#. spmodinfo.dk
#. sphum.dk
#. spvort.dk
#. spdiv.dk
#. sptemp.dk
#. spintr.dk
#. vdelphi.dk
#. sppurg.dk
# --------------------------------- Tracer statistics (optional).
# #par_def
#   trac="X001 X002 X003 X004 X005 X006 X007 X008";
# . xstats.dk
# --------------------------------- Delete pressure-level files.
. gppurg.dk
# --------------------------------- Model physics statistics.
. mom_stat.dk
. eng_stat3.dk
. wat_stat.dk
. misc_stat.dk
. cloud8.dk
. sfc_stat.dk
. lmlstat.dk
# --------------------------------- Time series deck (optional)
#. ts_save.dk
# --------------------------------- Creation of cp file
. cp_save.dk
# --------------------------------- Creation of co file
#. co_save.dk
# --------------------------------- cleanup
. cleanup.dk

#end_of_job
