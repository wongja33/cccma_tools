#!/bin/sh
#=======================================================================
# Concatenate time series created by tseries.dk            --- tscat ---
# $Id: tscat_jobdef 657 2012-02-22 23:48:49Z acrnrls $
#=======================================================================
#     keyword :: tscat
# description :: Concatenate time series
#
# Larry Solheim  ...Mar,2008

 set -a
 . betapath2

 jobname=tscat ; time="10800" ; memory="8gb"

 # bail is a simple error exit routine
 bail(){
   echo "tscat: "$1
   echo " " >> haltit
   exit 1
 }

#  * ........................... Parmsub Parameters ............................

 # RUNPATH may be set if desired
 RUNPATH=$RUNPATH

 # It is often more efficient to set CCRNTMP = $RUNPATH
 CCRNTMP=$CCRNTMP

 # Alternate path to a directory where .queue/.crawork will be found
 JHOME=''

 if [ -n "$JHOME" -a x"$JHOME" != x"$HOME" ]; then
   # Allow optional reset of DATAPATH/RUNPATH
   JHOME_DATA=''
   DATAPATH=${JHOME_DATA:=$DATAPATH}
   RUNPATH=${JHOME_DATA:=$RUNPATH}
   # Allow optional reset of CCRNTMP
   JHOME_RUN=''
   CCRNTMP=${JHOME_RUN:=$CCRNTMP}
 fi

 # TS_del_after_cat flags the deletion of all input tscatNN files after
 # they have been concatenated into a single file. This will never happpen
 # unless the output file is saved (ie: tscat_save is set non null).
 # The default is to delete the input files when the output file is saved.
 TS_del_after_cat=on

 # TS_netcdf flags the creation of netcdf files from
 # data in the concatenated file created by tscat.dk
 TS_netcdf=off

 # TS_ccc2nc_opts will hold command line options for ccc2nc that are
 # to be used on all invocations of ccc2nc in tscat.dk
 TS_ccc2nc_opts=''

 # TS_nc_add_var flags inclusion of extra variables (e.g. VINF,PARM)
 # in the output netcdf files
 TS_nc_add_var=off

 # TS_nc_suffix is a string to add to the end of output netcdf file names
 TS_nc_suffix=''

 # TS_file_split flags writing multiple output files with one variable
 # per output file. Each individual file will be created if it does not
 # exist or appended to if it exists
 TS_file_split=''

 # tscat_split flags writing multiple output files with one variable
 # per output file. Each individual file will be created if it does not
 # exist or appended to if it exists
 tscat_split=''

# TS_file_append flags appending files created when TS_file_split=on
# to any existing file of the same name. If TS_file_append=off then multiple
# editions of the same file will be created. These multiple editions must
# then be concatentated to form a single time series later on.
# The default is TS_file_append = on.
 TS_file_append=''

 # TS_plot flags the creation of a set of time series plots
 # from data in the concatenated file created by tscat.dk
 TS_plot=off

 # TS_yoff is a year offset used to set time axis values
 # when TS_plot is set 'on'. This does not affect any fields
 # written to the concatenated output file.
 TS_yoff=0

 # TS_ptitle will form part of the plot title of every plot
 TS_ptitle=''

 # plunit is only used when TS_plot is set 'on'
 plunit="VIC"

 nextjob=on
 noprint=on
 debug=off

 # This is required for ocean data or high resolution atm files
 lopgm=lopgm_o4xa
 this_host=`hostname|cut -d'.' -f1`
 case $this_host in
   # This is pollux, use 64 bit binaries
   ib3-*) lopgm=lopgm_o4xal ;;
 esac

 # tscat_suffix_list is a white space separated list of suffixes
 # of file names to be generated here. Any suffix in this list may be
 # modified by appending a + followed by a comma separated list of
 # numbers (no white space is allowed within this modifier). Each
 # number within the modifier list will correspond to a month (1-12)
 # for which a file with this suffix is to be included. If the
 # modifier exists for a particular suffix then only those months
 # indicated in the modifier will be added to the file list. If no
 # modifier exists for a particular suffix then all months with that
 # suffix will be added to the file list.
 tscat_suffix_list='ts'

 # These variables are set when the job string is created
 run_start_year=NotSet
 run_start_month=NotSet
 run_stop_year=NotSet
 run_stop_month=NotSet
 current_year=NotSet
 current_month=NotSet
 previous_year=NotSet
 previous_month=NotSet
 next_year=NotSet
 next_month=NotSet

 year=yyy; mon=mm   # memory99=1

 runid="job000"; uxxx='uxxx'; tscat_uxxx=$uxxx;
 tscat_prefix="${tscat_uxxx}_${runid}"   # memory99=1
 tscat_prefix_=${tscat_prefix}_          # memory99=1
 crawork="${runid}_job"; username="acrnxxx"; user="XXX";

 # ---Start_submit_ignore_code----

 # Time series files used will be for months in the range from
 # tscat_start_year,tscat_start_mon to tscat_end_year,tscat_end_mon
 [ "$current_year" = "NotSet" -o "$current_month" = "NotSet" ] && \
   bail "current_year or current_month is not set"
 if [ "$previous_year" = "NotSet" -o "$previous_month" = "NotSet" ]; then
   [ "$next_year" = "NotSet" -o "$next_month" = "NotSet" ] && \
     bail "tscat: Neither previous_(year|month) nor next_(year|month) are set"
   tscat_start_year=$current_year
   tscat_start_mon=$current_month
   tscat_end_year=$next_year
   tscat_end_mon=$next_month
 else
   tscat_start_year=$previous_year
   tscat_start_mon=$previous_month
   tscat_end_year=$current_year
   tscat_end_mon=$current_month
 fi
 tscat_start_year=`echo $tscat_start_year|awk '{printf "%04d",$1}' -`
 tscat_start_mon=`echo $tscat_start_mon|awk '{printf "%02d",$1}' -`
 tscat_end_year=`echo $tscat_end_year|awk '{printf "%04d",$1}' -`
 tscat_end_mon=`echo $tscat_end_mon|awk '{printf "%02d",$1}' -`

 # Perform checks on variables that are set dynamically
 [ -z "$tscat_start_year" ] && bail "tscat_start_year is null"
 [ -z "$tscat_end_year" ]   && bail "tscat_end_year is null"
 [ -z "$tscat_start_mon" ]  && bail "tscat_start_mon is null"
 [ -z "$tscat_end_mon" ]    && bail "tscat_end_mon is null"
 [ $tscat_start_mon -gt 12 -o $tscat_start_mon -lt 1 ] &&\
   bail "tscat_start_mon=$tscat_start_mon is out of range"
 [ $tscat_end_mon -gt 12 -o $tscat_end_mon -lt 1 ] &&\
   bail "tscat_end_mon=$tscat_end_mon is out of range"
 [ $tscat_start_year -gt $tscat_end_year ] &&\
   bail "tscat_start_year=$tscat_start_year is out of range"

 # range is a string of the form YYYmMM_YYYmMM indicating the start
 # and end year/month, to be used in the output file name
 range="${tscat_start_year}m${tscat_start_mon}"
 range="${range}_${tscat_end_year}m$tscat_end_mon"

 # TS_ccmval flags CCMVal specific options
 TS_ccmval=''

 # tscat_ccmval_fid is used as part of CCMVal file names when tscat_split=on
 tscat_ccmval_fid=''

 # tscat_save is the name under which the concatenated file will be
 # saved to DATAPATH/RUNPATH. If tscat_save is null or not set then
 # the output file will not be saved.
 tscat_save=${tscat_prefix_}${range}_ts

# TS_force_tscat=on tells tscat.dk to run tscat even when there is
# only 1 input file supplied
  TS_force_tscat=${TS_force_tscat:=0}
  XXX=`echo $TS_force_tscat|sed 's/ //g'`
  eval TS_force_tscat\=$XXX
  [ "$TS_force_tscat" = 'on'  ] && eval TS_force_tscat\=1
  [ "$TS_force_tscat" = 'off' ] && eval TS_force_tscat\=0
  [ "$TS_force_tscat" = 'yes' ] && eval TS_force_tscat\=1
  [ "$TS_force_tscat" = 'no'  ] && eval TS_force_tscat\=0

 # If any of the following file names are defined then use them as
 # input to tscat rather than generating a list of file names
 # in the loop below. This allows a user to specify file names on
 # the cccjob command line that will override internally generated
 # file names.
 # tscatNN variable definitions for NN > 20 may be added on the cccjob
 # command line by appending ":A" to the variable definition (e.g. tscatNN=val:A)
 tscat01=''
 tscat02=''
 tscat03=''
 tscat04=''
 tscat05=''
 tscat06=''
 tscat07=''
 tscat08=''
 tscat09=''
 tscat10=''

 # Use TS_file as an alternative name for tscat01 when tscat01 is not set.
 TS_file=${TS_file:=''}
 # The user may override this by setting tscat_use_TS_file off (or 0).
 tscat_use_TS_file=${tscat_use_TS_file:=1}
 XXX=`echo $tscat_use_TS_file|sed 's/ //g'|tr '[A-Z]' '[a-z]'`
 eval tscat_use_TS_file\=$XXX
 [ "$tscat_use_TS_file" = 'on'  ] && eval tscat_use_TS_file\=1
 [ "$tscat_use_TS_file" = 'off' ] && eval tscat_use_TS_file\=0
 [ "$tscat_use_TS_file" = 'yes' ] && eval tscat_use_TS_file\=1
 [ "$tscat_use_TS_file" = 'no'  ] && eval tscat_use_TS_file\=0
 if [ $tscat_use_TS_file -eq 1 ]; then
   [ -z "$tscat01" ] && tscat01=$TS_file
 fi

 # If TS_file_clobber is set to on then the file named in TS_file
 # will always be overwritten
 TS_file_clobber=off

 # <<INSERT>> any user supplied variable definitions here.

 if [ -n "$tscat01" ]; then
   # Only test tscat01 since it must be set if any of the others are
   # The user has supplied tscatNN file names on the command line
   echo "TSCAT: file names specified on the command line:"
   nn=0
   while [ $nn -lt 999 ]; do
     if [ $nn -lt 99 ]; then
       nn=`echo $nn|awk '{printf "%02d",$1+1}' -`
     else
       nn=`echo $nn|awk '{printf "%04d",$1+1}' -`
     fi
     eval tsfile=\$tscat$nn
     [ -z "$tsfile" ] && break
     echo "    $tsfile"
   done
   # When more than 1 tscatNN is defined the default is to not save any output file
   # The user must explicitly set tscat_save to save the output
   tscat_save=''
   if [ $nn -eq 2 ]; then
     # tscat01 is the only file defined
     # Assume the user wants to run tscat on this file and resave it
     tscat_save=$tscat01
     # Run tscat even though there is only 1 input file supplied
     TS_force_tscat=on
   fi
   # By default, do not delete any input files in this case
   TS_del_after_cat=off

 else

   # Generate a list of time series file names and assign these names
   # to variables tscat01, tscat02, ...
   tscat_curr_year=`echo $tscat_start_year|awk '{y=$1-1;printf "%04d", y}' -`
   nnts=0
   while [ $tscat_curr_year -lt $tscat_end_year ]; do
     tscat_curr_year=`echo $tscat_curr_year|awk '{y=1+$1;printf "%04d", y}' -`
     if [ $tscat_curr_year -eq $tscat_start_year ]; then
       mm=`echo $tscat_start_mon|awk '{m=$1-1;printf "%02d", m}' -`
     else
       mm=0
     fi
     if [ $tscat_curr_year -eq $tscat_end_year ]; then
       mm_end=$tscat_end_mon
     else
       mm_end=12
     fi
     while [ $mm -lt $mm_end ]; do
       mm=`echo $mm|awk '{m=1+$1;printf "%02d", m}' -`

       # tscat_file_pat, if set and non null, will contain a pattern that will be
       # used to generate file names. This pattern can contain variables such as
       # tscat_curr_year or mm (set in this loop), or runid etc (set above)
       # tscat_file_pat must be defined here and will be reset when the job string
       # is created, if defined by the user.
       tscat_file_pat=''
       if [ -n "$tscat_file_pat" ]; then

         # Generate file names from a user supplied pattern
         # This pattern can contain variables such as runid, tscat_curr_year and mm
         if [ $nnts -lt 99 ]; then
           nnts=`echo $nnts|awk '{printf "%02d",$1+1}' -`
         else
           nnts=`echo $nnts|awk '{printf "%04d",$1+1}' -`
         fi
         eval tscat${nnts}=$tscat_file_pat

       else

         bname=${tscat_prefix_}${tscat_curr_year}_m${mm}_
         for suffix in $tscat_suffix_list; do
           mlist=`echo $suffix|awk -F'+' '{print $2}' -`
           mlist=`echo $mlist|sed 's/,/ /g'`
           if [ -n "$mlist" ]; then
             suffix=`echo $suffix|sed 's/+.*$//'`
             # assume that mlist is a white space separated list of numbers
             # indicating which months to dump
             for xx in $mlist; do
               if [ $xx -eq $mm ]; then
                 if [ $nnts -lt 99 ]; then
                   nnts=`echo $nnts|awk '{printf "%02d",$1+1}' -`
                 else
                   nnts=`echo $nnts|awk '{printf "%04d",$1+1}' -`
                 fi
                 eval tscat${nnts}=$bname$suffix
               fi
             done
           else
             if [ $nnts -lt 99 ]; then
               nnts=`echo $nnts|awk '{printf "%02d",$1+1}' -`
             else
               nnts=`echo $nnts|awk '{printf "%04d",$1+1}' -`
             fi
             eval tscat${nnts}=$bname$suffix
           fi
         done

       fi

     done
   done
 fi

 # Used for debugging
 if [ 1 -eq 2 ]; then
 # List the file names generated above
   nn=0
   echo "Files passed to tscat.dk ::"
   while [ $nn -lt 999 ]; do
     if [ $nn -lt 99 ]; then
       nn=`echo $nn|awk '{printf "%02d",$1+1}' -`
     else
       nn=`echo $nn|awk '{printf "%04d",$1+1}' -`
     fi
     eval tsfile=\$tscat$nn
     [ -z "$tsfile" ] && break
     echo "${nn}) $tsfile"
   done
  fi

 # ---Stop_submit_ignore_code----

#  * ............................. Deck Definition .............................

 . tscat.dk

#end_of_job
