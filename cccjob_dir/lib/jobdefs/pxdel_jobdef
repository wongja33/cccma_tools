#!/bin/sh
#=======================================================================
# Delete files on the back end after a parallel dump job   --- pxdel ---
# has finished. This module will create a delete job and submit it to
# the back end to remove the files 
# $Id: pxdel_jobdef 659 2012-03-21 21:56:58Z acrnrls $
#=======================================================================
#           keyword :: pxdel
# short description :: delete back end files from pdump
#              hide :: yes
 set -a
 . betapath2

#  * ........................... Parmsub Parameters ............................

 jobname=pxdel; time="1800" ; memory="900mb" ;

 noprint=on
 nextjob=on

 username="acrnxxx"; user="XXX";
 runid=job000; uxxx='uxxx'; crawork="${runid}_job"
 nqsprfx="${runid}_"; nqsext=''

 # Temporary directory where this script will run
 CCRNTMP=$CCRNTMP

 # RUNPATH on execution machine
 RUNPATH=$RUNPATH

 # Alternate path to a directory where .queue/.crawork will be found
 JHOME=''

 if [ -n "$JHOME" -a x"$JHOME" != x"$HOME" ]; then
   # Allow optional reset of DATAPATH/RUNPATH
   JHOME_DATA=''
   DATAPATH=${JHOME_DATA:=$DATAPATH}
   RUNPATH=${JHOME_DATA:=$RUNPATH}
   # Allow optional reset of CCRNTMP
   JHOME_RUN=''
   CCRNTMP=${JHOME_RUN:=$CCRNTMP}
 fi

 . comjcl.cdk
 
cat > Execute_Script <<'end_of_script'

  # ---Start_submit_ignore_code----
  set -a

  # Define a unique stamp for use in file names etc
  stamp=`date "+%j%H%M%S"$$`

  # Use -e option (enable interpretation of backslash escapes) if recognized by echo
  if [ "X`echo -e`" = "X-e" ]; then
    echo_e() { echo ${1+"$@"}; }
  else
    echo_e() { echo -e ${1+"$@"}; }
  fi

  # bail is a simple error exit routine
  # Note: we write the error directly to a file in ~/.queue so that this
  #       info is not lost if/when stdout is not returned
  error_out="${JHOME:-$HOME}/.queue/error_pxdel_${runid}_$stamp"
  [ ! -z "$error_out" ] && rm -f $error_out
  bail(){
    echo_e `date`" --- pxdel: $*"
    echo_e `date`" --- pxdel: $*" >>$error_out
    exit 1
  }

#  * ........................... Parmsub Parameters ....................

  runid=job000; uxxx='uxxx'; pxdel_uxxx=$uxxx; pxdel_prefix="${pxdel_uxxx}_${runid}";
  crawork="${runid}_job"; username="acrnxxx"; user="XXX";

  # These variables are set when the job string is created
  previous_year=NotSet
  previous_month=NotSet

  current_year=NotSet
  current_month=NotSet

  next_year=NotSet
  next_month=NotSet

  run_start_year=NotSet
  run_start_month=NotSet
  run_stop_year=NotSet
  run_stop_month=NotSet

  # This invocation of make_file_name_list will process the *_year and *_months
  # variables defined above and output a file containing definitions for
  # start_year, start_mon, stop_year, stop_mon
  tmp_file_list="pxdel_date_list_${runid}_${stamp}"
  make_file_name_list --dates_only $tmp_file_list >>$error_out 2>&1 ||
    bail "Problem in make_file_name_list"
  rm -f $error_out

  # Verify that the output list is not empty
  [ ! -s "$tmp_file_list" ] && bail "Unable to create dates list"

  # A file list was created ...source it
  # This will define start_year, start_mon, stop_year, stop_mon
  : ; . $tmp_file_list
  rm -f $tmp_file_list

  pxdel_lock_check=on
  if [ x"$pxdel_lock_check" = xon ]; then
    # Check on the existence of various lock files
    # If any of these files exists then abort the delete of files on the back end

    # Define a directory used to hold persistent files (lock files etc)
    persistd="${JHOME:-$HOME}/.queue/.crawork"
    [ -z "$persistd" ] && bail "persistd is not set"

    # last_year is used in the following checks for lock files
    last_year=`echo $current_year|awk '{printf "%04d",$1-1}' -`

    # pxdel_check_last_mon may be set to limit the range of lock files
    # that will cause an abort.
    pxdel_check_last_mon=off

    # Check for any lock files from pullfe with the current runid
    # and the current year or previous year.
    # Do not delete any history files if any of these lock files exists

    # lock files are of the form:
    # lock_${module}_${runid}_${year1}m{$mon1}_${year2}m{$mon2}_NNNNNN
    # where module is one of pdump, mdump, pullfe, diag, rtrans, ...
    #       year1/mon1 to year2/mon2 identifies the time period
    #       NNNNNN is a unique tag

    check_lock_files() {
      ftype=$1
      [ -z "$ftype" ] && bail "Empty file type specified in check_lock_files"
      [ -z "$runid" ] && bail "runid is not defined in check_lock_files"
      case $ftype in
        mdump|pdump|pullfe|pdiag|pxdiag|rtd) ;;
        *)  bail "Invalid file type specified in check_lock_files $ftype"
      esac
      lock_pfx="${persistd}/lock_${ftype}_${runid}_"
      if [ x"$pxdel_check_last_mon" = xon ]; then

        # When pxdel_check_last_mon is set then only look for lock files that contain
        # ${stop_year}m${stop_mon} as the second date in the date range
        # 3 digit year and ${stop_year}m${stop_mon} is second date
        lock_file_def="$lock_pfx"'[0-9][0-9][0-9]m??'"_${stop_year}m${stop_mon}"'_*'
        found_lock=`(ls -1 ${lock_file_def} || : ) 2>/dev/null`
        [ -n "$found_lock" ] && bail "Existing lock file(s): $found_lock"
        # 4 digit year and ${stop_year}m${stop_mon} is second date
        lock_file_def="$lock_pfx"'[0-9][0-9][0-9][0-9]m??'"_${stop_year}m${stop_mon}"'_*'
        found_lock=`(ls -1 ${lock_file_def} || : ) 2>/dev/null`
        [ -n "$found_lock" ] && bail "Existing lock file(s): $found_lock"

      else

        # current_year is either year1 or year2 in _${year1}m{$mon1}_${year2}m{$mon2}_
        # 3 digit year and current_year is year2
        lock_file_def="$lock_pfx"'[0-9][0-9][0-9]m??'"_${current_year}m"'??_*'
        found_lock=`(ls -1 ${lock_file_def} || : ) 2>/dev/null`
        [ -n "$found_lock" ] && bail "Existing lock file(s): $found_lock"
        # 4 digit year and current_year is year2
        lock_file_def="$lock_pfx"'[0-9][0-9][0-9][0-9]m??'"_${current_year}m"'??_*'
        found_lock=`(ls -1 ${lock_file_def} || : ) 2>/dev/null`
        [ -n "$found_lock" ] && bail "Existing lock file(s): $found_lock"
        # current_year is year1
        lock_file_def="${lock_pfx}${current_year}m"'??_*'
        found_lock=`(ls -1 ${lock_file_def} || : ) 2>/dev/null`
        [ -n "$found_lock" ] && bail "Existing lock file(s): $found_lock"

        # Last year's DEC lock file
        # last_year year2 in _${year1}m{$mon1}_${year2}m12_
        # 3 digit year and last_year is year2
        lock_file_def="${lock_pfx}"'[0-9][0-9][0-9]m??'"_${last_year}m12_"'*'
        found_lock=`(ls -1 ${lock_file_def} || : ) 2>/dev/null`
        [ -n "$found_lock" ] && bail "Existing lock file(s): $found_lock"
        # 4 digit year and last_year is year2
        lock_file_def="${lock_pfx}"'[0-9][0-9][0-9][0-9]m??'"_${last_year}m12_"'*'
        found_lock=`(ls -1 ${lock_file_def} || : ) 2>/dev/null`
        [ -n "$found_lock" ] && bail "Existing lock file(s): $found_lock"

      fi

      # We need a zero exit status from this function for sh on pollux
      # so this echo should remain as the last command
      echo "No $ftype lock files found for years ${last_year}, $current_year"
    }

    check_lock_files mdump
    check_lock_files pdump
    check_lock_files pdiag
    check_lock_files pxdiag
    check_lock_files pullfe
    rtd_lock_file=''
    if [ -n "$rtd_lock_file" ]; then
      # Check for any lock files from rtdiag for the current year
      # Do not delete any history files if any of these lock files exists
      check_lock_files rtd
    fi
  fi

  # Define a unique stamp for use in file names etc
  stamp=`date "+%j"$$`

  # Identify the name of the back end machine
  back_end_mach=''
  [ -z "$back_end_mach" ] && bail "back_end_mach is not set"

  # Insert the list of files to be deleted
  # <<INSERT_PXDEL>>

  # join must be included in the inserted list of files names and be
  # assigned an integer value which is the number of files in the list
  [ -z "$join" ] && bail "join is not set"
  [ $join -le 0 ] && bail "Invalid join=$join"

  # Create a file containing the list of file names to delete
  nf=0
  rm -f file_list
  touch file_list
  while [ $nf -lt $join ]; do
    nf=`expr $nf + 1`
    eval local_file=\$file$nf
    [ -z "$local_file" ] && bail "Invalid file name in file$nf"
    # Create this list in a format that may be
    # inserted directly into a delete job
    echo "  file${nf}=$local_file" >> file_list
  done

  # Append the number of files to file_list
  echo "  join=$join" >> file_list

  # Write the file list to stdout for inspection if/when this job dies
  echo "pxdel: file_list"
  cat file_list

#  * ............................ Condef Parameters ............................

  noprint=on       # do not return stdout to ~/.queue
  nextjob=on       # more jobs in the job string will follow

#  * ............................. Deck Definition .............................

  # Require cccjob
  which cccjob || bail "cccjob is not in your path"

  # Indicate how cccjob should be invoked
  # Setting CCCJOB_ROOT will allow a job specific version of cccjob to used
  CCCJOB_ROOT=''
  if [ -z "$CCCJOB_ROOT" ]; then
    CCCJOB_ENV=''
  else
    eval CCCJOB_ENV=\'env CCCJOB_ROOT\=$CCCJOB_ROOT\'
  fi

  # Provide command line defs for JHOME and JHOME_DATA
  # if these variables are defined in the current env
  JHOME_opt=''
  [ -n "$JHOME" ]      && JHOME_opt="JHOME=$JHOME "
  [ -n "$JHOME_DATA" ] && JHOME_opt="$JHOME_opt JHOME_DATA=$JHOME_DATA "
  [ -n "$JHOME_RUN" ]  && JHOME_opt="$JHOME_opt JHOME_RUN=$JHOME_RUN "

  # Create the delete job string
  $CCCJOB_ENV cccjob --out=pxdel_job --job="del_list=file_list:s" \
    runid=pxdel months=12 $JHOME_opt

  # Create a temporary directory in ~/tmp to hold submission files
  psubdir=${JHOME:-$HOME}/tmp/tmp_pxdel_$stamp
  mkdir -p $psubdir || bail "Cannot create $psubdir"

  # Submit this job to run on the back end
  cp pxdel_job $psubdir
  ssh $back_end_mach "cd ${psubdir}; rsub mdest=$back_end_mach pxdel_job" 2>&1 || \
    bail "***ERROR*** Remote submission failed: rsub mdest=$back_end_mach pxdel_job\n"

  # Clean up the temporary directory
  rm -fr $psubdir

  # sucessful completion
  exit 0

  # ---Stop_submit_ignore_code----

end_of_script

 . endjcl.cdk

#end_of_job

