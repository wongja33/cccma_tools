#!/bin/sh
#     keyword :: canesm_agcm
# description :: AGCM model basefile
###############################################################################
# Apr 01/2015 - ML.          --- v01 ---
# 1. This file is the official CGCM basefile for frozen area version CanAM4.4
#    (gcm18-based).
###############################################################################
 set -a
 . betapath2   # sets path for beta test version of new diagnostics package

### parmsub ###

#  * ........................... Parmsub Parameters ............................

 runid=job000; uxxx=mc; prefix="${uxxx}_${runid}"; crawork=${runid}_job;
 RUNID=`echo "${runid}"|tr '[a-z]' '[A-Z]'`;
 production=1

 modver=gcm18;
 gcmparm=gcmparm ; stime=3600 ; memory1=1000mb
 lopgm=lopgm; oldiag=diag4;

 # NOTE: use_NEMO must not be defined in the cppdefs_file
 use_nemo=off
 use_cancpl=on

 year="yyyy"; mon="mm"; year_restart="yyyy"; mon_restart="mm";
 months=1; months_run=1;kfinal="0000000000";
 model=${prefix}_${year}_m${mon}_;
 start=${prefix}_${year_restart}_m${mon_restart}_;
 job="$runid"; run="${RUNID}_${year}_M${mon}";

 username="acrnxxx"; user="XXX"; nqsprfx="${model}"; nqsext="";

 initime=1801;  initmem="15000mb";
 gcmtime=10800; gcmmem="15000mb";

 iyear=" 1850"; iday="  001"; gmt=" 0.00"; incd="    1";

 runmode=SPBC-AGCM
 model_variant=p2

 # post-processing switch
 # with_post_processing = 1 means to post-process model output on frontend
 # with_post_processing = 0 means to run model only
 with_post_processing=1

 # exexecutable names
 agcm_exec="canam.exe"
 coupler_exec="cancpl.exe"

 # 3-node configuration (CanAM: 3 nodes, CanCPL - 1 node shared with AGCM)
 phys_node=3
 nproc_a=2
 nnode_a=32
 nproc_o=1
 nnode_o=0
 nproc_c=1
 nnode_c=1
 cmd_runp1='PMI_NO_FORK=1 aprun -n 33 -N 11 -d 3 -j 1 -cc depth env OMP_NUM_THREADS=3 ./gcmcpl_script '
 cmd_runp2=' '
 cmd_runp3=' '

 # Number of MPI tasks
 nnodex=33
 # Number of physical nodes to map the job to
 phys_nodex=$phys_node

 isoc="    1"; ires="    1";
 ifdiff="    0"; isen="    1";

 issp="   24"; isgg="   24"; israd="   12"; isbeg="   12";
 jlatpr="    0";icsw="    4"; iclw="    4";  iepr="    0";  istr="   24";
   ishf="    4";isst="    2"

 # A netcdf file containing the horizontal grid description for the atm
 # atm_grid_desc=grid_desc_t63_o_gcm3.5_landmask_128x64v5.nc
 atm_grid_desc=''

 # A netcdf file containing the horizontal grid description for the ocean
 # ocn_grid_desc=grid_desc_ocnt_o_uwl_canesm4_1_1_256x192l40_datadesc.nc
 # ocn_grid_desc=grid_desc_orca1_nemo_3.4_orca1_coordinates_ukorca1_with_mask.nc
 ocn_grid_desc=''

 # A netcdf file containing the horizontal grid description for the CanOM4 ocean
 # canom4_grid_desc=grid_desc_ocnt_o_uwl_canesm4_1_1_256x192l40_datadesc.nc

 # A restart from a CanESM4 coupled run
 # canesm4_cpl_restart=mc_cpl_gcm18_init_2320_m12_cs

 # Residual climatology
 resid=pa_gcm17_mix18_64bit_2004m01_2008m12_res;

# AMIP BC files - make sure it is consistent with coupler_specified_bc_file CPP flag below!
 coupler_specified_bc_file='';
 coupler_specified_bc_file_sicn=td_pcmdi-amip-1-1-3-v1_187001-201706_gp_128_64_sicn;
 coupler_specified_bc_file_gt=td_pcmdi-amip-1-1-3-v1_187001-201706_gp_128_64_gt;
 coupler_specified_bc_file_sic=td_orap5-piomas-v1_187001-201706_gp_128_64_sic;
 coupler_specified_bc_file_clim='';
 coupler_specified_bc_file_gtano='';

 pakgcm=pakgcm9

 icntrldat=icntrldat2
 nlcjcl=nlcjcl2

 gpinit=ie791gp
 inatmo=inatmo12
 inphys=inphys19

 llphys=iph18lp
 veg=glc2000_1_12x1_12_v2
 soci=mksmksrf_soilcol_global_c090324_cccma

 oz=ozclim_zonal_cmip6
 oz_chem=randel_ozclim_3d_1980_1999

# oz_rad_trans/oz_chem_trans are the names of the transient ozone concentration data
# (rcp26,rcp45,rcp85 - choose one!).
# NOTE!!! Use rcp45 for historical runs!!!!
 oz_rad_trans=cmip6_radozone_histo_picontrol_1850_2014_monthly_128_64
# oz_chem_trans=rcp45_ozone_chem_1850_2100_monthly_128_64

 ssti=ssti_amip2_360_180
 mtnfile=opt_phis_t63
 spfilt=hoskfilt_r_12_n61_t63_tst
 maskfile=t63_to_orca1_fland_caspian
 flaku=t63_lake_param_nn_pp_adj_antarctic_128x64_0.5
 flakr=flakr_zero
 subfle=etopo5_gau_020_opt_200_129_64
 albtable=nasa_albedo_lookup_table_v3
 snow_rt_lut=snow_rt_lut2

# files for PLA
actdat1=''
coagdat=''
ssdat=''

# Files for gas phase chemistry. When running with switch_gas_chem=TRUE, set
# the correct names for these files under the chem runmode on canesm.cfg.
#
# ---- 1. file for initialization of gas-phase chemistry tracers; reference year
 initspe=''
 init_ref_yr=''
# ---- 2. transient stratospheric aerosol surface area density
 ssadtran=''
# ---- 3. climatological tropospheric sulphate surface area
 initso4=''
# ---- 4. clear-sky photolysis rate look-up tables
 jtab1=''
 jtab2=''
# ---- 5. files of emissions on model grid
 emisfile=''
 acftemfl=''

# llchem is the name of the non-transient aerosol emissions forcing data.
# llchem=ipemi_cl_128_64;
# llchem=ipemi_cl_128_64_duemi_orilai;
  llchem=ipemi_cl_128_64_duemi_orilai_l10_suz0;

# lltrans is the name of the transient aerosol emissions forcing data.
# CMIP6
  lltrans=input4mips_emissions_cmip_175001_201412_128_64_v1;

# Historic+Future (IPCC) for RCP2.6, RCP4.5 and RCP8.5 (rcp26,rcp45,rcp85 - choose one!):
# NOTE!!! Use rcp45 for historical runs!!!!
#  lltrans=urks_ar5_rcp45_1850_2100_128_64_int;
#  lltrans=emi_ar5_rcp45_1850_2100_128_64;

# Old Historic to 2005(IPCC):
# lltrans=emi_ipcc_hist_128_64;
# Historic (old approach):
# lltrans=ipchmphs_tr_128_64;

 # ghg_scenario is the name of a file containing all GHG time series data
 # for a particular scenario.
 # If ghg_scenario is not set then GHG time series will be set to a CONSTANT
 # default value.
 ghg_scenario=mole_fraction_in_air_input4MIPs_GHGConcentrations_CMIP_UoM-CMIP-1-2-0_gr1-GMNHSH_0000-2014_v1

# datadest=pollux; datadir=/data/ccrn/r04

 # solvarfile contains time depent values of perturbations to the solar constant
 # broken down into separate values for radiation bands and sub bands
 # CMIP5 dataset
 #solvarfile=hist_15g_solar_cmip5_lean_6may09_1850_2300_monthly_anom
 # CMIP6 dataset
 solvarfile=solarforcing-ref-mon_input4MIPs_solar_CMIP_SOLARIS-HEPPA-3-2_gn_18500101-22991231_anom_v1

 # The "vtau_offset_year" offset year for the file with the stratospheric aerosols.
 #
 # vtau_offset_year  =   0 (default) read the forcing data for the same year as the model year
 #                   =  +N read the forcing data for the year ($year + N)
 #                   =  -N read the forcing data for the year ($year - N)
 vtau_offset_year=0
 # year_vtau_offset = $year + $vtau_offset_year
 year_vtau_offset=`echo $year $vtau_offset_year | awk '{printf "%04d",$1+$2}'`

 # Transient volcanic aerosol optical depths from Sato et al (4 lat bands, monthly)
 # vtaufile=sato_volcanic_4_tau_t63_1850_2300_monthly
 # Transient volcanic aerosol optical depths from Vernier (4 lat bands, monthly)
 # vtaufile=ver_sol_2011_volcanic_tau_t63_1985_2022_monthly
 # Transient stratospheric aerosol optical properties from ETHZ (latitude, 70 levels, monthly)
 vtaufile=cmip_canesm_radiation_185001-201412_v3.0.1
 # "Control" 1850 stratospheric aerosol optical properties from ETHZ (latitude, 70 levels, monthly)
 # vtaufile=cmip_canesm_radiation_1850_control_v3.0.1

 # Tropopause climatology derived from a 5 year t63, 71 level gcm13d run
 tropclim=trop_clim_ms_z_t63_monthly

 emis_co2_file="co2_nolu_rcp45_1850-2100_128x64_mm"

 # initpool     = 0               initialize pools from zero
 #              = year            initialize pools from that year from file on disk
 #              = negative value  initialize pools from coupler restart file
 #
 # lndcvrmod    = 12345  continuously update land cover from 1850 to 2100
 #              = year   use the land cover of July of that year continously
 #
 # lndcvr_offset = offset between model year and calendar year (model + offset = calendar)
 # the following line initializes CTEM pools from zero
 # initpool="00000"; lndcvrmod="01850"; lndcvr_offset="00000";
 initpool="-1000"; lndcvrmod="12345"; lndcvr_offset="00000";

# ctem_an="uvalsml_t63_ctem_an_file_with_wetlands";
 ctem_an="ctem_an_file_for_cmip6_sep_2017_129_64";

# lndcvr="uvalsml_luh_av1_1850_2100_rcp45_land_cover_crops_linear_128_64";
 lndcvr="uva_cmip6_luh_v2h_land_cover_1850_2014_linear_128_64_aug_2017";

 # T63 iron mask
 ironmask="uwl_t63_ironmask_256x192l40_10_levels_only"

 # The following radiative forcing parameters are only used when
 # "#define radforce" is defined in the model update section below.

 # assign the radiative forcing scenario identifier
 # valid values :: SRESA1B SRESA2 SRESB1 2XCO2 4XCO2 2X4XCO2 CFMIPA TEST VOL AERORF PAMRF GHAN 4XCO2_GHAN
 RF_SCENARIO=TEST

 # trop_idx is the model level index at which the tropopause is assumed
 # to exist for the adjusted radiative forcing case. If trop_idx=0 then
 # instantaneous radiative forcing will be used. If trop_idx<0 then a
 # tropopause height will be generated internally by the model.
 # trop_idx=-1  # adjusted radiative forcing
 trop_idx=0  # instantaneous radiative forcing

 # start_year_r is the starting year for an IPCC experiment (e.g. 1850)
 # It is used only in certain scenarios, currently SRESA1B SRESA2 and SRESB1
 start_year_r=1850

 # NRFP is the number of secondary radiation calls that will be made
 # This must be consistent with the value of RF_SCENARIO
 case $RF_SCENARIO in
   SRESA1B|SRESA2|SRESB1)
            NRFP=1
            start_year_r=1850 ;;
     2XCO2) NRFP=1 ;;
   2X4XCO2) NRFP=2 ;;
4XCO2_GHAN) NRFP=2 ;;
    CFMIPA) NRFP=3 ;;
    AERORF) NRFP=6 ;;
     PAMRF) NRFP=6 ;;
         *) NRFP=1 ;;
 esac

#/ This is for saving high-frequency output at select points.
 cf_sites="off"
 cf_sites_file="cmip6-cfsites-locations_v1.txt"

# Required parmsub parameters for gcmparm
# (* denotes a critical parameter which must appear in the ##PARC section)
#ex   * ilev   = number of vertical model    levels                (e.g.  35)
#ex   * levs   = number of vertical moisture levels                (e.g.  35)
#ex   * lonsld = number of longitudes         on the dynamics grid (e.g. 144)
#ex   * nlatd  = number of gaussian latitudes on the dynamics grid (e.g.  72)
#ex     lond   = number of grid points per slice for dynamics      (e.g. 144)
#ex   * lonsl  = number of longitudes         on the physics  grid (e.g.  96)
#ex   * nlat   = number of gaussian latitudes on the physics grid  (e.g.  48)
#ex     lon    = number of grid points per slice for physics       (e.g.  96)
#ex   * lmt    = spectral truncation wave number                   (e.g.  47)
#ex     ioztyp = switch for input ozone distribution               (e.g.   2)
#ex   * ntrac  = total number of tracers in the model              (e.g.  17)
#ex   * ntraca = number of advected tracers in the model           (e.g.  12)
#ex     nnode_a= number of smp nodes used to run atm (mpi for nnode_a>1)
#ex     ntld   = number of land  tiles in CLASS
#ex     ntlk   = number of lake  tiles in CLASS
#ex     ntwt   = number of water tiles in CLASS
#ex     iflm   = integer switch to control fractional land mask (iflm=1 -> on)
#
#       nnode_a is defined before gcmsub at the top of this script
#
    lon="  128";
   lond="  192";
 ioztyp="    6";

# new setting for oxidants. For now, relevant settings are {0,1}.
# ioxtyp=0 => CMIP6 setup. Input field is on model thermodynamic levels.
# ioxtyp=1 => input field is on pressure levels (19 levels).
 ioxtyp=1;
 if [ "$ioxtyp" -eq 0 ] ; then
   oxi=iphoxlp_cmam-961v_128_64
 else
   oxi=iphoxlp_cmam-961v_128x64_19plev
 fi

# --------------- Coupled Model Parameters -----------------
#
#
#     ocnmod: 0 = specified ocean temperature
#             1 = slab ocean
#             2 = NCOM 1.3 3D ocean model
#
#     icemod: 0 = specified ice
#             1 = thermodynamic + cavitating ice dynamics
#
#     rivmod: 0 = no targetting of land runoff to oceans
#             1 = targetting of land runoff using River Routing Scheme
#
#      iflux: 0 = adaptation (climatology file required)
#             1 = flux adjustment (flux correction file required)
#             2 = no flux correction
#
#  couplermod 0 = Standard mode
#             1 = NCOM/Coupler only mode

    ocnmod="00002";
    icemod="00001";
    rivmod="00001";
     iflux="00002";
couplermod="00000";

# --------------------- Time Parameters --------------------
#
# ncount:   Maximum number of coupling step before CGCM  stops and then
#           resubmits itself to the NQS queue.
#
# atmsteps: Coupling Frequency for the Atmospheric model (in seconds)
#
# ocnsteps: Coupling Frequency for the Ocean model (in seconds)
#
# ksteps:   Number of AGCM model time steps in a job (must be equal to
#           length of atmsteps)

   ncount="00032";
   atmsteps="  86400";
   ocnsteps="  86400";
#
   ksteps="   96";
   loop=1;

# -----------------------------------------------------------

 iocean="00000";
 if [ "$ocnmod" = "00001" ] ; then
   oceanslab=on;
   iocean="00001";
 fi

 if [ "$ocnmod" = "00002" ] ; then
   oceanslab=off;
   iocean="00001";
 fi

 if [ "$couplermod" = "00001" ] ; then
   oceanonly=on;
 else
   oceanonly=off;
 fi

 clim="udr_cgcm3.1_adaptation_256x192l29v1_v2";

 ocnmix=uwl_e_mix_t63_tidal_eddy_v2.dat

 # Daily chlorophyll climatology on ocean grid
 clim_bch=uwm_seawifs_bch_day_256x192v1

 # Flux added to heat and moisture fluxes; interpolated daily from mid-months values
 flux="mc_t63_zero_flux_year";

 # Flux added to heat and moisture fluxes; constant value from mid-month to mid-month
 flux2="mc_t63_zero2_flux_year";

#target="uva_128_64_river_parameters_v6";
# target="uva_river_parameters_128_64_aug_2016_for_cmip6_canesm_runs"
# target="uva_river_parameters_128_64_nov_2016_for_cmip6_canesm_runs"
# target="uva_river_parameters_128_64_dec_2016_for_cmip6_canesm_runs"
 target="uva_river_parameters_128_64_may_2017_cmip6_with_caspian_inland_lakes_t63_to_orca1_fland_caspian";
 lakes="gcm3.5_128x64_v5_lake_parameters_v2";

# cgcm_source="/users/tor/acrn/src/source/lsocn/CanESM4.3_cmip6_start";
# user_source="/users/tor/acrn/src/source/lsocn/CanESM4.3_cmip6_p02_agcm_ctem_river_routing_salinity_drift_fix";

## =========================================================================================
#  AGCM relaxation options
## =========================================================================================

 target_ss_pfx='' # target ss files. If not defined, use reanalysis.

 # reanalyses settings
  reanal_use_era5=off       # =on to use ERA5 data instead of ERA40/ERA-I
#  reanal_use_era_t63l60=on # =on to use ERA-I T63L60 instead of T120L37
#  RUNPATH_SPCFILE=$RUNID_ROOT/../td_era_int_t63l60_${modver}_t63 # save location
 reanal_use_era_t63l60=off # =on to use ERA-I T63L60 instead of T120L37
 RUNPATH_SPCFILE=$RUNID_ROOT/../td_era_int_${modver}_t63 # save location

 reanal_use_era_yotc=off # =on to use ERA-YOTC for 2008m05 - 2010m04
 reanal_force_sp2ss=off # =on to recompute spectral files
 reanal_debias_ncep=on # =on to remove bias from NCEP reference based on the 1979-2001 period
 reanal_debias_era40=on # = on to remove bias from ERA40 based on the 1979-2001 period
 reanal_add_top_levels_to_ncep=on # =on to add top levels to NCEP reanalysis
 reanal_tfilter=_ngaus3 # time filter suffix for output bias-corrected files
 reanal_ncep_bias=td_ncep-eraint_isobaric_t63_1979_2001_sp_ac${reanal_tfilter} # NCEP vs ERA-I bias
 reanal_era40_bias=td_era40-eraint_isobaric_t72_1979_2001_sp_ac${reanal_tfilter} # ERA40 vs ERA-I bias
 reanal_ncep_top_levels=td_ncep-eraint_isobaric_t63_1979_2001_sp_ac_top${reanal_tfilter} # NCEP vs ERA-I top levels
 reanal_spcfile_save=on # save spectral reference files on disk

 # upper soil level files
#  target_soil_tg1=td_era40_eraint_195709_201805_dp_128_64_soit1 # upper level soil temperature
#  target_soil_wg1=td_era40_eraint_195709_201805_dp_128_64_soim1 # upper level soil volumetric moisture
 target_soil_tg1='' # upper level soil temperature
 target_soil_wg1='' # upper level soil volumetric moisture

 # Ocean surface files
#  target_ocn_sst=td_ncep_era40_eraint_194801_201708_dp_128_64_sst # SST
#  target_ocn_sicn=td_hadisst2.2_128x64_1850_2017m07_dp_sicn # SICN
#   target_ocn_sic=td_piomas_orap5_194701_201710_dp_128_64_sic # PIOMAS & ORAP5 in SH
#  # target_ocn_sic=td_piomas_194701_201710_dp_128_64_sic # PIOMAS SIC is available only in Arctic
#  # target_ocn_sic=td_piomas_cfsr-cfsv2_194701_201710_dp_128_64_sic # PIOMAS & CFSR/CFSv2 in SH
 target_ocn_sst=''  # SST
 target_ocn_sicn='' # SICN
 target_ocn_sic=''  # SIC

# climatological empirical correction
#  tspcclim=da_gcm16_01_rlx27_1981_2010_ss_ac_daymean${reanal_tfilter};  # spectral
#  tgrdclim=da_gcm16_01_rlx27_1981_2010_ggtend_ac${reanal_tfilter}_t127; # gridded
 tspcclim='' # spectral
 tgrdclim='' # gridded

# CPP definitions files
 cppdefs_file=$CCRNSRC/CanESM/CONFIG/AMIP/cppdefs/cppdefs_32bit_default
 cppdefs_diag=$CCRNSRC/CanESM/CONFIG/AMIP/cppdefs/cppdefs_diag_default

#  * .................... Begin Critical Parameters ...........................
##PARC

    lmt="   63"     ;
  nlatd="   96"     ; lonsld="  192"     ;
   nlat="   64"     ;  lonsl="  128"     ;

   delt="    900.0" ;    lay="    2"     ;

   ilev="   49"     ;   levs="   49"     ;
  ntrac="   16"     ; ntraca="   12"     ;
  coord=" ET15"     ;   plid="      50.0";
  moist=" QHYB"     ; itrvar="QHYB"      ;

  ntld="    1";   ntlk="    3";   ntwt="    2";   iflm="    1";

  g01="-0108"; g02="-0144"; g03="-0190"; g04="-0250"; g05="-0327";
  g06="-0426"; g07="-0550"; g08="-0708"; g09="-0904"; g10="99115";
  g11="99145"; g12="99182"; g13="99228"; g14="99283"; g15="99350";
  g16="99430"; g17="99525"; g18="99637"; g19="99769"; g20="99923";
  g21="  110"; g22="  131"; g23="  154"; g24="  181"; g25="  211";
  g26="  245"; g27="  283"; g28="  325"; g29="  370"; g30="  420";
  g31="  474"; g32="  531"; g33="  592"; g34="  648"; g35="  698";
  g36="  742"; g37="  780"; g38="  813"; g39="  841"; g40="  865";
  g41="  885"; g42="  902"; g43="  916"; g44="  930"; g45="  944";
  g46="  958"; g47="  972"; g48="  986"; g49="  995"; g50="     ";
  g51="     "; g52="     "; g53="     "; g54="     "; g55="     ";
  g56="     "; g57="     "; g58="     "; g59="     "; g60="     ";
  g61="     "; g62="     "; g63="     "; g64="     "; g65="     ";
  g66="     "; g67="     "; g68="     "; g69="     "; g70="     ";
  g71="     "; g72="     "; g73="     "; g74="     "; g75="     ";
  g76="     "; g77="     "; g78="     "; g79="     "; g80="     ";
  g81="     "; g82="     "; g83="     "; g84="     "; g85="     ";
  g86="     "; g87="     "; g88="     "; g89="     "; g90="     ";
  g91="     "; g92="     "; g93="     "; g94="     "; g95="     ";
  g96="     "; g97="     "; g98="     "; g99="     ";
  h01="-0108"; h02="-0144"; h03="-0190"; h04="-0250"; h05="-0327";
  h06="-0426"; h07="-0550"; h08="-0708"; h09="-0904"; h10="99115";
  h11="99145"; h12="99182"; h13="99228"; h14="99283"; h15="99350";
  h16="99430"; h17="99525"; h18="99637"; h19="99769"; h20="99923";
  h21="  110"; h22="  131"; h23="  154"; h24="  181"; h25="  211";
  h26="  245"; h27="  283"; h28="  325"; h29="  370"; h30="  420";
  h31="  474"; h32="  531"; h33="  592"; h34="  648"; h35="  698";
  h36="  742"; h37="  780"; h38="  813"; h39="  841"; h40="  865";
  h41="  885"; h42="  902"; h43="  916"; h44="  930"; h45="  944";
  h46="  958"; h47="  972"; h48="  986"; h49="  995"; h50="     ";
  h51="     "; h52="     "; h53="     "; h54="     "; h55="     ";
  h56="     "; h57="     "; h58="     "; h59="     "; h60="     ";
  h61="     "; h62="     "; h63="     "; h64="     "; h65="     ";
  h66="     "; h67="     "; h68="     "; h69="     "; h70="     ";
  h71="     "; h72="     "; h73="     "; h74="     "; h75="     ";
  h76="     "; h77="     "; h78="     "; h79="     "; h80="     ";
  h81="     "; h82="     "; h83="     "; h84="     "; h85="     ";
  h86="     "; h87="     "; h88="     "; h89="     "; h90="     ";
  h91="     "; h92="     "; h93="     "; h94="     "; h95="     ";
  h96="     "; h97="     "; h98="     "; h99="     ";

 sref="  7.27e-03";   spow="        1.";

#  * ...................... End Critical Parameters ...........................

###

 # --- CanCPL coupler related parameters ---
 use_cancpl=$use_cancpl
 cancpl_ver=''
 cancpl_repo=''

 # cpl_rs_abort_if_missing_field = .false. means that the coupler will not abort
 # when it encounters a missing field in the coupler restat file.
 # The default is to abort in this situation.
 # The only valid values for cpl_rs_abort_if_missing_field are .true. or .false.
 cpl_rs_abort_if_missing_field=''

 # Is a bulk formulation used in the coupler to define fields sent to NEMO
 # The only valid values for bulk_in_cpl are .true. or .false.
 bulk_in_cpl=''

 # couple_serial = .true. means couple in serial mode (the agcm runs then the ocean runs)
 # This should never be set unless debugging requires serial mode
 # The only valid values for couple_serial are .true. or .false. (default .false.)
 couple_serial=''

 # Save coupler history files to DATAPATH
 save_cplhist=''
 save_cplhist_tavg=''

 #Specify the type of remapping used to remap windstress
 windstress_remap=''

 # Check to ensure that landfrac computed in coupler and restart are the same (retain behavior for p1)
 landfrac_bug=''

 # Define useful/obvious location of CanESM source code
 CANESM_SRC_ROOT=${CCRNSRC}/CanESM

 # files to bundle in the restart file
 agcm_restart_identifier="agcmrs"
 agcm_restart_files="OLDRS OLDTS ${agcm_exec} modl.dat INLINE_DIAG_NL PHYS_PARM RF_RTP"

 # --- NEMO related parameters ---
 # If use_nemo = on then the NEMO ocean will be coupled with the AGCM
 use_nemo=$use_nemo

 # If defined, nemo_rebuild_rsin is the name of a NEMO restart tarball that will be
 # used to initialize fields that are sent from the coupler to the agcm on the first
 # coupling interval. This restart will be rebuilt on the global ORCA grid prior to model
 # execution and then read by the coupler to extract the relevant fields.
 nemo_rebuild_rsin=''

 nemo_ver=''
 nemo_repo=''

 # nemo_config identifies a particular configuration (e.g. CCC_CANCPL_ORCA1_LIM)
 nemo_config=''

 # Namelists for NEMO IO
 # nemo_iodef=cancpl_orca1_lim_iodef.xml
 # output_level is NEMO priority level of output variables
 # nemo_xmlio_server_def=cancpl_orca1_lim_xmlio_server.def
 nemo_iodef=''
 output_level=''
 nemo_xmlio_server_def=''

 # nemo_from_rest is used to flag starting nemo from rest rather than a restart
 # If nemo_from_rest = on then nemo_restart must be undefined
 nemo_from_rest=''

 # The name of a tar file containing the NEMO restart files
 # This must be set for the first job of a run unless nemo_from_rest = on
 # Restart files created by the submission job will have names of the form
 #     ${uxxx}_${runid}_YYYYMMDD_restart.tar
 nemo_restart=''

 # nemo_carbon = off means use physical ocean only
 # nemo_carbon = on  means include PISCES
 nemo_carbon=off

 # nemo_trc = off means no passive tracers
 # nemo_trc = on  means include passive tracers (TOP).
 nemo_trc=off

 # nemo_cmoc = off means use PISCES
 # nemo_cmoc = on means use CMOC
 nemo_cmoc=off

 # nemo_cfc = off means CFCs are disabled
 # nemo_cfc = on means CFCs are enabled
 nemo_cfc=off

 # nemo_pisces_offline = off means use online model
 # nemo_pisces_offline = on  means use offline model with prescribed velocities
 nemo_pisces_offline=off

 nemo_runoff_core_monthly=''

 nemo_hist_file_suffix_list=""
 nemo_hist_file_freq_list=""
 nemo_hist_file_suffix_list_save=""
 nemo_hist_file_freq_list_save=""

 # Settings for NEMO AR6 diagnostics
 nemo_ln_subbas=.true.
 nemo_ln_ptrcomp=.true.

 # nemo_steps_in_job is the number of NEMO time steps to run
 # This number is determined internally and so is not normally set here
 # but it may be used for debugging (caveat lector)
 nemo_steps_in_job=''

 # Save NEMO history files to DATAPATH
 nemo_save_hist=on

 # Save NEMO restart files to DATAPATH
 nemo_save_rs=on

 # Dump NEMO restart archives to cfs
 nemo_dump_rs=off

 # Delete the previous nemo restart tar archive from DATAPATH.
 nemo_del_rs=off

 # NEMO time step in seconds
 nemo_rn_rdt=3600

 # nemo_nn_ice identifies the ice model used in NEMO
 #   nemo_nn_ice = 0 mean no sea ice
 #   nemo_nn_ice = 2 mean LIM2
 nemo_nn_ice=2

 # namelist lists
 agcm_namelists=""
 coupler_namelists=""

 # These variables are set when the job string is created
   previous_year=NotSet
  previous_month=NotSet
    current_year=NotSet
   current_month=NotSet
       next_year=NotSet
      next_month=NotSet
  run_start_year=NotSet
 run_start_month=NotSet
   run_stop_year=NotSet
  run_stop_month=NotSet

  run_start_time=NotSet
   run_stop_time=NotSet

### condef ###

#  * ............................ Condef Parameters ............................

 nextjob=on
 samerun=on
 parc_check=off
 float1=on
 initsp=off
 myrssti=off
 openmp=on
 slab=off
 flake=off
 coupled=on
 parallel_io=on # on, write model output in separate files for each task
 noprint=on
 nolist=off
 acctcom=on
 xmu=off
 pakjob=off
 debug=off
 noxlfimp=off
 noxlfqinitauto=on
 f90mod=on
 plaforce=off
 pla_access=off # access external data files for pla/pam
 relax=off

###

#  * ............................. Deck Definition .............................

. gcmrun.dk

#end_of_job
